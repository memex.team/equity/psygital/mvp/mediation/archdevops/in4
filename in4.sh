#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

## PRE - ENV ##
if test -f /tmp/in4_env; then
  . /tmp/in4_env
fi
. /media/sysdata/in4/platform/in4_core/internals/env/common_env.sh
## PRE - FUNCTIONS ##
. $In4CorePath/internals/helpers/in4func.sh
. /etc/os-release
echo -e "\n`date -u`\nRunning on $NAME version $VERSION kernel version `uname -r` \n\n"
##

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ -z $In4RunType ]]; then In4RunType="prod"; fi

if [[ -z $In4Area ]]; then
    DialogMsg="Please specify In4 Area"
    echo $DialogMsg; select In4Area in `in4func_findobj "$In4CorePath/areas/" 'd'`;  do  break ; done
fi

if [[ -z $In4Task ]]; then
    DialogMsg="Please specify In4 Task"
    echo $DialogMsg; select In4Task in `in4func_findobj "$In4CorePath/areas/$In4Area/" 'd'`;  do  break ; done
fi

if [[ -z $In4Context ]]; then
    DialogMsg="Please specify In4 context"
    echo $DialogMsg; select In4Context in `in4func_findobj "$In4CorePath/areas/$In4Area/$In4Task/interfaces/" 'f'`;  do  break ; done
fi

in4func_bash_include $In4CorePath/areas/$In4Area/$In4Task/_env.sh
in4func_bash_include $In4CorePath/areas/$In4Area/$In4Task/_1.pre.sh
in4func_bash_include $In4CorePath/areas/$In4Area/$In4Task/interfaces/$In4Context.sh
if [[ -f $In4CorePath/areas/$In4Area/$In4Task/_3.post.sh ]]; then
    in4func_bash_include $In4CorePath/areas/$In4Area/$In4Task/_3.post.sh
fi
## command echo
ArgsArray=`( set -o posix ; set )|grep '^In4'`
for i in ${ArgsArray[@]};do command=${command}" "$i; done
echo $command
##
in4func_bash_include $In4CorePath/areas/$In4Area/$In4Task/_4.run.sh

echo -e "Total exec time: $time"

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
