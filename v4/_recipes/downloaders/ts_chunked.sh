#!/bin/bash
set -e
filename=$1
folder=$2

filelines=`cat $filename`
for url in $filelines ; do
! rm *.ts
  res="-1"
  i=0
  echo "Downloading $url"
   while [[ $res -ne "0" ]]; do
     numD=$(printf "%03d" $i)
   ! wget $url${numD}.ts
     res="${?}"
     i=$((i+1))
   done
 
   ! files=`ls ./*.ts`
   bar=$(printf ",%s" "${files[@]}")
   bar=`echo $bar|sed -e 's/\.\//|/g' -e 's/\s//g'`
   bar=${bar:2}
   name=`echo $url|awk -F "/" {'print $5}'`
   ffmpeg -i "concat:$bar" -c copy $name.ts
   ffmpeg -i $name.ts -acodec copy -vcodec copy $folder/$name.mp4
done
