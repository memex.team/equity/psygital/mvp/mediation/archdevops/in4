########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

Summary:        Bigbluebutton
License:        (GPL-3.0+ and Apache-2.0)
Group:          System/Daemons
BuildRequires:  autoconf >= 2.61
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  zlib-devel  freetype-devel libjpeg-devel gcc gcc-c++ gradle

Requires: swftools freetype libjpeg zlib ghostscript freeswitch sox mariadb tomcat tomcat-lib graddle libreoffice libreoffice-common ffmpeg unixodbc redis
#activemq red5
#apt-get install -y build-essential git-core checkinstall yasm texi2html libvorbis-dev libx11-dev libxfixes-dev zlib1g-dev pkg-config nginx haproxy<
# libvpx 
#aglfn authbind bbb-apps bbb-apps-akka bbb-apps-deskshare bbb-apps-sip
# cabextract esound-common   fonts-crosextra-caladea fonts-crosextra-carlito gnuplot gnuplot-nox groff   jsvc libaa1 libaacs0 libao-common libao4 libaudiofile1 libavcodec54   libavformat54 libavutil52 libbluray1 libcdparanoia0   libcommons-collections3-java libcommons-daemon-java libcommons-dbcp-java   libcommons-pool-java libcurl4-openssl-dev libdca0 libdirectfb-1.2-9   libdvdnav4 libdvdread4 libecj-java libesd0 libfaad2 libgcrypt11-dev libgeronimo-jta-1.1-spec-java libgnutls-dev libgnutlsxx27 libgpg-error-dev   libgsm1 libidn11-dev libjemalloc1 libldap2-dev liblircclient0 liblua5.1-0   libmp3lame0 libmpeg2-4 libodbc1 libopenal-data libopenal1 libopencore-amrnb0   libopencore-amrwb0 libopenjpeg2 libp11-kit-dev libpostproc52 librtmp-dev   libschroedinger-1.0-0 libsox-fmt-alsa libsox-fmt-base libsox2 libspeex1   libssl-dev libssl-doc libssl1.0.2 libsvga1 libswscale2 libtasn1-6-dev libts-0.0-0 libwavpack1 libx86-1 libxml2-dev libxslt1-dev   libxvidcore4 libxvmc1 mencoder monit mplayer odbcinst odbcinst1debian2 poppler-utils psutils redis-tools tsconf ttf-mscorefonts-installer  vorbis-tools zip

#
%description
BBB
# bbb-apps-video bbb-client bbb-config bbb-freeswitch bbb-fsesl-akka bbb-mkclean bbb-office bbb-playback-presentation bbb-record-core bbb-red5  bbb-swftools bbb-web 

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

#%patch0 -p1
#
#dos2unix doc/*.html

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

sh -x bbb.sh


%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

make install DESTDIR="%{buildroot}"  V=1
#

%pre
#%{service_add_pre rsyslog.service}


%post


%preun

%postun

%files

%changelog
