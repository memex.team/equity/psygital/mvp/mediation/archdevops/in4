#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

#
AcmePath=$1

cd $AcmePath && ./acme.sh --renew-all

CertPath=$2

find $AcmePath -type d -name '*\.*' -maxdepth 1| sed 's#.*/##'|
while read domain
do
  if [[ ! -z "$domain" ]] ; then
    cp $domain/fullchain.cer $CertPath/$domain.pem
    cp $domain/$domain.key   $CertPath/$domain.key
    
    cat $domain/fullchain.cer > $CertPath/both/$domain.both
    cat $domain/$domain.key >> $CertPath/both/$domain.both
  fi
done
 
