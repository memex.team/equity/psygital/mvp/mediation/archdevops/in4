#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

time . $In4GitPath/v4/services--c/database--o/rdbms--f/oracle10g--g/_functional/in4_oracle_init_storage.sh
time . $In4GitPath/v4/services--c/database--o/rdbms--f/oracle10g--g/_functional/in4_oracle_init_database.sh                            

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
