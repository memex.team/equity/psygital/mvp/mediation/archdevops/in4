#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###


    ### SSH KEYS
    mkdir -p /media/storage/as/oracle/home/.ssh
    ssh-keygen -b 2048 -t rsa -f /media/storage/as/oracle/home/.ssh/id_rsa -q -N ""
    setfacl -R -b /media/storage/as/oracle/home/
    chmod 700 /media/storage/as/oracle/home/.ssh/
    chmod 600 /media/storage/as/oracle/home/.ssh/*
    chown -R oracle:oinstall /media/storage/as/oracle/home/
    ###

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
