#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

mkdir -p /media/storage/as/oracle/logs/adump
mkdir -p /media/storage/as/oracle/logs/bdump
mkdir -p /media/storage/as/oracle/logs/cdump
mkdir -p /media/storage/as/oracle/logs/udump
mkdir -p /media/storage/as/oracle/logs/audit
mkdir -p /media/storage/as/oracle/logs/network

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
