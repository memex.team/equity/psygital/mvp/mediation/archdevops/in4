#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###


if [[ $App_c2dbSchemeECoreImport == "Yes" ]]; then
    ! expdp SYSTEM/${App_c2dbsysPassword}@App_c2dbSchemeSrc.pool schemas=E\$CORE directory=export dumpfile=ecore_$Date.expdp.dump logfile=ecore_export_$Date.dump.log
else
    ! expdp SYSTEM/${App_c2dbsysPassword}@App_c2dbSchemeSrc.pool schemas=E\$${App_c2dbSchemeSrc} directory=export dumpfile=e${App_c2dbSchemeSrc}_${Date}.expdp.dump logfile=e${App_c2dbSchemeSrc}_export_${Date}.dump.log
fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
