#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

set -e

cp -rp /media/sysdata/in4/platform/v4/services--c/virtualization--o/orchestration--f/opennebula--g/dsl/datastore/in4 /var/lib/one/remotes/datastore/
cp -rp /media/sysdata/in4/platform/v4/services--c/virtualization--o/orchestration--f/opennebula--g/dsl/tm/in4 /var/lib/one/remotes/tm/

cp -rp /media/sysdata/in4/platform/v4/services--c/virtualization--o/orchestration--f/opennebula--g/dsl/im/xen-probes.d/ /var/lib/one/remotes/im/
cp -rp /media/sysdata/in4/platform/v4/services--c/virtualization--o/orchestration--f/opennebula--g/dsl/vmm/xen /var/lib/one/remotes/vmm/
cp -rp /media/sysdata/in4/platform/v4/services--c/virtualization--o/orchestration--f/opennebula--g/dsl/vnm/in4 /var/lib/one/remotes/vnm/
cp -rp /media/sysdata/in4/platform/v4/services--c/virtualization--o/orchestration--f/opennebula--g/dsl/vnm/vnmmad-load.d/* /var/lib/one/remotes/vnm/vnmmad-load.d/

chmod -R 755 /var/lib/one/remotes
chown -R oneadmin:cloud /var/lib/one/remotes
