 
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

class Forward(DatagramProtocol):
    def datagramReceived(self, data, (host, port)):
        print(data)
        self.transport.write(data, ('192.168.0.21', 2000))
        self.transport.write(data, ('127.0.0.1', 8505))
      
reactor.listenUDP(2000, Forward())
reactor.run() 
