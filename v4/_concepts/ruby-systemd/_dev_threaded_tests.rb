#!/usr/bin/env ruby
#
require 'dbus'
require 'concurrent'
require 'concurrent-edge'
require 'oj'
require 'json' 
#

bus = DBus::SystemBus.instance
service = bus.service("org.freedesktop.systemd1")
dotHex = '.'.unpack('H*')[0]
undScHex = '_'.unpack('H*')[0]
unit = service.object("/org/freedesktop/systemd1/unit/in4_#{undScHex}_#{undScHex}sshd_#{dotHex}service")
interface = unit["org.freedesktop.DBus.Properties"]

serviceName = ARGV[0]
accountingSec1 = 0.5
accountingSec2 = 2

serviceMetricsCountable = ['MainPID', 'SyslogIdentifier', 'MemoryCurrent', 'CPUUsageNSec', 'TasksCurrent','IPIngressPackets', 'IPEgressPackets', 'IPIngressBytes', 'IPEgressBytes', 'IOEgressBytes', 'IOIngressBytes']
serviceMetricsStatus = ['MainPID', 'SyslogIdentifier', 'Result', 'NRestarts', 'ExecMainStatus', 'Nice']
unitMetricsStatus = ['ActiveState', 'SubState', 'UnitFileState']
#


def getMetric(type, interface, metrics, idleSec) 

#   serviceDbus = DBus::Systemd::Unit.new("#{serviceName}.service")    
  res = {}
  loop do    
    metrics.each do |metric|
      preValue = interface.Get("org.freedesktop.systemd1.#{type}", metric)[0]
      preValue = 1122
      preValue == 18446744073709551615 ? value = 0 : value = preValue
      res[metric.to_sym] = value
    end
    p "res: #{res}"    
    sleep(idleSec)
  end  
    return  res
end


accounting = {}
status = {}

types = {}
types[:service] = {}
types[:service][:countable] = {}
types[:service][:countable][:metrics] = serviceMetricsCountable
types[:service][:countable][:sec] = 1
types[:service][:status] = {}
types[:service][:status][:metrics] = serviceMetricsStatus
types[:service][:status][:sec] = 0.5
aa = Concurrent::ImmediateExecutor.new
bb = Concurrent::CachedThreadPool.new


# all_promises = Concurrent::Promises.zip(
#   Concurrent::Promises.future { getMetric(:service, serviceName, serviceMetricsCountable, 1) },
#   Concurrent::Promises.future { getMetric(:service, serviceName, serviceMetricsStatus, 0.5) }
# )
# 
# p all_promises.then { |*values| puts values.inspect }.value!

# pr = {}
# # loop do
#   types.map do |type, typeOpts|
#     res =  typeOpts.map do |name, val|
#         pr[name] = Concurrent::Promise.new(executor: bb) do 
#           getMetric(type, serviceDbus, val[:metrics], val[:sec])
#           :value
#         end
#       end
# 
#     
#     res.each(&:value!) # resolve futures - you might make this an #all? to check the return value of the operation
# 
#     if res.any?(&:rejected?) # check for any that raised an exception
#       errors = res.select(&:rejected?).map(&:reason)
#       p "ERR - #{errors}"
#       # ...
#     else
#       p "OK"
#     end
# 
#   end
# # sleep(accountingSec)
# end

# a = Concurrent::Future.new { getMetric(:service, serviceName, serviceMetricsCountable, accountingSec)  }
# b = Concurrent::Future.new { getMetric(:service, serviceName, serviceMetricsStatus, accountingSec)  }

c = Concurrent::Promises.future(1) do |duration|
  getMetric('Service', interface, serviceMetricsCountable, accountingSec1)
  :result
end
d = Concurrent::Promises.future(1) do |duration|
  getMetric('Unit', interface, unitMetricsStatus, accountingSec2)
  :result
end

begin
  resC = c.value
  resD = d.value    
  p "resC: #{resC}, resD: #{resD}"
#   b.value
rescue => e 
  p "err: #{e}"
end  

# p c.value
# sleep(1)
# a.execute
# b.execute
# c.execute
# 
# a.value
# b.value
# b.value

# Concurrent::Promises.future{  }.value
# Concurrent::Promises.future{ getMetric(:service, serviceDbus, serviceMetricsStatus, status, accountingSec) }.value
# Concurrent::Promises.future{ getMetric(:unit, serviceDbus, unitMetricsStatus, status, accountingSec) }.value  
