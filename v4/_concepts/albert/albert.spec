########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
# Source0: albert.spec
#Patch0:        
##

Summary:        Albert keyboard launcher
License:        GPL
Group:          User Interface/X
URL:            https://github.com/albertlauncher/albert

BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildRequires:  cmake
BuildRequires:  gcc-c++ >= 4.8
BuildRequires:  hicolor-icon-theme
BuildRequires:  libqt5-qtbase-devel >= 5.8
BuildRequires:  libqt5-qtdeclarative-devel >= 5.8
BuildRequires:  libqt5-qtx11extras-devel >= 5.8
BuildRequires:  libqt5-qtsvg-devel >= 5.8
BuildRequires:  python3-devel
BuildRequires:  muparser-devel
BuildRequires:  libqalculate-devel

%description
Albert is a unified and efficient access to your machine. Technically it is a keyboard launcher written in C++/Qt. The plugin based architecture makes it extremely flexible and powerful. Plugins can be used to create modular frontends or native extensions. Extensions can also be implemented using embedded Python modules or via a CGI approach in any language you want. The frontends shipped heavily focus on customizability.

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //


%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

mkdir build
cd build
cmake \
 -D CMAKE_INSTALL_PREFIX=/usr \
 -D CMAKE_BUILD_TYPE=Release \
 -D CMAKE_BUILD_TYPE=Release \
%if 0%{?sle_version} == 120300 || 0%{?sle_version} == 120200 
 -D CMAKE_CXX_COMPILER=/usr/bin/g++-5 \
%endif
 -D BUILD_VIRTUALBOX=OFF \
 ..
make -j $((`nproc`+1)) 


%define  debug_package %{nil}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //
cd build
make DESTDIR=$RPM_BUILD_ROOT install VERBOSE=1
exit 0  # Dont compile python

%files
%defattr(-,root,root)
%{_bindir}/albert
%{_libdir}/%{name}/
%{_datadir}/%{name}/
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/applications/
%{_datadir}/applications/%{name}.desktop


%changelog
* Thu May 05 2018 Eugene Istomin 
- Initial
