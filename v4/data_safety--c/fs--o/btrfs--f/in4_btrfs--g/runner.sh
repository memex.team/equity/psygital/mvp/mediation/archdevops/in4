#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

. /media/sysdata/in4/platform/in4_core/internals/naming/naming.sh os
. /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/func.sh
if [[ -z $In4SnapMode ]]; then
    DialogMsg="Select snapshot mode"   
    echo $DialogMsg; select In4SnapMode in manual svn app;  do  break ; done;
fi
                
case $In4SnapMode in
    "manual")
        if [[ -z $SnapDirPath ]]; then echo "Please specify dir for snap";  read $SnapDirPath;  fi
        SnapSched="no"
        In4SnapMode="manual"
        SnapStartTime=`date +%s`
        time . /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/snap_manager.sh	
        SnapEndTime=`date +%s`
        SnapOk
        ;;

    "svn")                
    for SNAP_TASK in /media/sysdata/in4/_context/conf/snapshots/$Net/$SrvType/$SrvName/*; do
        . $SNAP_TASK
        In4SnapMode="svn"
        SnapStartTime=`date +%s`
        time . /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/snap_manager.sh	
        SnapEndTime=`date +%s`
        SnapOk
    done 
    ;;
    "app")                
    for SNAP_TASK in /media/sysdata/app/btrfs_snap/*/*; do
        . $SNAP_TASK
        In4SnapMode="app"
        SnapStartTime=`date +%s`
        time . /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/snap_manager.sh	
        SnapEndTime=`date +%s`
        SnapOk
    done 
    ;;    
esac

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
