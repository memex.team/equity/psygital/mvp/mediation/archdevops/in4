#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ -z $SnapDirPath ]]; then echo "Please specify dir for snap";  exit 1;  fi
if [[ -z $SnapSched ]]; then  echo "Please specify schedule";  exit 1; fi
if [[ -z $In4SnapMode ]]; then  echo "Please specify mode";  exit 1; fi

### INIT ###
declare -A k v SnapSchedArray
BTRFS_LABEL=`sudo btrfs filesystem label $SnapDirPath`
TMP_QGROUP_LIST="/tmp/btrfs_${BTRFS_LABEL}_qgroup_all"
TMP_SUB_LIST="/tmp/btrfs_${BTRFS_LABEL}_sub_all"
TMP_QGROUP_LIST_EMPTY="/tmp/btrfs_${BTRFS_LABEL}_qgroup_all_empty"
BTRFS_DEV=`sudo btrfs filesystem show $BTRFS_LABEL|grep "/dev/"|awk '{print $8}'`
BTRFS_MOUNT=`mount|grep $BTRFS_DEV -m1|awk '{print $3"/"}'`
if [[ $BTRFS_MOUNT == "//" ]]; then  BTRFS_MOUNT="/";  fi
if [[ $BTRFS_MOUNT == "$SnapDirPath/" ]]; then SnapMode='disk'; fi
SnapDirOwner=`sudo getfacl $SnapDirPath|grep "owner"|awk '{print $3}'`

if [[ $SnapMode == "disk" ]]; then
    SNAP_PATH="${SnapDirPath}/_snap"
    sudo mkdir -p $SNAP_PATH
    BTRFS_SNAP_PATH_ID=2
    BTRFS_PATH_ID=1
else
    SNAP_PATH="${SnapDirPath}_snap"
    sudo mkdir -p $SNAP_PATH
    BTRFS_SNAP_PATH_ID=`sudo btrfs subvolume show $SNAP_PATH|grep "Subvolume ID:"|awk '{print $3}'`
    BTRFS_PATH_ID=`sudo btrfs subvolume show $SnapDirPath|grep "Subvolume ID:"|awk '{print $3}'`
fi

# FIX!!  - only owner currently, neeeds full setfacl stack 
sudo chown $SnapDirOwner $SNAP_PATH
#

BTRFS_SNAP_PATH_REL=${SNAP_PATH#"$BTRFS_MOUNT"}
SnapQGroupRead  $BTRFS_MOUNT $TMP_QGROUP_LIST
DATE=`date +%d.%m.%y_w%W_%H:%M:%S`
SnapSchedParse $SnapSched

. /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/vars.sh
. /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/func.sh

### ROOT_QGROUP ###
    SnapUnitDigit=$SnapUnitDigitRoot
    SnapUnitNaming=$SnapUnitNamingRoot
    SnapCreateBaseQgroup
###

### REGISTRED_QGROUP ###
    SnapUnitDigit=$SnapUnitDigitRegistred
    SnapUnitNaming=$SnapUnitNamingRegistred
    SnapCreateBaseQgroup
###

### UNSORTED_QGROUP ###
    SnapUnitDigit=$SnapUnitDigitUnsorted
    SnapUnitNaming=$SnapUnitNamingUnsorted
    SnapCreateBaseQgroup
###

### TRASH_QGROUP ###
    SnapUnitDigit=$SnapUnitDigitTrash
    SnapUnitNaming=$SnapUnitNamingTrash
    SnapCreateBaseQgroup
###

if [[ $In4SnapMode == "manual" ]]; then
        SnapUnitDigit=$SnapUnitDigitManual
        SnapUnitNaming=$SnapUnitNamingManual
        SnapCreateBaseQgroup
        SnapDo        
elif ! [[ -z ${SnapSchedArray[h]} ]]; then
        SnapUnitDigit=$SnapUnitDigitHourly
        SnapUnitNaming=$SnapUnitNamingHourly
        SnapUnitTimingCriteria=$SnapUnitTimingCriteriaHourly
        SnapScope=${SnapSchedArray[h]}
        SnapCreateBaseQgroup
        SnapDo        
        SnapUnitPath="$SNAP_PATH/$SnapUnitDigit.$SnapUnitNaming/${SnapUnitTimingCriteria}*"        
elif ! [[ -z ${SnapSchedArray[d]} ]]; then
        SnapUnitDigit=$SnapUnitDigitDaily
        SnapUnitNaming=$SnapUnitNamingDaily
        SnapUnitTimingCriteria=$SnapUnitTimingCriteriaDaily
        SnapScope=${SnapSchedArray[d]}
        SnapCreateBaseQgroup
        SnapDo      
        SnapUnitPath="$SNAP_PATH/$SnapUnitDigit.$SnapUnitNaming/${SnapUnitTimingCriteria}*"
elif ! [[ -z ${SnapSchedArray[w]} ]]; then
        SnapUnitDigit=$SnapUnitDigitWeekly
        SnapUnitNaming=$SnapUnitNamingWeekly
        SnapUnitTimingCriteria=$SnapUnitTimingCriteriaWeekly
        SnapScope=${SnapSchedArray[w]}
        SnapCreateBaseQgroup
        SnapDo      
        SnapUnitPath="$SNAP_PATH/$SnapUnitDigit.$SnapUnitNaming/${SnapUnitTimingCriteria}*"
elif ! [[ -z ${SnapSchedArray[m]} ]]; then
        SnapUnitDigit=$SnapUnitDigitMonthly
        SnapUnitNaming=$SnapUnitNamingMonthly
        SnapUnitTimingCriteria=$SnapUnitTimingCriteriaMonthly
        SnapScope=${SnapSchedArray[m]}
        SnapCreateBaseQgroup
        SnapDo      
        SnapUnitPath="$SNAP_PATH/$SnapUnitDigit.$SnapUnitNaming/${SnapUnitTimingCriteria}*"
fi
   
if ! [[ $In4SnapMode == "manual" ]]; then
   
    SnapUnsorted=`sudo ls $SNAP_PATH/$SnapUnitDigitUnsorted.$SnapUnitNamingUnsorted/|tail -n1`
    if [[ -n "$SnapUnsorted" ]]; then

        for curr_path in $SnapUnitPath/*; do
            [ -d "${curr_path}" ] || continue # if not a directory, skip
            echo "Managing $curr_path"
            SnapDelete
        done
        
        ###  MIGRATION TO POOL ###
            SnapSubvolumeRead $BTRFS_MOUNT $TMP_SUB_LIST
            curr_path=$SNAP_PATH/$SnapUnitDigitUnsorted.$SnapUnitNamingUnsorted/$SnapUnsorted
            BTRFS_SNAP_PATH_REL=${curr_path#"$BTRFS_MOUNT"}
            BTRFS_SNAP_PATH_ID=`grep  "$BTRFS_SNAP_PATH_REL\/" $TMP_SUB_LIST|awk '{print $2}'`    
            ! sudo btrfs qgroup remove $BTRFS_SNAP_PATH_ID $SnapUnitDigitUnsorted/${BTRFS_PATH_ID}0000 $BTRFS_MOUNT;    
            ! sudo btrfs qgroup assign --no-rescan $BTRFS_SNAP_PATH_ID $SnapUnitQgroupId  $SNAP_PATH    
            sudo mv  $curr_path  $SNAP_PATH/$SnapUnitDigit.$SnapUnitNaming
            SnapSubvolumeRead $BTRFS_MOUNT $TMP_SUB_LIST
        ###
        
        ### SNAP SCOPE  - DELETE OUT OF SCOPE ###
            while [[ `ls -daA $SnapUnitPath|wc -l` -gt $SnapScope ]]; do
                curr_path=`sudo find $SnapUnitPath -type f -exec stat -c "%n" {} \; | sort -n | head -1`
                echo "Deleting $curr_path"
                SnapDelete
            done
        ###
        
        for curr_path in $SNAP_PATH/$SnapUnitDigitUnsorted.$SnapUnitNamingUnsorted/*/*; do
            [ -d "${curr_path}" ] || continue # if not a directory, skip
            echo "Managing $curr_path"        
            SnapDelete
        done
        
        QGROUP_EMPTY=`sudo btrfs qgroup show $BTRFS_MOUNT|grep "0.00B"|awk '{print $1}' > $TMP_QGROUP_LIST_EMPTY`
    #     for qgroup in `cat $TMP_QGROUP_LIST_EMPTY`; do
    #         btrfs qgroup destroy $qgroup $BTRFS_MOUNT
    #     done
        
    fi
fi
#

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
