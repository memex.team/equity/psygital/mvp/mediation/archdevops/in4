#!/bin/bash
set -e

SnapPath="/media/sysdata"

mkdir -p $SnapPath/.snapshots/
PrevSnapNumber=`ls $SnapPath/.snapshots/|sort -n|tail -1`
SnapNumber=$(( $PrevSnapNumber + 1))
Date=`date "+%Y-%m-%d %H:%M:%S"`

mkdir -p $SnapPath/.snapshots/$SnapNumber
. /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/snapper_xml.sh > $SnapPath/.snapshots/$SnapNumber/info.xml
btrfs subvolume snapshot $SnapPath $SnapPath/.snapshots/$SnapNumber/snapshot
