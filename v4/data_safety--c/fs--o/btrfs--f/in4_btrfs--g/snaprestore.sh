#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ -z $SnapDirPath ]]; then echo "Please specify dir for restore: ";  if ! [[ -z $1 ]]; then SnapDirPath=$1; else exit 1; fi;  fi

if [[ -z $SnapRestoreUnit ]]; then
    DialogMsg="Please select available snap units for $SnapDirPath snapshots: "
    #SnapRestoreUnitFullList=()
    echo $DialogMsg; select SnapRestoreUnitFull in `ls -daA ${SnapDirPath}_snap/*`;  do  break ; done
    DialogMsg="Please select available snapshots in $SnapRestoreUnitFull: "    
    echo $DialogMsg; select SnapRestorePathFQ in `ls -daA $SnapRestoreUnitFull/*`;  do  break ; done
else
    SnapRestorePathFQ=$SnapDirPath_snap/$SnapRestoreUnit/$SnapRestoreDate
fi

. /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g//vars.sh
SnapUnitDigit=$SnapUnitDigitManual
SnapUnitNaming=$SnapUnitNamingManual
SnapCreateBaseQgroup
echo "\n###Restoring $SnapRestorePathFQ as $SnapDirPath ###\n"
In4SnapMode="manual" . /media/sysdata/in4/platform/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/runner.sh
## BUG - assign to qroup "manual"
btrfs subvolume delete $SnapDirPath
btrfs subvolume snapshot $SnapRestorePathFQ/master /media/storage/as/oracle/data

#
export TERM=xterm
tput setaf 2
echo "${green}\n\n\n################# $SnapRestorePathFQ restored as $SnapDirPath #################\n"
tput setaf 9   
#

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###

