
    mkdir -p /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN/htdocs                    
    mkdir -p /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN/logs
    mkdir -p /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN/conf/services
    mkdir -p /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN/tmp/nginx
    mkdir -p /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN/tmp/php/sessions         

		#PHP TEST PAGE
		echo "<?php phpinfo(); ?>" > /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN/htdocs/index.php	
    fi
    echo "I am alive - $LX_DOMAIN.$L2_DOMAIN" > /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN/htdocs/index.html
    
   
    chown -R $LX_DOMAIN.$L2_DOMAIN:nobody /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    chmod -R 700 /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    
    setfacl -R -m d:u:$LX_DOMAIN.$L2_DOMAIN:rwx /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m d:u::rwx /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m u:$LX_DOMAIN.$L2_DOMAIN:rwx /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m u::rwx /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m d:g:nobody:x /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m d:g::x /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m g:nobody:x /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m g::x /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m d:o::x /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -m o::x /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -m u:$LX_DOMAIN.$L2_DOMAIN:rwx /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -x g:nobody /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    setfacl -R -x d:g:nobody /media/storage/sites/$LX_DOMAIN.$L2_DOMAIN
    
   
        
	if [[ $WITH_MYSQL == "yes" ]]; then        
		#DB database & user add
		CREATEDB="CREATE DATABASE IF NOT EXISTS \`$LX_DOMAIN.$L2_DOMAIN\` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;" 
		CREATEUSER="GRANT ALL ON \`$LX_DOMAIN.$L2_DOMAIN\`.* TO '$DB_USERNAME'@'localhost' IDENTIFIED BY '$PASSWORD';" 
		mysql -u root -e "$CREATEDB"
		mysql -u root -e "$CREATEUSER"
		mysql -u root -e "FLUSH PRIVILEGES;"
	fi
