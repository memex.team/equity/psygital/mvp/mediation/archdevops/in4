#!/bin/bash
set -e 

### Server
SrcSrv='10.0.3.111'
SrcPort=1000
SrcUser='www.runivers.ru'
SrcDir='/media/storage/sites/runivers.ru/www/htdocs/'
DbData='_synced/bitrix/php_interface/dbconn.php'

### Directories
BitrixCore=('bitrix')
# BitrixLocal=()
Static=('upload' 'books' 'bookreader' 'video')
More=('philosophy')
Tech=('sphinx')

### Data provision
FromSnapshot="${More[@]}"
Nosync="${FromSnapshot[@]} ${Tech[@]} ${Static[@]}"
#?? Git

