#!/bin/bash
set -e 

sudo /bin/bash -x /media/sysdata/in4/platform/v5/roles/php/init/userinit_sudo.sh 

mkdir -p ~/apps ~/apps/_specs ~/exenv ~/exenv/php
cp /media/sysdata/in4/platform/v5/landscape/lang--c/procedure--o/php--f/php-common/dsl/main--s/simple/php-fpm.conf ~/exenv/php/fpm.conf
sed -i "s/{Developer}/${USER}/g" ~/exenv/php/fpm.conf
sed -i "s/{DeveloperCompany}/pixelplus/g" ~/exenv/php/fpm.conf
sudo systemctl restart ${USER}_phpfpm@55

mkdir -p ~/exenv/mariadb/tmp ~/exenv/mariadb/data 
cp /media/sysdata/in4/platform/v4/services--c/database--o/rdbms--f/mariadb--g/my_devserver.cnf ~/exenv/mariadb/my.conf
sed -i "s/{RunPath}/\/media\/storage\/dev\/$USER\/exenv\/mariadb/g" ~/exenv/mariadb/my.conf
sed -i "s/{DataPath}/\/media\/storage\/dev\/$USER\/exenv\/mariadb\/data/g" ~/exenv/mariadb/my.conf
sed -i "s/{TmpPath}/\/media\/storage\/dev\/$USER\/exenv\/mariadb\/tmp/g" ~/exenv/mariadb/my.conf
sudo systemctl restart ${USER}_mariadb

cp /media/sysdata/in4/platform/v5/roles/php/profile.d/aliases ~/.bashrc
sed -i "s/{Developer}/${USER}/g" ~/.bashrc

cd ~/apps/_specs && git init

