#!/bin/bash
set -e
systemctl enable netns@${SUDO_USER}.service && systemctl restart netns@${SUDO_USER}.service
cp  s/systemd/dev_nginx@.service /etc/systemd/system/${SUDO_USER}_nginx@.service
sed -i "s/{Developer}/${SUDO_USER}/g" /etc/systemd/system/${SUDO_USER}_nginx@.service
sed -i "s/{DeveloperCompany}/pixelplus/g" /etc/systemd/system/${SUDO_USER}_nginx@.service

cp /media/sysdata/in4/platform/v5/landscape/lang--c/procedure--o/php--f/php-common/dsl/main--s/simple/developer_php-fpm@.service /etc/systemd/system/${SUDO_USER}_phpfpm@.service
sed -i "s/{Developer}/${SUDO_USER}/g" /etc/systemd/system/${SUDO_USER}_phpfpm@.service
sed -i "s/{DeveloperCompany}/pixelplus/g" /etc/systemd/system/${SUDO_USER}_phpfpm@.service


cp /media/sysdata/in4/platform/v4/services--c/database--o/rdbms--f/mariadb--g/_systemd/dev_mariadb.service /etc/systemd/system/${SUDO_USER}_mariadb.service
sed -i "s/{Developer}/${SUDO_USER}/g" /etc/systemd/system/${SUDO_USER}_mariadb.service
sed -i "s/{DeveloperCompany}/pixelplus/g" /etc/systemd/system/${SUDO_USER}_mariadb.service


cp /media/sysdata/in4/platform/v5/roles/php/sudo/sudo_developer /etc/sudoers.d/${SUDO_USER}_dev
sed -i "s/{Developer}/${SUDO_USER}/g" /etc/sudoers.d/${SUDO_USER}_dev

cp /etc/haproxy/haproxy.cfg_tmpl /etc/haproxy/haproxy.cfg_try
for Developer in $(ls /media/storage/dev/); do 
  echo "backend  $Developer" >> /etc/haproxy/haproxy.cfg_try
  echo "    option tcp-smart-connect" >> /etc/haproxy/haproxy.cfg_try
  echo "    server 127.0.0.7:1080 127.0.0.7:1080 namespace $Developer  weight 1 maxconn 30 check inter 4000 fastinter 8000 rise 1 fall 2" >> /etc/haproxy/haproxy.cfg_try
done

haproxy -c -f /etc/haproxy/haproxy.cfg_try
cp /etc/haproxy/haproxy.cfg_try /etc/haproxy/haproxy.cfg
systemctl reload haproxy.service
