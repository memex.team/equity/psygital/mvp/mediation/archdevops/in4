#!/bin/bash
set -e
User=$1
! groupadd pixelplus
! useradd $User -m -g pixelplus -d /media/storage/dev/$User -k /dev/null
ln -s  /media/sysdata/in4/platform/v5/roles/php/init/user_init.sh /media/storage/dev/$User/.first_login
