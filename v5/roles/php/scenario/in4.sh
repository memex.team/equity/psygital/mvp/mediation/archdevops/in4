#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e
. /etc/in4-release
. /media/sysdata/in4/platform/in4_core/internals/env/common_env.sh
## PRE - FUNCTIONS ##
. $In4CorePath/internals/helpers/in4func.sh


in4func_ZypperRepo "add" "in4::lang--c--procedure--o--php--f--php7--g"

in4func_Zypper "php7 php7-bcmath php7-bz2 php7-curl php7-ctype php7-dom php7-gd php7-gettext php7-iconv php7-fpm php7-json php7-mbstring php7-mysql php7-ldap php7-openssl php7-pdo php7-pgsql php7-soap php7-sockets php7-sqlite php7-snmp php7-sysvmsg php7-sysvsem php7-sysvshm php7-tidy php7-tokenizer php7-zip php7-zlib php7-xmlreader php7-xmlwriter php7-intl"
#php7-imagick

#??? - phpX-opcache, ImageMagick + php-im
#suhosin.ini

in4func_ZypperRepo "add" "in4::services--c--proxy--o--in--f--haproxy--g"
in4func_Zypper haproxy
# systemctl enable haproxy.service 
#haproxy config
#haproxy 

in4func_ZypperRepo "add" "in4::services--c--server--o--http--f--nginx--g"
in4func_Zypper nginx


# in4func_ZypperRepo "add" "in4::services--c--database--o--rdbms--f--mariadb--g"
# in4func_Zypper "mariadb mariadb-client mariadb-tools xtrabackup percona-toolkit libmariadb_plugins"
# mkdir -p /media/storage/app/mariadb

# in4func_Zypper pure-ftpd
#rev5_pure-ftpd.sh

# rev5_mariadb.sh


####
mkdir /media/storage/dev
chmod 770 /media/storage/dev/
chown root:pixelplus /media/storage/dev

mkdir /media/storage/app
#chown, chmod
#setfacl  -x o:: /media/storage/
#setfacl  -x d:o:: /media/storage/
