#!/bin/bash
set -e 

App=$1

. ~/apps/_specs/$App.appspec

mkdir -p ~/apps/$App
mkdir -p ~/apps/$App/BitrixCore
mkdir -p ~/apps/$App/BitrixLocal
mkdir -p ~/apps/$App/Static
mkdir -p ~/apps/$App/More
# mkdir -p ~/apps/$App/Tech
mkdir -p ~/apps/$App/Caches
mkdir -p ~/apps/$App/_tmp/nginx
mkdir -p ~/apps/$App/_tmp/sqldump
mkdir -p ~/apps/$App/_synced
mkdir -p ~/apps/$App/_conf
mkdir -p ~/apps/$App/_logs

cp /media/sysdata/in4/platform/v5/landscape/services--c/server--o/http--f/nginx/dsl/main--s/conf/server_php_bitrix_dev.conf ~/apps/$App/_conf/nginx.conf
sed -i "s/{ROOT}/_synced/g" ~/apps/$App/_conf/nginx.conf
sed -i "s/{SOCK}/\/media\/storage\/dev\/$USER\/exenv\/php\/fpm.sock/g" ~/apps/$App/_conf/nginx.conf
sudo systemctl restart ${USER}_nginx@$App


for i in $Nosync; do
  RsyncNosync="$RsyncNosync --exclude $i "
done

# Type=' -acrz --del '
RsyncType=' -acurz '

if [[ -f ~/apps/$App/_synced/index.php ]]; then
  echo "Already synced"
else
#   sudo systemctl restart dev_get_files_rsync@
  time rsync $RsyncNosync $RsyncType --info=progress2 $SrcUser@$SrcSrv:$SrcDir $HOME/apps/$App/_synced
fi


## DB
if [[ ! -f ~/apps/$App/${DbData}_prod ]]; then
  cp ~/apps/$App/$DbData ~/apps/$App/${DbData}_prod
fi

DBHost=`cat ~/apps/$App/${DbData}_prod|grep '$DBHost'|cut -d '"' -f 2`
DBLogin=`cat ~/apps/$App/${DbData}_prod|grep '$DBLogin'|cut -d '"' -f 2`
DBPassword=`cat ~/apps/$App/${DbData}_prod|grep '$DBPassword'|cut -d '"' -f 2`
DBName=`cat ~/apps/$App/${DbData}_prod|grep '$DBName'|cut -d '"' -f 2`

if [[ -f ~/apps/$App/_tmp/sqldump/dump.sql ]]; then
  echo "Already dumped"
else
#   sudo systemctl restart dev_get_db_mysqldump@
  time mysqldump --opt --single-transaction --skip-lock-tables --add-drop-table "$DBName" -h $DBHost -u "$DBLogin" -p"$DBPassword"  > ~/apps/$App/_tmp/sqldump/dump.sql
fi

! mysql -u root -S "$HOME/exenv/mariadb/mysql.sock" -e"CREATE DATABASE \`${DBName}\`;"
! mysql -u root -S "$HOME/exenv/mariadb/mysql.sock" -e"GRANT ALL PRIVILEGES ON \`${DBName}\`.* TO '${DBName}'@'localhost' IDENTIFIED BY '$DBPassword';"
! mysql -u root -S "$HOME/exenv/mariadb/mysql.sock" -e"FLUSH PRIVILEGES;"

time mysql --init-command="set autocommit=0; set unique_checks=0; set foreign_key_checks=0;" -u root -S "$HOME/exenv/mariadb/mysql.sock" ${DBName} < ~/apps/$App/_tmp/sqldump/dump.sql
#rm ~/apps/$App/_tmp/sqldump/dump.sql

### TRYING TO CHANGE DB ON-FLY
! sed -i "s/?>/define("BX_USE_MYSQLI", true); \n ?>/" $HOME/apps/$App/_synced/bitrix/php_interface/dbconn.php
! sed -i "s/\$DBHost = .*/\$DBHost = '127.0.0.1';/" $HOME/apps/$App/_synced/bitrix/php_interface/dbconn.php

! sed -i "s/'className' => '\\Bitrix\\Main\\DB\\MysqlConnection',/'className' => '\\Bitrix\\Main\\DB\\MysqliConnection',/" $HOME/apps/$App/_synced/bitrix/.settings.php
! sed -i "s/'host' => .*/'host' => '127.0.0.1',/" $HOME/apps/$App/_synced/bitrix/.settings.php
