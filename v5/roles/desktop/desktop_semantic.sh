 
in4func_Zypper "baloo5-kioslaves baloo5-widgets baloo5-tools baloo5-file"
in4func_Zypper "attr qrencode xdotool xsel"
in4func_Zypper "kdav kfilemetadata5 "

in4func_Zypper "akonadi-search akonadi-calendar-tools akonadi-plugin-calendar akonadi-plugin-contacts akonadi-plugin-kalarmcal akonadi-import-wizard akonadi-server akonadiconsole pim-sieve-editor pim-data-exporter "
# in4func_Zypper "plasmoid-akonadi-calendars plasmoid-akonadi-tasks"
in4func_Zypper "poppler-tools "

in4func_Zypper "ruby2.5-devel"
in4func_Zypper "ruby2.5-rubygem-oj ruby2.5-rubygem-curb ruby2.5-rubygem-faraday ruby2.5-rubygem-redis-2_2 ruby2.5-rubygem-sqlite3"


! in4func_ZypperRepo "in4::lang--c--procedure--o--python--f--python3"
in4func_Zypper "python3 python3-base python3-pip pypy python3-Cython"
! in4func_ZypperRepo "in4::lang--c--functional--o--erlang--f--elixir"
in4func_Zypper "elixir elixir-hex"


#new ruby
! in4func_ZypperRepo "add" "https://download.opensuse.org/repositories/devel:/languages:/ruby/openSUSE_Leap_15.0 devel::languages::ruby"
! in4func_ZypperRepo "add" "https://download.opensuse.org/repositories/devel:/languages:/ruby:/extensions/openSUSE_Leap_15.0 devel::languages::ruby::extensions"
in4func_Zypper "ruby2.5"


! in4func_ZypperRepo "add" "https://download.opensuse.org/repositories/Application:/Geo/openSUSE_Leap_15.0 Application::Geo"
in4func_Zypper "qgis qgis-gui qgis-server"

in4func_Zypper "zbar"

#digest
in4func_ZypperRepo 'add' 'https://download.opensuse.org/repositories/home:/in4ops:/Innosense/openSUSE_Leap_15.0 in4ops::Innosense'
in4func_Zypper 'digest'

#  удаление calligra5-extras-okular-3.1.0-337.1.x86_64
# in4func_Zypper "calligra5-extras-dolphin calligra5-extras-okular"


#libqt5-qdbus 

in4func_Zypper 'qmake'


pip3.6 install --user git+https://github.com/natasha/natasha.git
gem.ruby2.5 install --user-install redis
#add alias
