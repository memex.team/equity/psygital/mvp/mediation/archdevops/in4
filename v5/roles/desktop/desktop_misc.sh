echo "ErrorPolicy retry-job" >> /etc/cups/cupsd.conf 

#security
/sbin/yast security level roaming

#X11, display & graphic
ln -s /usr/lib/systemd/system/sddm.service /etc/systemd/system/display-manager.service
systemctl set-default graphical.target 

#SSD tunes
echo 'ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="noop"' > /etc/udev/rules.d/60-sched.rules
echo 'ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="cfq"' >> /etc/udev/rules.d/60-sched.rules
sed -i 's/LABEL=swap/#LABEL=swap/' /etc/fstab

#Network
systemctl stop wicked
systemctl disable wicked
systemctl enable NetworkManager.service 
systemctl start NetworkManager.service 
sed -i 's/ipv6.disable=1//g' /boot/grub2/grub.cfg 

#media
zypper mr -p 10 'non-oss::packman'

#disable coredump
# ln /media/sysdata/in4/platform/v5/landscape/internals--c/management--o/bash_sugar--f/kitchen/dsl/main--s/simple/systemd/coredump.conf
echo 'kernel.core_pattern=|/bin/false' > /etc/sysctl.d/50-coredump.conf

#storage
sed -i 's/#\[Install\]/\[Install\]/' /etc/systemd/system/media-storage.mount
sed -i 's/#WantedBy/WantedBy/'   /etc/systemd/system/media-storage.mount
systemctl enable media-storage.mount 


#final 
zypper dup -y



## ## ## ## ##
## PRE   ##
## ## ## ## ##

vm.overcommit_ratio=1000

#mount encrypted disk
# echo 'storage /dev/sda5 none luks' > /etc/crypttab

#create new user
#useradd -u 1001 -g users -G in4,sysdata,log,systemd-journal -M e.istomin
#echo 'e.istomin     ALL=(ALL) ALL'  > /etc/sudoers.d/e.istomin

#home ln to external disk
#ln -s /media/storage/home /

rm -rf ~/Desktop/ ~/bin

loginctl enable-linger e.istomin

sed -i 's/\/etc\/rsyslog.d\/engine\/instance\/localstore.conf/\/etc\/rsyslog.d\/engine\/instance\/localstore_tmp.conf/g' /etc/systemd/system/in4__rsyslog.service
sed -i 's/PrivateTmp.*/PrivateTmp = false/' /etc/systemd/system/in4__rsyslog.service

/etc/tmpfiles.d/kde_cache.conf
/etc/tmpfiles.d/logs


## ## ## ## ##
##  TO DO   ##
## ## ## ## ##


sed -i "s/vm\.swappiness.*/vm.swappiness=1/" /etc/sysctl.d/memory.conf
vm.laptop_mode = 5

echo 'HISTSIZE=-1' > /etc/profile.local



/home/e.istomin/.local/share/TelegramDesktop/ -> programs-telegram
/home/e.istomin/.config/user-dirs.dirs template

trash /home/e.istomin/.local/share/Trash/ -> tmpfs
ln -s ..tmpfiles .. /etc/tmpfiles.d/ :
/tmp/in4/elastic


####
/home/e.istomin/.local/share/dolphin/
/home/e.istomin/.local/share/konsole/


#semantic
/home/e.istomin_prev/.local/share/plasma_notes/


locale

journald user

aliases for systemctl & journalctl


auditd

fonts - open sans + 

 + khotkeysrc
 
 + krunnerrc
 
 + kwalletrc
 

 /home/e.istomin/.local/share/dolphin/view_properties/global - change in case of sort
 
 meta+x -> how to cut?
 
 
 sudo chmod 744 /etc/ssl/ca-bundle.pem 
