#!/bin/bash

mkdir -p /media/storage/layers/$USER/private/
/sbin/btrfs subvolume create /media/storage/layers/$USER/private/current
mkdir -p /media/storage/layers/$USER/shared/
/sbin/btrfs subvolume create /media/storage/layers/$USER/shared/current
mkdir -p /media/storage/layers/$USER/_system/
/sbin/btrfs subvolume create /media/storage/layers/$USER/_system/current
mkdir -p /media/storage/layers/$USER/_recycle/
/sbin/btrfs subvolume create /media/storage/layers/$USER/_recycle/current

sudo chown -R $USER /media/storage/layers/$USER/
#Private
mkdir /media/storage/layers/$USER/private/current/Data
mkdir /media/storage/layers/$USER/private/current/Software

for files in `ls /media/sysdata/in4/platform/v5/roles/desktop/kde_configs/`; do
    rm ~/.config/$files 
    ln -sf /media/sysdata/in4/platform/v5/roles/desktop/kde_configs/$files ~/.config/
done

# KDE Patches
sudo sed -i 's/Layout.minimumWidth: units.gridUnit.*/Layout.minimumWidth: units.gridUnit * 40/'  /usr/share/plasma/look-and-feel/org.kde.breeze.desktop/contents/runcommand/RunCommand.qml
#


ln -s /tmp/kdecache/ ~/cache
ln -s /tmp/kde_cache/local/klipper ~/.local/share/
ln -s /tmp/kde_cache/local/sddm ~/.local/share/
ln -s /tmp/kde_cache/local/kactivitymanagerd ~/.local/share/
ln -s /tmp/kde_cache/local/RecentDocuments ~/.local/share/
ln -s /tmp/kde_cache/local/Trash ~/.local/share/

ln -s /tmp/kde_cache/config/session ~/.config/sessio


#Semantic
mkdir ~/.local/share/kservices5
ln -s /media/sysdata/in4/platform/v5/roles/desktop/semantic/kde_internals/semantic_finder.desktop ~/.local/share/kservices5/
ln -s /media/sysdata/in4/platform/v5/roles/desktop/semantic/kde_internals/sync.desktop ~/.local/share/kservices5/

mkdir -p ~/../../private/current/Data/_indexed
mkdir -p ~/../../private/current/Data/semantic_db
mkdir -p ~/../../private/current/Data/telegram_chats
mkdir -p ~/../../private/current/Data/telegram_cli
mkdir -p ~/../../private/current/Data/telegram_desktop

#

#Systemd
mkdir -p ~/.config/systemd/user/

ln -s /media/sysdata/in4/platform/v5/roles/desktop/semantic/dbus/in4_dbus.service ~/.config/systemd/user/
systemctl --user enable in4_dbus.service
ln -s /media/sysdata/in4/platform/v5/roles/desktop/semantic/baloo/in4_balooMount.service ~/.config/systemd/user/
systemctl --user enable in4_balooMount.service

ln -s /media/sysdata/in4/platform/v5/roles/desktop/semantic/telegram/in4_telegram_cli@.service ~/.config/systemd/user/
ln -s /media/sysdata/in4/platform/v5/roles/desktop/semantic/telegram/in4_telegram_chats_sync.service ~/.config/systemd/user/
ln -s /media/sysdata/in4/platform/v5/roles/desktop/semantic/redis/in4_semantic_db@.service ~/.config/systemd/user/

#ruby ~/../../private/current/Software/telegram/telegram-history-dump/telegram-history-dump.rb -c ~/../../private/current/Data/telegram_chats/config.yaml
##



#Subvolumes




#Data
rm -rf ~/.local/share/baloo && mkdir -p ~/../../private/current/Data/baloo && ln -s ~/../../private/current/Data/baloo ~/.local/share/
rm -rf ~/.local/share/kwalletd && mkdir -p ~/../../private/current/Data/kwalletd && ln -s ~/../../private/current/Data/kwalletd ~/.local/share/
rm -rf ~/.config/BraveSoftware/Brave-Browser && mkdir -p ~/../../private/current/Data/brave && mkdir ~/.config/BraveSoftware && ln -s ~/../../private/current/Data/brave ~/.config/BraveSoftware/Brave-Browser
rm -rf ~/.config/chromium && mkdir -p ~/../../private/current/Data/chromium && ln -s ~/../../private/current/Data/chromium  ~/.config/
rm -rf ~/.mozilla && mkdir -p ~/../../private/current/Data/firefox && ln -s ~/../../private/current/Data/firefox ~/.mozilla

mkdir -p ~/.local/share/applications/
ln -s '~/../../private/current/Data/telegram_desktop/Telegram - A.Klimova.desktop' ~/.local/share/applications/
#

##git
#.local/share/konsole/
#.config/plasma-localerc
#.config/okular-generator-popplerrc
#.config/mimeapps.list
#.config/kwriterc
#.config/kscreenlockerrc
#.config/krunnerrc
#.config/konsolerc
#.config/kmetainformationrc
#.config/klipperrc
#.config/kioslaverc
#.config/khotkeysrc
#.config/kglobalshortcutsrc
#.config/kded5rc
#.config/katerc
#.config/baloorc
#.config/
#.config/
#.config/


##check
#.config/user-dirs.dirs
#.config/plasma-org.kde.plasma.desktop-appletsrc
#sieveeditorrc
#.local/share/mime/*
#.local/share/user-places.xbel
#.local/share/
#.local/share/
