! in4func_ZypperRepo "add" "http://download.opensuse.org/distribution/leap/15.0/repo/non-oss non-oss::stable"
! in4func_ZypperRepo "add" "http://download.opensuse.org/update/leap/15.0/non-oss update_non-oss::stable"
! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/network:/chromium/openSUSE_Leap_15.0 network::chromium"
! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/mozilla/openSUSE_Leap_15.0 mozilla"
! in4func_ZypperRepo "add" "http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_15.0 non-oss::packman"
! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/multimedia:/apps/openSUSE_Leap_15.0 multimedia::apps"
! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/multimedia:/libs/openSUSE_Leap_15.0 multimedia::libs"
! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/LibreOffice:/6.0/openSUSE_Leap_15.0 LibreOffice:6.0"

#x11
in4func_Zypper "xcb"

#fonts
in4func_Zypper "xorg-x11-fonts  free-ttf-fonts adobe-sourcecodepro-fonts adobe-sourcehansans-fonts adobe-sourcesanspro-fonts adobe-sourceserifpro-fonts dejavu-fonts yast2-fonts"

#some old-school
in4func_Zypper "termcap terminfo"

#security
in4func_Zypper "yast2-users cryptsetup polkit-default-privs signon-kwallet-extension"

#yast2-ca-management - needs build !!!  - https://forums.opensuse.org/showthread.php/531653-YAST-CA-not-available-anymore-very-disappointing!

#remote desktop
in4func_Zypper "remmina remmina-plugin-rdp remmina-plugin-vnc remmina-plugin-xdmcp remmina-plugin-spice"
#rdesktop

#console colors
in4func_Zypper "powerline"

#sound
in4func_Zypper "pulseaudio pulseaudio-utils"

#browsers
in4func_Zypper "MozillaFirefox"
in4func_Zypper "chromium"

#network
in4func_Zypper "yast2-network iw"
in4func_Zypper "wireshark wireshark-ui-qt"

#web dev
in4func_Zypper "filezilla"

#browsers - adv
in4func_Zypper "MozillaFirefox-branding-upstream"
in4func_Zypper "chromium-ffmpeg-extra"

#flash
in4func_Zypper "flash-player flash-player-ppapi pepper"

#media
in4func_Zypper "dragonplayer vlc vlc-codecs"

#codecs
in4func_Zypper "ffmpeg lame libxmp4 ffmpeg x265 x264 libvorbis flac mpg123 libdv libopus0 libmpeg2-0 faac faad2 lame twolame wavpack libtheora"

#sound
in4func_Zypper "alsa alsa-plugins-pulse"
in4func_Zypper "pulseaudio-utils pulseaudio-module-x11 pulseaudio-module-jack pulseaudio-equalizer pulseaudio-bash-completion pavucontrol pavumeter pavucontrol-qt phonon4qt5-backend-vlc"

#screencast
in4func_Zypper "vokoscreen"

#alarm & notification
in4func_Zypper "kalarm"

#printer
in4func_Zypper "cups yast2-printer cups-pdf cups-filters hplip hplip-hpijs"

#filesystems
in4func_Zypper "ntfs-3g fuse fuse-exfat exfat-utils samba-client"

#email misc
in4func_Zypper "cyrus-sasl cyrus-sasl-crammd5 cyrus-sasl-plain cyrus-sasl-digestmd5 cyrus-sasl-gssapi"

#boot screen ui
in4func_Zypper "plymouth plymouth-branding-openSUSE plymouth-dracut plymouth-theme-breeze"

#desktop energy
in4func_Zypper "powertop redshift plasmoid-redshift"   

#libreoffice
in4func_Zypper "libreoffice libreoffice-writer libreoffice-math libreoffice-l10n-ru libreoffice-impress libreoffice-calc libreoffice-base libreoffice-kde4 libreoffice-draw libreoffice-writer-extensions"
in4func_Zypper "unoconv"
chmod -R 755 /usr/lib64/libreoffice

#USB modems
in4func_Zypper "usbutils ModemManager mobile-broadband-provider-info modem-manager-gui"


#dev
in4func_Zypper "osc python-keyring"
in4func_Zypper "build obs-service-* make cmake gcc-c++ glibc-devel-static"

#spice-client
## skype teamviewer
#chromium-desktop-kde
# libasound2-32bit libXv1-32bit libXss1-32bit libqt4-x11-32bit libQtWebKit4-32bit glibc-locale-32bit
#powerstat
#gstreamer-0_10-plugins-ffmpeg
