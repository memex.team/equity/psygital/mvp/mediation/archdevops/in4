#
# spec file for package kdav2
#
# Copyright (c) 2018 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
##

Summary:        A DAV protocol implementation with KJobs

Group:          Productivity/Networking/Other
License:        GPL-2.0
URL:            https://projects.kde.org/projects/kde/pim/kdav2

BuildRequires:  fdupes
BuildRequires:  boost-devel
BuildRequires:  cmake >= 2.8.12
BuildRequires:  extra-cmake-modules
BuildRequires:  kcoreaddons-devel
BuildRequires:  python
BuildRequires:  libqt5-qtbase-devel
BuildRequires:  pkgconfig(Qt5XmlPatterns)

%description
%{summary}.
Package spec for kdav2

%package -n libKPimKDAV2-5
Summary:        C++ library for controlling asynchronous tasks
Group:          System/Libraries
Provides:       %{name} = %{version}

%description -n libKPimKDAV2-5
%{summary}.
System libraries for kdav2

%package devel
Summary:        Development headers for kdav2
Group:          Development/Libraries/C and C++
Requires:       libKPimKDAV2-5 = %{version}

%description devel
%{summary}.
Development headers for kdav2

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

%{cmake}
make %{?_smp_mflags}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

%cmake_install
%fdupes /usr/lib64/cmake/KPimKDAV2

%post -n libKPimKDAV2-5 -p /sbin/ldconfig

%postun -n libKPimKDAV2-5 -p /sbin/ldconfig

%files -n libKPimKDAV2-5
%doc
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/xdg/kdav2.categories
%{_includedir}/KPim/
%{_includedir}/KPim/kdav2/
%{_includedir}/KPim/KDAV2/
%{_includedir}/KPim/kpimkdav2_version.h
%{_libdir}/cmake/KPimKDAV2/
%{_libdir}/*.so
%{_libdir}/cmake/KPimKDAV2/
%{_libdir}/qt5/mkspecs/modules/qt_kdav2.pri


%changelog
* Wed Jun 19 04:34:43 UTC 2019 - Eugene Istomin
- initial
