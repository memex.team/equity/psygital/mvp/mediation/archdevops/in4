########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########


## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

Summary:        Library to assist working with IMAP servers

Group:          Productivity/Networking/Other
License:        GPL-2.0+
URL:            https://projects.kde.org/projects/kde/pim/kimap2

BuildRequires:  fdupes
BuildRequires:  extra-cmake-modules
BuildRequires:  libqt5-qtbase-devel
BuildRequires:  boost-devel
BuildRequires:  cyrus-sasl-devel
BuildRequires:  kcodecs-devel
BuildRequires:  kcoreaddons-devel >= 5.15
BuildRequires:  kmime-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
%{summary}.
Package spec for kimap2

%package -n libKIMAP2-0
Summary:        C++ library for controlling asynchronous tasks
Group:          System/Libraries
Provides:       %{name} = %{version}

%description -n libKIMAP2-0
%{summary}.
System libraries for kimap2

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries/C and C++
Requires:       libKIMAP2-0 = %{version}

%description    devel
%{summary}.
Development headers for kimap2

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

%{cmake}
make %{?_smp_mflags}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

%cmake_install
%fdupes %buildroot

%post -n libKIMAP2-0 -p /sbin/ldconfig
%postun -n libKIMAP2-0 -p /sbin/ldconfig

%files -n libKIMAP2-0
%{_libdir}/libKIMAP2.so.*

%files devel
%{_includedir}/kimap2test
%{_includedir}/kimap2_version.h
%{_includedir}/kimap2
%{_includedir}/KIMAP2
%{_includedir}/kimap2test/*.h
%{_libdir}/libKIMAP2.so
%{_libdir}/cmake/KIMAP2
%{_libdir}/libkimap2test.a
%{_libdir}/qt5/mkspecs/modules/qt_KIMAP2.pri

%changelog
* Wed Jun 19 04:34:43 UTC 2019 - Eugene Istomin
- initial
