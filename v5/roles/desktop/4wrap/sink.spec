#
# spec file for package sink
#
# Copyright (c) 2018 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#
## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //


Summary:        An offline-caching, synchronization and indexing system for PIM data

Group:          Productivity/Networking/Other
License:        GPL-2.0
URL:            https://projects.kde.org/projects/kde/pim/sink
Patch0:         sink-no-return-in-nonvoid-function.patch

BuildRequires:  cmake >= 2.8.12
BuildRequires:  extra-cmake-modules
BuildRequires:  flatbuffers-devel >= 1.4
BuildRequires:  gcc-c++
BuildRequires:  kasync-devel
BuildRequires:  kcoreaddons-devel
BuildRequires:  kcontacts-devel
BuildRequires:  kmime-devel
BuildRequires:  kcalcore-devel
BuildRequires:  kimap2-devel >= 0.2.0
BuildRequires:  kdav2-devel >= 0.2.0
BuildRequires:  libcurl-devel
BuildRequires:  libgit2-devel
BuildRequires:  lmdb-devel
BuildRequires:  libqt5-qtbase-devel
BuildRequires:  pkgconfig(xapian-core) >= 1.4
BuildRequires:  pkgconfig(libsasl2)
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
%{summary}.
Package spec for sink.

%package -n libsink0
Summary:        C++ library for controlling asynchronous tasks
Group:          System/Libraries
Provides:       lib%{name}0 = %{version}

%description -n libsink0
%{summary}.
System libraries for sink

%package devel
Summary:        Development headers for sink
Group:          Development/Libraries/C and C++
Requires:       %{name} = %{version}-%{release}
Requires:       lib%{name}0 = %{version}

%description devel
%{summary}.
Development headers for sink.

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //
#%patch0 -p1

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

%{cmake} \
    -DQT_PLUGIN_INSTALL_DIR:PATH=%{_libdir}/qt5/plugins/
make %{?_smp_mflags}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

%cmake_install

%post -n libsink0 -p /sbin/ldconfig

%postun -n libsink0 -p /sbin/ldconfig

%files
%doc
%{_bindir}/*
%{_libdir}/liblibhawd.so
%dir %{_libdir}/qt5/plugins/
%{_libdir}/qt5/plugins/sink/

%files -n libsink0
%{_libdir}/libsink.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/sink/
%{_libdir}/cmake/Sink
%{_libdir}/libsink.so

%changelog
* Wed Jun 19 04:34:43 UTC 2019 - Eugene Istomin
- initial
