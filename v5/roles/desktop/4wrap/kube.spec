#
# spec file for package kube
#
# Copyright (c) 2018 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#
## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

Summary:        A modern groupware client based on QtQuick and Sink

Group:          Productivity/Networking/Other
License:        GPL-2.0
URL:            https://projects.kde.org/projects/kde/pim/kube
BuildRequires:  boost-devel
BuildRequires:  cmake >= 2.8.12
BuildRequires:  extra-cmake-modules
BuildRequires:  gcc-c++
BuildRequires:  kasync-devel
BuildRequires:  kpackage-devel
BuildRequires:  kcodecs-devel
BuildRequires:  kmime-devel
BuildRequires:  kcontacts-devel
BuildRequires:  kcalcore-devel
BuildRequires:  lmdb-devel
BuildRequires:  libqt5-qtbase-devel
BuildRequires:  libqt5-qtdeclarative-devel
BuildRequires:  sink-devel
BuildRequires:  libcurl-devel
BuildRequires:  gpgme-devel
BuildRequires:  pkgconfig(xapian-core) >= 1.4
BuildRequires:  pkgconfig(Qt5WebEngineWidgets)
BuildRequires:  pkgconfig(Qt5QuickControls2)

#Requires:       libqt5-qtquickcontrols2
#Requires:       libQt5Svg5
Requires:       sink >= 0.6
Requires:       gpgme >= 1.8.0
#Requires:       google-noto-sans-fonts
Requires:       noto-sans-fonts

%description
%{summary}.
kube.kde.org

%package devel
Summary:        Development headers for kube
Group:          Development/Libraries/C and C++
Requires:       %{name} = %{version}

%description devel
%{summary}.
Development headers for kube

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

%{cmake} \
    -DQML_INSTALL_DIR:PATH=%{_libdir}/qt5/qml/
make %{?_smp_mflags}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

%cmake_install
chmod -x %buildroot%{_datadir}/applications/org.kde.kube.desktop

%files
%doc
%{_bindir}/kube
%dir %{_datadir}/kube/
%dir %{_libdir}/qt5/
%dir %{_libdir}/qt5/qml/
%dir %{_libdir}/qt5/qml/org/
%{_libdir}/qt5/qml/org/kube/
%exclude %{_libdir}/qt5/qml/org/kube/framework/tests
%{_libdir}/libkubeframework.so
%{_datadir}/appdata/org.kde.kube.appdata.xml
%{_datadir}/applications/org.kde.kube.desktop
%{_datadir}/kube/kube-icons.rcc
%dir %{_datadir}/icons/hicolor/16x16/
%dir %{_datadir}/icons/hicolor/32x32/
%dir %{_datadir}/icons/hicolor/48x48/
%dir %{_datadir}/icons/hicolor/128x128/
%dir %{_datadir}/icons/hicolor/256x256/
%dir %{_datadir}/icons/hicolor/256x256/apps/
%{_datadir}/icons/hicolor/16x16/apps/kube_icon.png
%{_datadir}/icons/hicolor/32x32/apps/kube_icon.png
%{_datadir}/icons/hicolor/48x48/apps/kube_icon.png
%{_datadir}/icons/hicolor/128x128/apps/kube_icon.png
%{_datadir}/icons/hicolor/256x256/apps/kube_icon.png
%{_datadir}/icons/hicolor/scalable/apps/kube_icon.svg
%{_datadir}/icons/hicolor/scalable/apps/kube_logo.svg
%{_datadir}/icons/hicolor/scalable/apps/kube_symbol.svg

%files devel
%{_libdir}/qt5/qml/org/kube/framework/tests

%changelog
* Wed Jun 19 04:34:43 UTC 2019 - Eugene Istomin
- initial
