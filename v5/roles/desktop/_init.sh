 #!/bin/bash
set -e
## PRE - ENV ##
. /media/sysdata/in4/platform/in4_core/internals/env/common_env.sh
## PRE - FUNCTIONS ##
. $In4CorePath/internals/helpers/in4func.sh
. /etc/os-release

. /media/sysdata/in4/platform/v5/roles/desktop/gui_minimal.sh
. /media/sysdata/in4/platform/v5/roles/desktop/desktop_base.sh
. /media/sysdata/in4/platform/v5/roles/desktop/desktop_kde_base.sh
. /media/sysdata/in4/platform/v5/roles/desktop/desktop_semantic.sh
