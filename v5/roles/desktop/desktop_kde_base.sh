! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/KDE:/Applications/KDE_Frameworks5_openSUSE_Leap_15.0 KDE::Applications"
! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/KDE:/Extra/openSUSE_Leap_15.0 KDE::Extra"

#base
in4func_Zypper "systemsettings5 kf5-filesystem kcm_sddm"
in4func_Zypper "kwalletd5 kwalletmanager5 libsvn_auth_kwallet-1-0 "
in4func_Zypper "patterns-openSUSE-kde_yast"

#apps
in4func_Zypper "dolphin dolphin-part-lang konsole spectacle okular okular-lang kmail5 kcalc kaddressbook5 gwenview5 kate kwrite kontact5 kdesvn kdiff3 kmail kmail-account-wizard kdiff3"
in4func_Zypper "ksshaskpass5 kgpg pam_kwallet"

#plasma
in4func_Zypper "plasma5-addons discover-plasmoid plasma-nm5-l2tp plasma-nm5-openconnect plasma-nm5-openvpn plasma-nm5-ssh plasma-nm5-pptp plasma-nm5-vpnc plasma-theme-oxygen plasma5-mediacenter plasma5-pa plasmoid-activitymanager"


