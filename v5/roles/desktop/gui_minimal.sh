! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/KDE:/Qt5/openSUSE_Leap_15.0 KDE::Qt5"
! in4func_ZypperRepo "add" "http://download.opensuse.org/repositories/KDE:/Frameworks5/openSUSE_Leap_15.0 KDE::Frameworks5"

#x11 + minimal xf86
in4func_Zypper "xf86-input-keyboard xf86-input-mouse xf86-input-libinput xf86-video-intel xorg-x11-server xorg-x11"

#sddm + env
in4func_Zypper "sddm breeze plasma5-workspace plasma5-session"

#openbox
