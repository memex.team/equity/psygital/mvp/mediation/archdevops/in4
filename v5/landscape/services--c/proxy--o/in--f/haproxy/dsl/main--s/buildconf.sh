#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

v5Path="landscape/services--c/proxy--o/in--f/haproxy"
mkdir -p /media/sysdata/in4/context/_generated/$v5Path
Path="/media/sysdata/in4/platform/v5/$v5Path/dsl/main--s/templates/"
Hostname=`hostname -f`
HostnameShort=`hostname`
PeerPort=1010

logALL='\"bytesSrv2Cli\":%B,\ \"haproxyHost\":\"%H\",\ \"timeTCPBeConn\":%Tc,\ \"timeTCPFullTransmit\":%Td,\ \"timeTCPHshake\":%Th,\ \"unixtime\":%Ts%ms,\ \"timeTCPFull\":%Tt,\ \"timeWaitInQ\":%Tw,\ \"bytesCli2Srv\":%U,\ \"procConnCurr\":%ac,\ \"beName\":\"%b\",\ \"beConnCurr\":%bc,\ \"beIP\":\"%bi\",\ \"bePortS\":\"%bp\",\ \"beQ\":%bq,\ \"cliIP\":\"%ci\",\ \"cliPort\":%cp,\ \"feName\":\"%f\",\ \"feConnCurr\":%fc,\ \"feIP\":\"%fi\",\ \"fePort\":%fp,\ \"feTransport\":\"%ft\",\ \"feLogCount\":%lc,\ \"PID\":%pid,\ \"retries\":%rc,\ \"sessCount\":%rt,\ \"srvName\":\"%s\",\ \"srvConCurr\":%sc,\ \"srvIP\":\"%si\",\ \"srvPortS\":\"%sp\",\ \"srvQ\":%sq,\ \"termState\":\"%ts\",\ \"hDate\":\"%t\",\ \"perfCPU\":%[cpu_ns_tot],\ \"perfLat\":%[lat_ns_tot]'
logTCP='\"cliRTT\":%[fc_rtt],\ \"cliRTTVar\":%[fc_rttvar],\ \"cliRetr\":%[fc_retrans],\ \"cliLost\":%[fc_lost]'
LogSSL='\"sslBe\":%[ssl_bc],\ \"sslBeReuse\":%[ssl_bc_is_resumed],\ \"sslBeProto\":\"%[ssl_bc_protocol]\",\ \"ssl\":%[ssl_fc],\ \"sslCiphers\":\"%sslc\",\ \"sslVer\":\"%sslv\",\ \"sslReuse\":%[ssl_fc_is_resumed],\ \"sslProto\":\"%[ssl_fc_protocol]\",\ \"sslSNI\":\"%[ssl_fc_sni]\",\ \"sslCliCertUsed\":\"%[ssl_c_used]\",\ \"sslCliCertCN\":\"%[ssl_c_s_dn(CN)]\"'
LogHTTP='\"id\":\"%ID\",\ \"HTTPstatus\":%ST,\ \"timeHTTPProccess\":%Ta,\ \"timeHTTPIdle\":%Ti,\ \"timeHTTPFullReq\":%Tq,\ \"timeHTTPReqHead\":%TR,\ \"timeHTTPRespHead\":%Tr,\ \"HTTPMethod\":\"%HM\",\ \"HTTPUriPath\":\"%[capture.req.hdr(7)]\",\ \"HTTPUriQuery\":\"%HQ\",\ \"HTTPVer\":\"%HV\",\ \"HTTPReqHost\":\"%[capture.req.hdr(0)]\",\ \"HTTPReferrer\":\"%[capture.req.hdr(5)]\",\ \"HTTPReqUA\":\"%[capture.req.hdr(1)]\",\ \"HTTPReqForw4\":\"%[capture.req.hdr(2)]\",\ \"HTTPTransactionID\":\"%[capture.req.hdr(8),word(2,-)]\",\ \"HTTPSpanID\":\"%[capture.req.hdr(8),word(3,-)]\"'


for Conf in $(ls /media/sysdata/in4/context/_me/$v5Path/*/*.ha); do 
  Type=`echo $Conf|cut -d '/' -f 12`
  Name=`echo $Conf|cut -d '/' -f 13|cut -d '.' -f 1`
  if [[ $Type == 'socket_http' ]]; then
    logHeader='\"event_family\":\"http\",\ \"event_genus\":%ST'  
    LogFormat="in4:\ {\ $logHeader,$logALL,$LogHTTP,$LogSSL\ }"
  
    cat $Path/global.ha             >   /tmp/haproxy_${Type}_${Name}.ha  
    cat $Path/bind_ciphers          >>  /tmp/haproxy_${Type}_${Name}.ha
    . $Path/${Type}/1.defaults.ha >>  /tmp/haproxy_${Type}_${Name}.ha    
    cat $Path/${Type}/2.fe_start.ha >>  /tmp/haproxy_${Type}_${Name}.ha
    cat $Conf                       >>  /tmp/haproxy_${Type}_${Name}.ha
    cat $Path/${Type}/3.fe_end      >>  /tmp/haproxy_${Type}_${Name}.ha
    
    BEPath="${Conf%???}.be/"
    for BE in $(ls $BEPath/*.be); do 
      cat $BE                      >>  /tmp/haproxy_${Type}_${Name}.ha  
    done    
  else
    logHeader='\"event_family\":\"tcp\",\ \"event_genus\":\"haproxy\"'
    LogFormat="in4:\ {\ $logHeader,$logALL,$logTCP,$LogSSL\ }"

    cat $Path/global.ha             >   /tmp/haproxy_${Type}_${Name}.ha  
    cat $Path/bind_ciphers          >>  /tmp/haproxy_${Type}_${Name}.ha
    . $Path/${Type}/1.defaults.ha >>  /tmp/haproxy_${Type}_${Name}.ha
    cat $Conf                       >>  /tmp/haproxy_${Type}_${Name}.ha  
  fi

  LogFormat=`echo $LogFormat|sed -e "s|\"|\\\\\\\\\"|g"`
  echo $LogFormat
  sed -i "s/LOGFORMAT/${LogFormat}/g" /tmp/haproxy_${Type}_${Name}.ha   
  sed -i "s/TYPE/${Type}/g" /tmp/haproxy_${Type}_${Name}.ha   
  sed -i "s/HOSTNAME_SERVICE/${Hostname}_${Type}_${Name}/g" /tmp/haproxy_${Type}_${Name}.ha 
  sed -i "s/HOSTNAME_SHORT/$HostnameShort/g" /tmp/haproxy_${Type}_${Name}.ha  
  sed -i "s/HOSTNAME/$Hostname/g" /tmp/haproxy_${Type}_${Name}.ha  
  sed -i "s/SERVICE/${Type}_${Name}/g" /tmp/haproxy_${Type}_${Name}.ha
  sed -i "s/PeerPort/$PeerPort/g" /tmp/haproxy_${Type}_${Name}.ha
  ((PeerPort++))
  
  chown haproxy:haproxy /tmp/haproxy_${Type}_${Name}.ha
  /usr/sbin/haproxy -c -f /tmp/haproxy_${Type}_${Name}.ha
  
  
  if [[ `id -u` == 0 ]]; then
    Prefix=''
  else
    Prefix=' --user '
  fi
  cp /tmp/haproxy_${Type}_${Name}.ha /media/sysdata/in4/context/_generated/$v5Path/${Type}_${Name}.ha
  chown haproxy:haproxy /media/sysdata/in4/context/_generated/$v5Path/${Type}_${Name}.ha
  ! systemctl $Prefix reload in4_haproxy@${Type}_${Name}
done 
