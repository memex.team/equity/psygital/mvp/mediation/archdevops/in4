#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

~/.acme.sh/acme.sh --renew-all

CertPath='/media/sysdata/in4/context/secure/certs/external'
find ~/.acme.sh/ -type d -name '*\.*' -maxdepth 1| sed 's#.*/##'|
while read Domain
do
  if [[ ! -z "$Domain" ]] ; then
    cp ~/.acme.sh/$Domain/fullchain.cer $CertPath/$Domain.pem
    cp ~/.acme.sh/$Domain/$Domain.key $CertPath/$Domain.key
    
    cat ~/.acme.sh/$Domain/fullchain.cer > $CertPath/both/$Domain.both
    cat ~/.acme.sh/$Domain/$Domain.key  >> $CertPath/both/$Domain.both
  fi
done
