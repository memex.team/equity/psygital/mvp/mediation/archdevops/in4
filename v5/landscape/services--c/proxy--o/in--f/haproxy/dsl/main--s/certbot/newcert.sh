#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

mkdir -p /media/sysdata/in4/context/secure/certs/external
Hostname=`hostname -f`
PeerPort=1010
for Conf in $(ls /media/sysdata/in4/context/_me/landscape/services--c/proxy--o/in--f/haproxy/hosts/*); do 
  ParentHostname=`echo $Conf|cut -d '/' -f 13`
  CerbotHosts=""
  for host in $(cat $Conf); do
    CerbotHosts="$CerbotHosts -d $host "
  done
  certbot certonly -n --agree-tos -m notify@$ParentHostname --standalone --http-01-port 89 --rsa-key-size 2048  --preferred-challenges http --server https://acme-v02.api.letsencrypt.org/directory $CerbotHosts
   
  cat /etc/certbot/live/$ParentHostname/fullchain.pem > /media/sysdata/in4/context/secure/certs/external/$ParentHostname.both
  cat /etc/certbot/live/$ParentHostname/privkey.pem >> /media/sysdata/in4/context/secure/certs/external/$ParentHostname.both
done
 
