#!/bin/bash 
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

in4func_run "services--c--security--o--firewall--f--firewalld--g--main--s" "2_init/opensuse" "firewalld.package.zypper.sh"

in4func_systemd "services--c--security--o--firewall--f--firewalld--g--main--s" "add" "service" "in4__firewalld"
in4func_systemd "services--c--security--o--firewall--f--firewalld--g--main--s" "enable" "service" "in4__firewalld"

in4func_ln "dsl::services--c--security--o--firewall--f--firewalld--g--main--s" "conf/firewalld.conf" "/media/sysdata/app/firewall/"
systemctl disable firewalld && systemctl mask firewalld
