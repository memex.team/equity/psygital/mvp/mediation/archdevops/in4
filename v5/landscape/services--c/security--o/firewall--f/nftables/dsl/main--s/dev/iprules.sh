#!/bin/sh
#/sbin/ip rule add from all table main pref 32766
#/sbin/ip rule add from all table default pref 32767

/sbin/ip route add default via {DEF_GW}
/sbin/ip rule add from 10.0.0.0/8 table {EXT_INTERFACE1_NAME} pref 30000

## add get from ip a

/sbin/ip route add {EXT_INTERFACE1_SUBNET} via {EXT_INTERFACE1_MAIN_IP} dev net{EXT_INTERFACE1_NAME} table {EXT_INTERFACE1_NAME}
/sbin/ip route add default via {EXT_INTERFACE1_DEF_GW} dev {EXT_INTERFACE1_NAME} src {EXT_INTERFACE1_MAIN_IP} table {EXT_INTERFACE1_NAME}
/sbin/ip rule add from {EXT_INTERFACE1_SUBNET} table {EXT_INTERFACE1_NAME} pref 10000
/sbin/ip rule add fwmark {EXT_INTERFACE1_NAME_INTEGER} table {EXT_INTERFACE1_NAME} pref 10000

/sbin/ip route add {INT_INTERFACE1_SUBNET} via {INT_INTERFACE1_MAIN_IP} dev net{INT_INTERFACE1_NAME} table net{INT_INTERFACE1_NAME}
/sbin/ip rule add to {INT_INTERFACE1_SUBNET} table net{INT_INTERFACE1_NAME} pref 5000
/sbin/ip rule add fwmark {INT_INTERFACE1_NAME_INTEGER} table net{INT_INTERFACE1_NAME} pref 5000

