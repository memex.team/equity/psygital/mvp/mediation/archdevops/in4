  ########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

License:       GPL-2.0
Group:         Productivity/Networking/Security
URL:           http://suricata-ids.org/
Summary:       High performance Network IDS, IPS and Network Security Monitoring engine

# Install-time parameters
%if 0%{?suse_version}
Recommends:    suricata-rules
%else
Requires:      suricata-rules
%endif

# Build-time parameters
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: pkg-config python
BuildRequires: pcre-devel libyaml-devel libhtp2-devel
BuildRequires: libnfnetlink-devel libnetfilter_queue-devel libnetfilter_log-devel
BuildRequires: libnet-devel libpcap-devel libcap-ng-devel libGeoIP-devel zlib-devel
BuildRequires: file-devel
BuildRequires: hiredis-devel
BuildRequires: pkgconfig(nspr)
BuildRequires: pkgconfig(nss)
BuildRequires: libjansson-devel
BuildRequires: pkgconfig(luajit)
BuildRequires: pkgconfig(libnetfilter_queue)
BuildRequires: python-Sphinx python-setuptools

# libcap-ng0 make libmagic-dev git-core libnetfilter-queue-dev 
# libnetfilter-queue1 libnss3-dev libnspr4-dev  libhyperscan-dev
# BuildRequires: netmap-devel

BuildRoot:     %{_tmppath}/%{name}-root

%description
Suricata is a high performance Network IDS, IPS and Network Security Monitoring
engine. Open Source and owned by a community run non-profit foundation,
the Open Information Security Foundation (OISF).

%package -n suricatasc
Version:       %{_my_release_version}
Group:         Productivity/Networking/Security
Summary:       Client for Suricata unix socket
Requires:      suricata
Requires:      python%{?suse_version:-base}
Provides:      python-suricatasc = %{_my_release_version}

%description -n suricatasc
Command-line client for Suricata unix socket.


%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

./autogen.sh

%configure \
 --disable-static \
 --enable-python \
 --enable-gccprotect \
 \
 --enable-nfqueue \
 --enable-luajit \
 --enable-geoip \
 --enable-pie \
 --enable-hiredis \
 --enable-non-bundled-htp \
 --enable-geoip 
#  CFLAGS="%{optflags} %{gcc_lto}" \
#  CXXFLAGS="%{optflags} %{gcc_lto}" \
#  LDFLAGS="-Wl,--as-needed -Wl,--strip-all %{gcc_lto}"
%{__make} %{?_smp_mflags}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

%{__make} install DESTDIR=%{buildroot}
%{__install} -D -m644 suricata.yaml         %{buildroot}%{_sysconfdir}/suricata/suricata.yaml
%{__install} -D -m644 classification.config %{buildroot}%{_sysconfdir}/suricata/classification.config
%{__install} -D -m644 reference.config      %{buildroot}%{_sysconfdir}/suricata/reference.config
%{__install} -D -m644 threshold.config      %{buildroot}%{_sysconfdir}/suricata/threshold.config
%{__install} -d -m755 %{buildroot}%{_sysconfdir}/suricata/rules
# This one rules file don't exist in ETOpen ruleset
%{__install}    -m644 rules/dns-events.rules %{buildroot}%{_sysconfdir}/suricata/rules/dns-events.rules
%{__install} -d -m755 %{buildroot}/var/log/suricata/
#%{__install} -d -m755 %{buildroot}/var/run/suricata/
%{__rm} -rf %{buildroot}%{_datadir}/doc/%{name}
%{__python} -c "import sys, os, compileall; br='%{buildroot}'; compileall.compile_dir(sys.argv[1], ddir=br and (sys.argv[1][len(os.path.abspath(br)):]+'/') or None)" %{buildroot}%{python_sitelib}/suricatasc/
[ "%{buildroot}" != "/" ] && %{__rm} -f %{buildroot}%{python_sitelib}/suricatasc/*.py{c,o}
cd %{buildroot}%{python_sitelib}/suricatasc/
python -m compileall *.py
cd -

%clean
# [ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir %{_sysconfdir}/suricata/rules
%dir %{_sysconfdir}/suricata/
%config(noreplace) %{_sysconfdir}/suricata/suricata.yaml
%config(noreplace) %{_sysconfdir}/suricata/classification.config
%config(noreplace) %{_sysconfdir}/suricata/reference.config
%config(noreplace) %{_sysconfdir}/suricata/threshold.config
%config(noreplace) %{_sysconfdir}/suricata/rules/dns-events.rules
%dir /usr/share/suricata
%dir /usr/share/suricata/rules/
/usr/share/suricata/rules/*
/usr/share/man/man1/suricata.1.gz
%{_bindir}/suricata
%{_bindir}/suricatactl
%dir /var/log/suricata/
#%ghost %dir /var/run/suricata/

%files -n suricatasc
%defattr(-,root,root)
%{_bindir}/suricatasc
%dir %{python_sitelib}/suricata
%{python_sitelib}/suricata/*
%{python_sitelib}/suricata-*.egg-info
%dir %{python_sitelib}/suricatasc
%{python_sitelib}/suricatasc/*


%changelog
* Wed Feb 07 2018 Eugene Istomin 
- Initial
