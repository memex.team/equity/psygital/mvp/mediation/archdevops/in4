########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
# Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

Name:           libhtp2
Summary:        HTTP normalizer and parser
License:        GPL-3.0
Group:          Development/Libraries/C and C++
Url:            http://www.openinfosecfoundation.org/
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  zlib-devel

%description
The HTP Library is an HTTP normalizer and parser written by Ivan Ristic of Mod Security fame for the OISF. This integrates and provides very advanced processing of HTTP streams for Suricata. The HTP library is required by the engine, but may also be used independently in a range of applications and tools.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries/C and C++
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

./autogen.sh
%configure --disable-static
make %{?_smp_mflags}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

make %{?_smp_mflags} DESTDIR=%{buildroot} install
find %{buildroot} -type f -name "*.la" -delete -print


%post -n %{name} -p /sbin/ldconfig
%postun -n %{name} -p /sbin/ldconfig

%files -n %{name}
%defattr(-,root,root,-)
%doc
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/htp.pc

%changelog
* Wed Feb 07 2018 Eugene Istomin 
- Initial
