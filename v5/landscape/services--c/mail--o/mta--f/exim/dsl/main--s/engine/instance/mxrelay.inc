###########################
###  INCLUDES                            	###
###########################
    
# GLOBAL FUNC    
    .include ../_globals/retry.conf
    .include ../_globals/transport.conf
#

# ROUTER
    .include ../router/mxrelay.router
#
