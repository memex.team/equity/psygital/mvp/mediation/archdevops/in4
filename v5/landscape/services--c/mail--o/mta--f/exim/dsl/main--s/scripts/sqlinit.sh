#/bin/bash

sqlite_path='/media/sysdata/in4/context/_generated/landscape/services--c/mail--o/mta--f/exim'

mkdir -p $sqlite_path
sqlite3 $sqlite_path/mta.sqlite << EOF

PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

CREATE TABLE domains
(
  id INTEGER PRIMARY KEY,
  name text NOT NULL,
  type text NOT NULL DEFAULT 'hosted',
  rcv_from text NOT NULL DEFAULT 'all',
  CONSTRAINT domains_name_key UNIQUE (name)
);

CREATE TABLE router
(
  id INTEGER PRIMARY KEY,
  src text NOT NULL,
  from_domain integer NOT NULL,
  from_user text NOT NULL,
  to_domain integer NOT NULL,
  to_user text NOT NULL,
  transport text NOT NULL,
  inactive boolean DEFAULT true,
  unique (src, from_domain,from_user,to_domain,to_user,transport,inactive)
  CONSTRAINT router_from_domain_fkey FOREIGN KEY (from_domain)
      REFERENCES domains (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT router_to_domain_fkey FOREIGN KEY (to_domain)
      REFERENCES domains (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE relay_from
(
  id INTEGER PRIMARY KEY,
  src text NOT NULL,
  ip text NOT NULL
);

CREATE TABLE accesslists
(
  id INTEGER PRIMARY KEY,
  src text NOT NULL,
  ip text NOT NULL,
  hdr_from text NOT NULL,
  type text NOT NULL
);

CREATE TABLE greylist
(
  id INTEGER  PRIMARY KEY AUTOINCREMENT,
  relay_ip TEXT default NULL,
  sender_type TEXT CHECK( sender_type IN ('NORMAL','BOUNCE') ) NOT NULL default 'NORMAL',
  sender TEXT default NULL,
  recipient TEXT default NULL,
  block_expires TEXT NOT NULL default '0000-00-00 00:00:00',
  record_expires TEXT NOT NULL default '9999-12-31 23:59:59',
  create_time TEXT NOT NULL default '0000-00-00 00:00:00',
  TYPE TEXT CHECK( TYPE IN ('AUTO','MANUAL') ) NOT NULL default 'MANUAL',
    passcount INTEGER NOT NULL default '0',
  last_pass TEXT NOT NULL default '0000-00-00 00:00:00',
  blockcount INTEGER NOT NULL default '0',
  last_block TEXT NOT NULL default '0000-00-00 00:00:00'
);
CREATE TABLE greylist_log (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  listid INTEGER NOT NULL default '0',
  timestamp TEXT NOT NULL default '0000-00-00 00:00:00',
  kind TEXT CHECK( kind IN ('deferred','accepted') ) NOT NULL default 'deferred'
);
DELETE FROM sqlite_sequence;
COMMIT;


EOF


chmod -R 777 $sqlite_path
