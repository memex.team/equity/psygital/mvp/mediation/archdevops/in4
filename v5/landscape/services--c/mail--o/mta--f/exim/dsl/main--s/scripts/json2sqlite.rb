#!/usr/bin/ruby

require 'json'
require 'sqlite3'
require 'find'


exit 1 if not ARGV[0]
domain = ARGV[0]
jsonPath = "/media/sysdata/in4/context/_me/landscape/services--c/mail--o/mta--f/exim/domains/#{domain}.json"
sqlite_path = '/media/sysdata/in4/context/_generated/landscape/services--c/mail--o/mta--f/exim/mta.sqlite'

sql = Array.new
db = SQLite3::Database.new(sqlite_path)

Find.find(jsonPath) do |json|
  json = JSON.parse(File.read(json))

  domain = json['domain']
  innerDomain = json['innerDomain']

  db.execute("INSERT OR IGNORE INTO domains (name) VALUES ('#{domain}')") 
  domainID = db.execute("SELECT id from domains where name LIKE '#{domain}'")[0][0]
  puts "domainID - #{domainID}"

  db.execute("INSERT OR IGNORE INTO domains (name,type) VALUES ('#{innerDomain}', 'backend')") 
  innerDomainID = db.execute("SELECT id from domains where name LIKE '#{innerDomain}'")[0][0]
  puts "innerDomainID - #{innerDomainID}"

  
  if json.key?('users')
    json['users'].each do |user, userProps|
      puts "User -  #{user}"    
      db.execute("INSERT OR IGNORE INTO router (src,from_domain,from_user,to_domain,to_user,transport) VALUES ('in4_json',#{domainID},'#{user}',#{innerDomainID},'#{user}','lmtp_over_smtp');")
      if userProps["aliases"]
        puts "Aliases -  #{userProps["aliases"]}"
        userProps["aliases"].each do |mail_alias|
          db.execute("INSERT OR IGNORE INTO router (src,from_domain,from_user,to_domain,to_user,transport)  VALUES ('in4_json',#{domainID},'#{mail_alias}',#{innerDomainID},'#{user}','smtp');")
        end
      end
    end
  end
  
  if json.key?('shared')
    json['shared'].each do |mailbox, mailboxProps|
      puts "Shared -  #{mailbox}"
      db.execute("INSERT OR IGNORE INTO router (src,from_domain,from_user,to_domain,to_user,transport) VALUES ('in4_json',#{domainID},'#{mailbox}',#{innerDomainID},'#{mailbox}','lmtp_over_smtp');")
      if mailboxProps["aliases"]
        puts "Aliases -  #{mailboxProps["aliases"]}"
        mailboxProps["aliases"].each do |mail_alias|
          db.execute("INSERT OR IGNORE INTO router (src,from_domain,from_user,to_domain,to_user,transport)  VALUES ('in4_json',#{domainID},'#{mail_alias}',#{innerDomainID},'#{user}','smtp');")
        end
      end
    end  
  end
  
  if json.key?('lists')
    json['lists'].each do |list, types|
      types.each do |type, users|
        users.each do |userFull|
          case type
          when 'internal'
            transport = 'lmtp_over_smtp'
            user = userFull
            domainIDDst = innerDomainID
            
          when 'external'
            transport = 'smtp'
            user   = userFull.split('@')[0]
            domain = userFull.split('@')[1]
            preDomainID = db.execute("SELECT id from domains where name LIKE '#{domain}'")
            db.execute("INSERT OR IGNORE INTO domains (name) VALUES ('#{domain}')") unless preDomainID.is_a?(Hash)
            domainIDDst = preDomainID[0][0]
          end
          
          puts "List #{list}: to user #{user} domain ID #{domainIDDst} transport #{transport}"        
          db.execute("INSERT OR IGNORE INTO router (src,from_domain,from_user,to_domain,to_user,transport)  VALUES ('in4_json',#{domainID},'#{list}',#{domainIDDst},'#{user}','#{transport}');")
        end
      end
    end
  end
  
end
pp sql


# 	db.transaction 
# 		db.execute( "delete from domains" )
# 		db.execute( "delete from accounts" )
# 		db.execute( "delete from aliases" )
# 		db.execute( "delete from destinations" )
# 		db.execute_batch( sql.join(" ") )
# 		db.commit
