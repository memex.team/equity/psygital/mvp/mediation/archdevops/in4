###########################
###  INCLUDES                            	###
###########################


## CLI CONF
    .include /media/sysdata/in4/context/exim/instance/proxy.conf
##    

# ROUTER LOOKUPS
    .include ../router/default_sql.lookup
#    
    
# GLOBAL FUNC    
    .include ../_globals/retry.conf
    .include ../_globals/transport.conf
    .include ../_globals/auth.conf
    .include ../_globals/cli_lists.conf
#

# CLI FUNC
    .include /media/sysdata/in4/context/exim/instance/proxy.func
#



