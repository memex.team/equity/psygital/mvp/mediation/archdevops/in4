########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##


Summary:        DRBD driver for Linux
License:        GPL-2.0
Group:          System/Kernel
Url:            http://drbd.linbit.com/

BuildRequires:  kernel-source
BuildRequires:  kernel-syms
BuildRequires:  module-init-tools
Requires:       drbd-utils >= 2.1

BuildRoot:      %{_tmppath}/%{name}-%{version}-build
Source1:      drbd_git_revision

%kernel_module_package

%description
Drbd is a distributed replicated block device. It mirrors a block
device over the network to another machine. Think of it as networked
raid 1. It is a building block for setting up clusters.

%package KMP
Summary:        Kernel driver for DRBD
Group:          Productivity/Clustering/HA

%description KMP
This module is the kernel-dependent driver for DRBD.  This is split out so
that multiple kernel driver versions can be installed, one for each
installed kernel.

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //


mkdir source
cp -a drbd/. source/. || :
cp $RPM_SOURCE_DIR/drbd_git_revision source/.drbd_git_revision
# cp source/Makefile-2.6 source/Makefile
# WIP cp -a %_sourcedir/Module.supported source/
mkdir obj


%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //
export CONFIG_BLK_DEV_DRBD=m
export EXTRA_CFLAGS='-DVERSION=\"%{version}\"'

for flavor in %flavors_to_build; do
	rm -rf $flavor
	cp -r source $flavor
	export DRBDSRC="$PWD/obj/$flavor"
	make -C %{kernel_source $flavor} modules M=$PWD/$flavor
done


%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs/src
## \\ In4 //

export INSTALL_MOD_PATH=%{buildroot}
export INSTALL_MOD_DIR=updates
for flavor in %{flavors_to_build}; do
    make -C %{kernel_source $flavor} modules_install \
	 M=$PWD/$flavor
done

mkdir -p %{buildroot}/%{_sbindir}
%{__ln_s} -f %{_sbindir}/service %{buildroot}/%{_sbindir}/rc%{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%doc ChangeLog
%{_sbindir}/rc%{name}

%changelog
