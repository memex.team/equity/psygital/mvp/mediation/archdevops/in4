########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
Source1:      drbd_git_revision
#Patch0:        
##

Summary:        Distributed Replicated Block Device
License:        GPL-2.0+
Group:          Productivity/Clustering/HA
Url:            http://www.drbd.org/

BuildRequires:  git
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  bison
BuildRequires:  docbook-xsl-stylesheets
BuildRequires:  flex
BuildRequires:  gcc
BuildRequires:  glibc-devel
BuildRequires:  libxslt
BuildRequires:  make
BuildRequires:  systemd
BuildRequires:  udev
Provides:       drbd-control
Provides:       drbdsetup
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
Requires(post): %insserv_prereq %fillup_prereq
Requires(preun): %insserv_prereq %fillup_prereq
Requires(postun): %insserv_prereq fileutils


%description
Drbd is a distributed replicated block device. It mirrors a block
device over the network to another machine. Think of it as networked
raid 1. It is a building block for setting up clusters.

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //


mkdir source
cp -a drbd/. source/. || :
cp $RPM_SOURCE_DIR/drbd_git_revision source/.drbd_git_revision
# cp source/Makefile-2.6 source/Makefile
# WIP cp -a %_sourcedir/Module.supported source/
mkdir obj

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

./autogen.sh
PATH=/sbin:$PATH ./configure \
    --with-udev \
    --with-distro=suse \
    --without-heartbeat \
    --with-pacemaker \
    --with-bashcompletion \
    --with-initscripttype=systemd \
    --with-systemdunitdir=/usr/lib/systemd/system \
    --prefix=/ \
    --sbindir=/sbin \
    --libdir=/usr/lib \
    --mandir=%{_mandir} \
    --sysconfdir=%{_sysconfdir} \
    --datarootdir=/usr/share \
    --datadir=/usr/share \
    --libdir=/usr/lib \
    --exec_prefix=/usr/lib \
    --with-tmpfilesdir=%{_tmpfilesdir}

make OPTFLAGS="%{optflags}" %{?_smp_mflags}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs/src
## \\ In4 //

%make_install

mkdir -p %{buildroot}%{_localstatedir}/lib/drbd
mkdir -p %{buildroot}/usr/lib/udev/rules.d

%files
%defattr(-, root, root)
%config(noreplace) %{_sysconfdir}/drbd.conf
%config %{_sysconfdir}/bash_completion.d/drbdadm.sh
%config(noreplace) %{_sysconfdir}/drbd.d/global_common.conf
%{_tmpfilesdir}/drbd.conf
%doc %{_mandir}/man5/drbd.*
%doc %{_mandir}/man8/drbd*
%doc COPYING
%doc README
%doc ChangeLog
%doc scripts/drbd.conf.example
%dir %{_sysconfdir}/drbd.d
/sbin/drbdadm
/sbin/drbdsetup
/sbin/drbdmeta
/sbin/drbd-overview
%dir %attr(700,root,root) %{_sysconfdir}/xen
%dir %{_sysconfdir}/xen/scripts
%{_sysconfdir}/xen/scripts/block-drbd
%{_prefix}/lib/ocf/resource.d/linbit/drbd
%dir /usr/lib/udev
%dir /usr/lib/udev/rules.d
 /usr/lib/udev/rules.d/65-drbd.rules
%{_unitdir}/drbd.service
/usr/lib/systemd/system/drbd.service
%defattr(-, root, root)
%{_localstatedir}/lib/drbd
/usr/lib/drbd
/lib/drbd
/lib/drbd/drbdadm*
/lib/drbd/drbdsetup*
%dir %{_prefix}/lib/ocf
%dir %{_prefix}/lib/ocf/resource.d
%dir %{_prefix}/lib/ocf/resource.d/linbit

%changelog
