#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ $In4Context == "v5" ]]; then
    In4ContextPrefix="landscape"
fi


if [[ -z $In4DeployAppClass ]]; then
    DialogMsg="Please specify In4 application class"
    echo $DialogMsg; select In4DeployAppClass in `in4func_findobj "$In4GitPath/$In4Context/$In4ContextPrefix/" 'd' '--c'`;  do  break ; done
fi

if [[ -z $In4DeployAppOrder ]]; then
    DialogMsg="Please specify In4 application order"
    echo $DialogMsg; select In4DeployAppOrder in `in4func_findobj "$In4GitPath/$In4Context/$In4ContextPrefix/$In4DeployAppClass/" 'd' '--o'`;  do  break ; done
fi

if [[ -z $In4DeployAppFamily ]]; then
    DialogMsg="Please specify In4 application family"
    echo $DialogMsg; select In4DeployAppFamily in `in4func_findobj "$In4GitPath/$In4Context/$In4ContextPrefix/$In4DeployAppClass/$In4DeployAppOrder/" 'd' '--f'`;  do  break ; done
fi

if [[ -z $In4DeployAppGenus ]]; then
    DialogMsg="Please specify In4 application genus"
    echo $DialogMsg; select In4DeployAppGenus in `in4func_findobj "$In4GitPath/$In4Context/$In4ContextPrefix/$In4DeployAppClass/$In4DeployAppOrder/$In4DeployAppFamily/" 'd'`;  do  break ; done
fi


### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
