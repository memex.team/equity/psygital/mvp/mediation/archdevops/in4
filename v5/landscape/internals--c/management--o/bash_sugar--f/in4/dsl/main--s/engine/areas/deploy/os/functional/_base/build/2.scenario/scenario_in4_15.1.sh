#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### PREREQ ###
. /tmp/in4_env.sh
. /media/sysdata/in4/platform/in4_core/internals/env/common_env.sh
## PRE - FUNCTIONS ##
. $In4CorePath/internals/helpers/in4func.sh
###

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

in4func_bash_include $In4ExecPath/_base/build/2.scenario/1.init.sh
in4func_bash_include $In4ExecPath/_base/build/2.scenario/2.all-repo_15.1.sh
in4func_bash_include $In4ExecPath/_base/build/2.scenario/3.pre.sh
in4func_bash_include $In4ExecPath/_base/build/2.scenario/4.all-packages.sh
if [[ " ${In4BuildLayers[@]} " =~ " os " ]] && ! [[ $In4DeployOsMode =~ "container_" ]]; then 
  in4func_bash_include $In4ExecPath/_base/build/2.scenario/5.kern-step2_boot.sh; 
fi
in4func_bash_include $In4ExecPath/_base/build/2.scenario/6.all-pre.sh
in4func_bash_include $In4ExecPath/_base/build/2.scenario/7.all-conf_init.sh
if ! [[ $In4DeployOsMode =~ "container_" ]]; then
  in4func_bash_include $In4ExecPath/_base/build/2.scenario/8.mounts.sh
fi
in4func_bash_include $In4ExecPath/_base/build/2.scenario/9.all-common.sh
in4func_bash_include $In4ExecPath/_base/build/2.scenario/10.all-post.sh
in4func_bash_include $In4ExecPath/_base/build/2.scenario/11.clean.sh

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
