#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

In4OfflineCliMode="No"
In4OfflineBuildMode="Yes"
In4BuildLayers=(unit os)
In4ExportType="none"

### DISKS
In4Disk_SysdataOnBaseDisk="Yes"
In4Disk_StorageOnBaseDisk="No"

if [[ -z $In4Disk_BaseDisk ]]; then
    DialogMsg="Please specify disk name for OS install"
    echo $DialogMsg; select In4Disk_BaseDisk in sdb sdc sdd sde sdd;  do  break ; done
fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
