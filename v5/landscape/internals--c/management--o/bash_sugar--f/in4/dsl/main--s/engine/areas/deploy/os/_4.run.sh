#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###
In4ExecPath="$In4CorePath/areas/deploy/os/functional"

if in4func_execute_as_root_wo_tests test -f $In4BuildEnv/images/$In4DeployOsMode/$In4OsBuild* ; then
  if [[ -z $In4OsBuildRebuild ]]; then
      DialogMsg="Image $In4BuildEnv/images/$In4DeployOsMode/$In4OsBuild exists, rebuild?"
      echo $DialogMsg; select In4OsBuildRebuild in yes no;  do  break ; done
  fi
else
  In4OsBuildRebuild='yes'
fi

if [[ $In4OsBuildRebuild == yes ]]; then
  in4func_bash_include $In4ExecPath/1.1.build.sh
fi

#if needs deploy
in4func_bash_include $In4ExecPath/2.deploy.sh

#fi
### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
