#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

in4func_bash_include $In4ExecPath/_base/build/1.init/clean.sh

in4func_execute_as_root cp --sparse=always $In4BuildEnv/${OsBuild}_${OsSrvType}.raw $In4BuildEnv/../
in4func_execute_as_root cp --sparse=always $In4BuildEnv/sysdata.raw $In4BuildEnv/../
in4func_execute_as_root cp --sparse=always $In4BuildEnv/swap.raw $In4BuildEnv/../

VMPATH=$In4BuildEnv

in4func_bash_include $In4CorePath/areas/deploy/os/functional/vm_xen/build/3.env/demo.xl > /tmp/demo.xl
in4func_execute_as_root xl create /tmp/demo.xl
DOMID=`in4func_execute_as_root xl domid demo-hvxen-test`

secs=60  
SECONDS=0 
if [[ $In4Test == "Yes" ]]; then
    echo "Test mode, no test vm"
else
    while (( SECONDS < secs )); do    # Loop until interval has elapsed.
        STATE=`in4func_execute_as_root xenstore-read /local/domain/$DOMID/device/vif/0/state`
        if [[ $STATE == 4 ]]; then break; fi
        sleep 5
    done

    in4func_execute_as_root xl destroy demo-hvxen-test
    if [[ $STATE != 4 ]]; then exit 1; fi
fi
### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
