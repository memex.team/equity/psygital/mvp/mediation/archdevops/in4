#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

Path2In4() {
  FullDir=$1
  declare -A store  
  in4Pre=`echo $FullDir|sed -e 's/\/media\/sysdata\/in4\/platform\/v5\/landscape\///'`
  store["c"]="`echo $in4Pre|cut -d '/' -f 1`"
  store["o"]="`echo $in4Pre|cut -d '/' -f 2`"
  store["f"]="`echo $in4Pre|cut -d '/' -f 3`"
  store["g"]="`echo $in4Pre|cut -d '/' -f 4`"
  store["s"]="`echo $in4Pre|cut -d '/' -f 8`"
  In4OBSBuildProject="${store[c]}--${store[o]}--${store[f]}--${store[g]}"
  In4OBSPackageName="${store[s]}"
}

obsPackageStatus() {
  path=$1
  cd $path
  buildPackage=`echo $path|sed -e "s/.*_localbuild\///"`    
  ! osc blt $openSuseVersion x86_64 -l > $oscTmpFile
  buildFinished=`cat $oscTmpFile |grep "finished \""`
  buildFailed=`cat $oscTmpFile |grep "failed \"build"`
  buildSRPM=`cat $oscTmpFile|grep SRPMS -m 1 `
  buildVersion=`echo $buildSRPM|sed -e "s/.*\/ *//"|cut -d "-" -f 2`
  if [[ -z $buildFailed ]]; then
    buildDate=`echo $buildFinished|sed -e "s/.*at *//" -e "s/\.//"`
  else
    buildDate=`echo $buildFailed|sed -e "s/.*at *//" -e "s/\.//"`  
  fi
  buildUnixtime=`date --date="$buildDate" "+%s"`    
    
 if [[ ! $buildVersion =~ [0-9] ]]; then buildVersion="unknown"; fi
    echo "## Package - $buildPackage - Ver. $buildVersion"    

    if [[ $buildUnixtime == ?(-)+([0-9]) ]]; then
        if [[ $(($currentUnixtime - $buildUnixtime)) -gt 432000 ]] || [[ $buildFinished == "" ]]; then
            echo "Need upd - $buildPackage, last update - $buildDate"
            if [ -f "$path/_service" ]; then
                in4func_execute_as_is osc service rr
                echo "Service update trigger was sent, new package will be builded"
            fi
            #osc service rebuild
        else
            echo "Already updated - $buildPackage, last update - $buildDate"    
        fi
    else
        echo "$buildUnixtime is not the number in package $buildPackage. Build string  - $buildFinished"
    fi
    echo  -e "\n"
}

in4obs()
{
  declare -A exists
  for Dir in `ls -d $In4GitPath/v5/landscape/*/*/*/*/in4/1_build/obs/`; do
    echo -e "\n In4 G: $Dir\n"
    Path2In4 $Dir
    
    if curl -s --head  --request GET "https://build.opensuse.org/project/show/home:$In4OBSBuildURIAuthor:$In4OBSBuildProject" | grep "200 OK" > /dev/null; then  
      echo "Repo \"$In4OBSBuildProject\" exists"
    else
      echo "Repo \"$In4OBSBuildProject\" does't exists, creating"
      in4func_bash_include $In4GitPath/in4_core/internals/in4_init/obs_project.xml
      in4func_execute_as_is osc meta prj home:$In4OBSBuildURIAuthor:$In4OBSBuildProject  -F /tmp/obs_project.xml   
    fi

    if ! test -d ${Dir}/_localbuild; then
      in4func_execute_as_is osc co -u -o ${Dir}/_localbuild home`printf '%b' '\72'`$In4OBSBuildURIAuthor`printf '%b' '\72'`$In4OBSBuildProject    
    fi
    cd ${Dir}_localbuild

    if ls -d ${Dir}* > /dev/null; then
      for PackagePath in `ls -d ${Dir}*`; do
        echo -e "\n In4 S: $PackagePath\n"          
        if [[ "$PackagePath" == *_localbuild ]]; then continue; fi
        Path2In4 $PackagePath
        if curl -s --head  --request GET "https://build.opensuse.org/package/show/home:$In4OBSBuildURIAuthor:$In4OBSBuildProject/$In4OBSPackageName" | grep "200 OK" > /dev/null; then
          echo "Package \"$In4OBSPackageName\" exists"
          exists[$In4OBSPackageName]=1
        else
          echo "Package \"$In4OBSPackageName\" does't exists, creating"  
          exists[$In4OBSPackageName]=0
          in4func_bash_include $In4GitPath/in4_core/internals/in4_init/obs_package.xml
          in4func_execute_as_is osc meta pkg home:$In4OBSBuildURIAuthor:$In4OBSBuildProject $In4OBSPackageName -F /tmp/obs_package.xml  
        fi
      done
      
      in4func_execute_as_is osc up -u

      for PackagePath in `ls -d ${Dir}*`; do
        if [[ "$PackagePath" == *_localbuild ]]; then continue; fi      
        Path2In4 $PackagePath
        cd ${PackagePath}/../_localbuild/$In4OBSPackageName/        
        if [[ ${exists[$In4OBSPackageName]} == 0 ]]; then
          shopt -s extglob
          in4func_execute_as_is cp ${PackagePath}/_service ./
          in4func_execute_as_is osc add *
          in4func_execute_as_is osc ci -m 'in4'
        else
          currentUnixtime=`date "+%s"`
          oscTmpFile="/tmp/osc_status.tmp"
          openSuseVersion="openSUSE_Leap_15.1" # BUG version doubled
          ! obsPackageStatus ${PackagePath}/../_localbuild/$In4OBSPackageName/
        fi
      done
      
    fi
  done
}

in4v5_template() {
    in4func_execute_as_is mkdir -p $in4TaxonomyPath
    in4func_execute_as_is cp -R $In4GitPath/in4_core/internals/in4_init/_template/* $in4TaxonomyPath/
    in4func_execute_as_is grep -rl --exclude-dir=.git logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s ./ | xargs sed -i "s/logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s/${store[c]}--c--${store[o]}--o--${store[f]}--f--${store[g]}--g--${store[s]}--s/g"
    in4func_execute_as_is grep -rl --exclude-dir=.git logitoring--c--messagebus--o--syslog--f--rsyslog ./ | xargs sed -i "s/logitoring--c--messagebus--o--syslog--f--rsyslog/${store[c]}--c--${store[o]}--o--${store[f]}--f--${store[g]}/g"
    in4func_execute_as_is grep -rl --exclude-dir=.git rsyslog--g--main--s ./ | xargs sed -i "s/rsyslog--g--main--s/${store[g]}--g--${store[s]}--s/g"    
    in4func_execute_as_is grep -rl --exclude-dir=.git rsyslog--g ./ | xargs sed -i "s/rsyslog--g/${store[g]}--g/g"

    ##in4func_execute_as_is find . -not -path "./.git/*" -type f -name *logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s* -exec bash -c 'for file in $@; do mv $file ${file/logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s/'$in4TaxonomySerial'}; done' bash {} +
}
        
        
in4v5_obsAddProject () {
    #obsArray=("${store[c]}--c" "${store[c]}--c:${store[o]}--o" "${store[c]}--c:${store[o]}--o:${store[f]}--f")
    obsArray=("$in4TaxonomyProject")    
    for In4OBSBuildProject in ${obsArray[*]}; do   
        in4func_bash_include $In4GitPath/in4_core/internals/in4_init/obs_project_dev.xml > /tmp/obs_project_dev.xml
        in4func_execute_as_is osc meta prj home:$In4OBSBuildURIAuthor:$In4OBSBuildURIIn4Prefix:$In4OBSBuildProject_dev  -F /tmp/obs_project_dev.xml

        in4func_bash_include $In4GitPath/in4_core/internals/in4_init/obs_project.xml > /tmp/obs_project.xml
        in4func_execute_as_is osc meta prj home:$In4OBSBuildURIAuthor:$In4OBSBuildURIIn4Prefix:$In4OBSBuildProject  -F /tmp/obs_project.xml        
    done
}

in4v5_obsAddPackage () {
    in4func_bash_include $In4GitPath/in4_core/internals/in4_init/obs_package.xml > /tmp/obs_package.xml
    in4func_execute_as_is osc meta pkg home:$In4OBSBuildURIAuthor:$In4OBSBuildURIIn4Prefix:$in4TaxonomyProject ${store[g]} -F /tmp/obs_package.xml
    in4func_execute_as_is mkdir -p $in4TaxonomyPath/in4/1_build

    in4func_execute_as_is osc linkpac home:$In4OBSBuildURIAuthor:$In4OBSBuildURIIn4Prefix:$In4OBSBuildProject_dev ${store[g]} home:$In4OBSBuildURIAuthor:$In4OBSBuildURIIn4Prefix:$In4OBSBuildProject
}

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
