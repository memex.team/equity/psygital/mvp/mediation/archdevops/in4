#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

in4func_bash_include $In4CorePath/areas/deploy/os/interfaces/_hw.sh

In4Disk_RecreatePartitions="Yes"
In4Disk_Mode="single"

if [[ -z $In4Disk_SizingUnit ]];  then In4Disk_SizingUnit="MiB"; fi
if [[ -z $In4Disk_SystemSize ]];  then In4Disk_SystemSize="2000"; fi
if [[ -z $In4Disk_SwapSize ]];    then In4Disk_SwapSize="100"   ; fi
if [[ -z $In4Disk_SysdataSize ]]; then In4Disk_SysdataSize="1000"; fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
