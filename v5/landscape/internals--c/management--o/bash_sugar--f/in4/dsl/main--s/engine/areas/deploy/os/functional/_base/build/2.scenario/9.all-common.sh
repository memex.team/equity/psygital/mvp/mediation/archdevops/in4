#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "in4.recipe.sh"
in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "bash.recipe.sh"
in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "sshd.recipe.sh"
in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "profile.d.recipe.sh"
in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "nss.recipe.sh"

in4func_run "logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s" "3_recipe/in4_shell" "main--s.recipe.sh"

if ! [[ $In4DeployOsMode =~ "container_" ]]; then
    in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "sssd.recipe.sh"
    in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "sudo.recipe.sh"
    in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "sysctl.recipe.sh"
    in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "systemd.recipe.sh"
   # in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "drbd.recipe.sh" # BUG
    
    in4func_run "services--c--security--o--firewall--f--firewalld--g--main--s" "3_recipe/in4_shell" "firewalld.recipe.sh"
    
    
    ## BUG - add to kitchen
    in4func_ZypperRepo "add" "in4::logitoring--c--agents--o--space--f--agedu--g"
    in4func_ZypperRepo "add" "in4::logitoring--c--agents--o--space--f--duc--g"
    in4func_Zypper "atop agedu"
    # BUG duc - add libtokyocabinet
    
    in4func_ZypperRepo "add" "in4::services--c--mail--o--mta--f--exim--g"
    in4func_Zypper "exim"
    
    in4func_ZypperRepo "add" "in4::data_safety--c--snapshots--o--fs--f--btrfs--g"
    in4func_Zypper "btrfsprogs snapper"
    
    in4func_ZypperRepo "add" "in4::services--c--routing--o--netfilter--f--nftables--g"
    in4func_Zypper "nftables"
    
fi

#apparmor
#auditd
#bareos

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
