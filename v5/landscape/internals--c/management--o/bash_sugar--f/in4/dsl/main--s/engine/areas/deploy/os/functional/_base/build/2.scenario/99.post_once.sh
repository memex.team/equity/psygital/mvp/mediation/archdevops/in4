#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

## SSH
! in4func_execute_as_is rm  /etc/ssh/ssh_host_*
in4func_execute_as_is sshd-gen-keys-start
in4func_execute_as_is chmod 644 /etc/ssh/ssh_host_*.pub
##

    
### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
