#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

in4func_execute_as_is zypper mr -K -a
## zypper clean -a ## NOT COMP WITH OFFLINE 

### OS BUILD TAG ###
echo "#!/bin/bash" > /etc/in4-release
echo "View='os'" >> /etc/in4-release
echo "In4DeployOsMode=\"$In4DeployOsMode\"" >> /etc/in4-release
echo "In4OsVendor=\"$In4OsVendor\"" >> /etc/in4-release
echo "In4OsRelease=\"$In4OsRelease\"" >> /etc/in4-release
echo "In4OsArch=\"$In4OsArch\"" >> /etc/in4-release
echo "In4OsBuild=\"$In4OsBuild\"" >> /etc/in4-release 
echo "In4OsOBSName=\"$In4OsOBSName\"" >> /etc/in4-release 
echo "In4GitPath=\"$In4GitPath\"" >> /etc/in4-release
In4OsBuildDate=`date +"w"%W"y"%y`
echo "In4OsBuildDate=\"$In4OsBuildDate\"" >> /etc/in4-release
In4OsBuildDate=`date +%d.%m.%y_w%W_%H:%M:%S`
echo "In4OsBuildDateFull=\"$In4OsBuildDate\"" >> /etc/in4-release
In4OsBuildDateYear=`date +%y`
echo "In4OsBuildDateYear=\"$In4OsBuildDateYear\"" >> /etc/in4-release
In4OsBuildDateWeek=`date +w%W`
echo "In4OsBuildDateWeek=\"$In4OsBuildDateWeek\"" >> /etc/in4-release

OsBuildGitBranch=`git -C $In4GitPath rev-parse --abbrev-ref HEAD`
echo "OsBuildGitBranch=\"$OsBuildGitBranch\"" >> /etc/in4-release
OsBuildGitHash=`git -C $In4GitPath rev-parse HEAD`
echo "OsBuildGitHash=\"$OsBuildGitHash\"" >> /etc/in4-release
OsBuildGitHashShort=`git -C $In4GitPath rev-parse --short HEAD`
echo "OsBuildGitHashShort=\"$OsBuildGitHashShort\"" >> /etc/in4-release
echo "In4OsBuildGitTag=\"$In4OsBuildGitTag\"" >> /etc/in4-release
echo "In4OsBuildGitTagWoDot=\"$In4OsBuildGitTagWoDot\"" >> /etc/in4-release
echo "#" >> /etc/in4-release

###

### RM ALL LOGS & TRACES ###
in4func_execute_as_is cd /
in4func_execute_as_is rm -rf  /media/sysdata/offline
in4func_execute_as_is rm /etc/resolv.conf ### BUG
in4func_execute_as_is rm /etc/sysconfig/proxy ### BUG
! in4func_execute_as_is rm -f /var/log/*/*
! in4func_execute_as_is rm -f /var/log/*.log
in4func_execute_as_is rm -f /root/.bash_history
in4func_execute_as_is rm -rf /tmp/*
in4func_execute_as_is history -c
###

### Set area flag
in4func_SetAreaFlag deploy os $In4DeployOsMode
###

exit 

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
