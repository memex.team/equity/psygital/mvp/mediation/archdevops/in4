#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if ! [[ $In4Disk_Mode == 'current' ]]; then
  in4func_bash_include $In4ExecPath/_base/build/1.init/offline_merge.sh loop_to_main
fi
if in4func_execute_as_root_wo_tests test -d $In4BuildEnv/loop/dev ; then ! in4func_execute_as_root umount $In4BuildEnv/loop/dev; fi
if in4func_execute_as_root_wo_tests test -d $In4BuildEnv/loop/proc ; then ! in4func_execute_as_root umount $In4BuildEnv/loop/proc; fi
if in4func_execute_as_root_wo_tests test -d $In4BuildEnv/loop/sys ; then ! in4func_execute_as_root umount $In4BuildEnv/loop/sys; fi

if in4func_execute_as_root_wo_tests mountpoint -q $In4BuildEnv/loop/media/sysdata ; then _umount $In4BuildEnv/loop/media/sysdata; fi
if in4func_execute_as_root_wo_tests mountpoint -q $In4BuildEnv/loop ; then _umount $In4BuildEnv/loop; fi
    
if [[ -n $(in4func_execute_as_root_wo_tests losetup|grep "$In4Disk_LoopSysdata") ]]; then in4func_execute_as_root losetup -d /dev/$In4Disk_LoopSysdata; fi
if [[ -n $(in4func_execute_as_root_wo_tests losetup|grep "$In4Disk_LoopSystem") ]] ; then in4func_execute_as_root losetup -d /dev/$In4Disk_LoopSystem; fi


if [[ " ${In4BuildLayers[@]} " =~ "unit" ]] ; then 
    ! _umount /dev/${In4Disk_BaseDisk}*
    ! _umount /dev/${In4Disk_BaseDisk}*
fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
