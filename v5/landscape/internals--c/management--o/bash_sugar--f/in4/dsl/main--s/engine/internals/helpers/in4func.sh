#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash DSL, profiles
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

in4func_logit() {
    Point=$1
    ScriptName=$2
    Desc=$3
    LogMsg="$Point :: $ScriptName --- $Desc"
    if [[ $In4Debug == "Yes" ]]; then
        echo -e "\e[34m\n\n########  $LogMsg  ########\n\n\e[0m"
        if [[ -f /usr/bin/logger ]]; then logger -p info -t "in4" "$LogMsg"; fi
    fi    
}


in4func_resolve_in4() {
declare -A k v store
   input_serialised=$1
       
    case $input_serialised in
    "in4::"*)
      in4TaxonomyType='in4'
      input_serialised=`echo $input_serialised|sed -e "s/in4:://"`      
      ;;
    "dsl::"*)
      in4TaxonomyType='dsl'
      input_serialised=`echo $input_serialised|sed -e "s/dsl:://"`
      ;;     
    esac

    
    if [[ $input_serialised =~ --f--.*.--g ]]; then 

        arr=(${input_serialised//--/ }) 
        i=0
        for pairs in ${arr[*]}; do
            if [[ $pairs != ? ]]; then 
                v[${i}]=${pairs}
            else
                k[${i}]=${pairs}
                (( ++i))
            fi
        done
        
        i=0
        for kv in ${k[*]}; do   
            store[${kv}]=${v[$i]}
                (( ++i))
    done

    else
        echo -e "\n### Please choose from already defined var or define by yourself ###"   
        
        DialogMsg="\n### Class ###"   
        echo -e $DialogMsg; select a in `ls $In4GitPath/v5/landscape/` "## NEW ##";  do  break ; done          
        if [[ $a == "## NEW ##" ]]; then echo  "Enter a new Class :" && read a; new=1; fi 
        if [[ $a =~ "--" ]]; then
            store["c"]=`echo $a|awk -F '--' '{print $1}'`            
        else
            store["c"]=$a
        fi

        DialogMsg="\n### Order ###"   
        if ! [[ $new == 1 ]]; then  echo -e $DialogMsg; select a in `ls $In4GitPath/v5/landscape/${store[c]}--c/` "## NEW ##";  do  break ; done; fi
        if [[ $a == "## NEW ##" ]] || [[ $new == 1 ]] ; then echo  "Enter a new Order :" && read a; new=1; fi 
        if [[ $a =~ "--" ]]; then
            store["o"]=`echo $a|awk -F '--' '{print $1}'`            
        else
            store["o"]=$a
        fi

        DialogMsg="\n### Family ###"   
        if ! [[ $new == 1 ]]; then echo -e $DialogMsg; select a in `ls $In4GitPath/v5/landscape/${store[c]}--c/${store[o]}--o/` "## NEW ##";  do  break ; done; fi          
        if [[ $a == "## NEW ##" ]] || [[ $new == 1 ]] ; then echo  "Enter a new Family :" && read a; new=1; fi 
        if [[ $a =~ "--" ]]; then
            store["f"]=`echo $a|awk -F '--' '{print $1}'`            
        else
            store["f"]=$a
        fi
    
        DialogMsg="\n### Genus ###"   
        if ! [[ $new == 1 ]]; then echo -e $DialogMsg; select a in `ls $In4GitPath/v5/landscape/${store[c]}--c/${store[o]}--o/${store[f]}--f/` "## NEW ##";  do  break ; done; fi          
        if [[ $a == "## NEW ##" ]] || [[ $new == 1 ]] ; then echo  "Enter a new Genus :" && read a; new=1; fi 
        if [[ $a =~ "--" ]]; then
            store["g"]=`echo $a|awk -F '--' '{print $1}'`            
        else
            store["g"]=$a
        fi


        DialogMsg="\n### Species ###"   
        echo -e $DialogMsg; select a in "main" "dev" "## NEW ##";  do  break ; done          
        if [[ $a == "## NEW ##" ]]; then echo  "Enter a new Species :" && read a; fi 
        store["s"]="$a"
        
    fi

    in4TaxonomyPath="$In4GitPath/v5/landscape/${store[c]}--c/${store[o]}--o/${store[f]}--f/${store[g]}"
    in4TaxonomyProject="${store[c]}--c:${store[o]}--o:${store[f]}--f:${store[g]}--g"
    in4TaxonomySpecies="${store[s]}--s"
    in4TaxonomySerial="${store[c]}--c--${store[o]}--o--${store[f]}--f--${store[g]}--g--${store[s]}--s"
    in4TaxonomySerialShort="${store[c]}--c--${store[o]}--o--${store[f]}--f--${store[g]}"
    in4TaxonomyMidPath="${store[c]}--c/${store[o]}--o/${store[f]}--f/${store[g]}"    
    
    case $in4TaxonomyType in
    "in4")
      in4TaxonomyPathPosfix="in4"    
      ;;
    "dsl")
      in4TaxonomyPathPosfix="dsl/$in4TaxonomySpecies"
    esac    
}
# 

in4func_execute_helper_runner () {
    Command=${@}
        
    if [[ $In4Debug == "Yes" ]]; then
        echo -e "\e[35m\n\n########  Running: $CommandSrc  ########\n\n\e[0m"
        begin=$(date +%s) 
        # TODO - .%N - as nanoseconds
        time $Command
        retVal=$?
        end=$(date +%s) && echo -e "\e[35m########  End of running: $CommandSrc , total time: $(expr $end - $begin) seconds ########\n\n\e[0m"
        return $retVal
    else
        $Command
    fi
}


in4func_execute_helper_with_tests () {
    CommandSrc=${@}
    if [[ $In4Test == "Yes" ]]; then
        Command=""
    else
        Command="$CommandSrc"
    fi
        
    in4func_execute_helper_runner $Command
}

in4func_execute_helper_wo_tests () {
    Command=${@}
    in4func_execute_helper_runner $Command
}


in4func_execute_as_is () {
    Command=${@}
    in4func_execute_helper_with_tests $Command
}

in4func_execute_as_user () {
    AsUser=$1
    Command=$2
    in4func_execute_helper_with_tests sudo -u $AsUser $Command
}

in4func_resolve_sudo() {
    UserId=`id -u`
    
    if [[ $UserId == 0 ]]; then
        Prefix=""
    else
        Prefix="sudo "
    fi
}

in4func_execute_as_root () {
    Command=${@}
    in4func_resolve_sudo
    in4func_execute_helper_with_tests $Prefix $Command
}

in4func_execute_as_root_wo_tests () {
    Command=${@}
    in4func_resolve_sudo
    in4func_execute_helper_wo_tests $Prefix $Command
}

in4func_bash_include () {
    Command=${@}
    if [[ $In4Debug == "Yes" ]]; then
        echo -e "\e[35m\n\n########  Running: $Command  ########\n\n\e[0m"    
        begin=$(date +%s) 
        time . $Command
        retVal=$?
        end=$(date +%s) && echo -e "\e[35m########  End of running: $Command , total time: $(expr $end - $begin) seconds ########\n\n\e[0m" 
        return $retVal        
    else
        . $Command
    fi
}

in4func_Zypper () {

    if [[ -z $ZypperOperation ]]; then ZypperOperation="in"; fi
    ZypperArgsOnline="--non-interactive  --gpg-auto-import-keys $ZypperOperation --no-recommends " 
    ZypperArgsAltOnline="--non-interactive  --gpg-auto-import-keys  -C /var/cache/zypp_offline $ZypperOperation --no-recommends " 
    ZypperArgsOffline="--non-interactive  --no-gpg-checks --no-refresh -C /var/cache/zypp_offline $ZypperOperation --force-resolution --no-recommends "
    
    if [[ -n $(echo "$1"|grep "/") ]]; then
        readarray PackagesArray < $1    
    else
        PackagesArray=("$1")
    fi
    
    Packages=$(printf "%s" "${PackagesArray[@]}")
    
    if [[ $In4OfflineCliMode == "Yes" ]]; then
        in4func_execute_as_root zypper $ZypperArgsOffline ${Packages}        
    elif  [[ $In4OfflineBuildMode == "Yes" ]]; then
        
        set +e
        echo -e "\nnon-blocking zypper\n"
        in4func_execute_as_root zypper $ZypperArgsAltOnline ${Packages}
        AltOnline=$?
        
        echo -e "\nnon-blocking zypper\n"
        in4func_execute_as_root zypper $ZypperArgsOffline ${Packages}
        Offline=$?
        ZypperStatus=$(($AltOnline + $Offline))
        set -e
        
        if [[ $ZypperStatus -gt 4 ]] ; then
            echo "Both AltOnline & Offline zypper are failed"; exit 1
        fi
        
    else
        echo -e "\nblocking zypper\n" && in4func_execute_as_root zypper $ZypperArgsOnline ${Packages}
    fi  
    ZypperOperation="in"
}


in4func_ZypperRepo () {

    ZypperRepoAction=$1
    ZypperRepoURI=$2
    ZypperRepoArgsOnline="--non-interactive  --gpg-auto-import-keys ar -cfk" 
    
    if [[ $ZypperRepoURI =~ "in4::" ]]; then
        ZypperRepoArgsOnline="$ZypperRepoArgsOnline -p 10 "
        in4func_bash_include $In4GitPath/in4_core/internals/env/v5_obs.sh    
        in4func_resolve_in4 `echo $ZypperRepoURI|awk -F '::' '{print $2}'`
        In4OBSBuildURIIn4Prefix=`echo $In4OBSBuildURIIn4Prefix|sed -e "s/:/:\//"`
        if curl -s --head  --request GET "$In4OBSBuildURIsusePart:/$In4OBSBuildURIAuthor:/$in4TaxonomySerialShort/$In4OsOBSName/" | grep "200 OK" > /dev/null; then  
          ZypperRepoURI="-n $in4TaxonomySerialShort $In4OBSBuildURIsusePart:/$In4OBSBuildURIAuthor:/$in4TaxonomySerialShort/$In4OsOBSName  $in4TaxonomySerialShort"
        else
          ZypperRepoURI="-n $in4TaxonomySerialShort $In4OBSBuildURIsusePart:/$In4OBSBuildURIAuthor:/$in4TaxonomySerialShort/openSUSE_Tumbleweed  $in4TaxonomySerialShort"        
        fi        
    fi
    
    case $ZypperRepoAction in
    "add" )
    
        if [[ $In4OfflineCliMode == "Yes" ]]; then
            echo "Offline mode, all repos are cached"
        elif  [[ $In4OfflineBuildMode == "Yes" ]]; then
            echo -e "\nnon-blocking zypper\n" && ! in4func_execute_as_root zypper $ZypperRepoArgsOnline $ZypperRepoURI
            echo "#" ### FIX FOR "!" SELF_EXIT
        else
            echo -e "\nblocking zypper\n" && in4func_execute_as_root zypper $ZypperRepoArgsOnline $ZypperRepoURI
        fi      
    ;;
    esac
}

in4func_cpsync() {
    if ! [[ -z $1 ]]; then 
        Src=$1
        Dst=$2
    fi
    
    if in4func_execute_as_root_wo_tests test -d $Src ; then in4func_execute_as_root cp -pruf $Src $Dst; fi
}

in4func_run () {
    # Compat code, needs rework
    if ! [[ -z $1 ]]; then 
        In4LandscapeFQN=$1
        In4RunPath=$2
        In4RunName=$3
    fi
    
    in4func_resolve_in4 $In4LandscapeFQN
    in4func_bash_include $in4TaxonomyPath/in4/$In4RunPath/$In4RunName

}

in4func_cp () {
    in4LandscapeFQN= in4func_resolve_in4 $1
    CpSource=$2
    CpDestination=$3
    
    in4func_execute_as_root cp -r $in4TaxonomyPath/$in4TaxonomyPathPosfix/$CpSource $CpDestination 
}

in4func_ln () {
        
    in4LandscapeFQN= in4func_resolve_in4 $1    
    LnSource=$2
    LnDestination=$3
    
    if [ -L "$LnDestination" ]; then
        in4func_execute_as_root rm -f $LnDestination
    
    elif [ -f "$LnDestination" ] && [ -f $in4TaxonomyPath/$in4TaxonomyPathPosfix/$LnSource ]; then
        in4func_execute_as_root rm -f $LnDestination        

    elif [ -d "$LnDestination" ] && [ -f $in4TaxonomyPath/$in4TaxonomyPathPosfix/$LnSource ]; then
        in4func_execute_as_root rm -f $LnDestination/`echo $LnSource|awk -F" +|/" '{print $NF}'`      
    fi
    
    in4func_execute_as_root ln -s -f $in4TaxonomyPath/$in4TaxonomyPathPosfix/$LnSource $LnDestination
}

in4func_systemd () {
    in4LandscapeFQN= in4func_resolve_in4 $1
    SystemdAction=$2
    SystemdType=$3
    SystemdName=$4
    
    case $SystemdAction in
    "add" )
        in4func_execute_as_root rm -f /etc/systemd/system/$SystemdName.$SystemdType
        in4func_execute_as_root cp -f $in4TaxonomyPath/in4/5_service/systemd/$SystemdName.$SystemdType /etc/systemd/system/
    ;;
    "enable") 
        in4func_execute_as_root systemctl enable $SystemdName.$SystemdType
    ;;
    esac
}

in4func_firewalld () {
    in4LandscapeFQN= in4func_resolve_in4 $1
    FirewalldAction=$2
    FirewalldName=$3
    
    case $FirewalldAction in
    "add" )
        FirewallPath="/media/sysdata/app/firewall"
        
        if ! [[ -d $FirewallPath ]]; then 
          in4func_execute_as_root mkdir -p $FirewallPath/zone $FirewallPath/services ## TODO 
        fi
        in4func_execute_as_root rm -f $FirewallPath/services/$FirewalldName.xml
        in4func_execute_as_root ln -s  $in4TaxonomyPath/in4/4_security/firewalld/$FirewalldName.xml $FirewallPath/services/
        in4func_execute_as_root firewall-offline-cmd --system-config $FirewallPath --add-service=$FirewalldName
    ;;
    esac
}

in4yesno ()  {
    echo  -n "$1? "
    while true; do
            read answer
            case $answer in
                    y | Y | yes | YES ) answer="Yes"; break;;
                    n | N | no | NO ) exit;;
                    *) echo "Please answer (y)es or (n)o.";;
            esac
    done
}

in4func_findobj () {
    Path=$1
    Type=$2
    if [[ -z $3 ]]; then 
        Filter="*"
    else
        Filter=$3;
    fi
    
    Exec=`find $Path -maxdepth 1 -type $Type|grep -v "/$"|sed -e 's/.*.\///'|grep -v "^_"|sort`
    if [[ $Exec =~ "." ]]; then
        for Line in $Exec; do
        echo $Line|cut -d . -f 1
        done
    else
        echo $Exec    
    fi
}

in4func_getuser () {
  User=$1
  IsUser=`cut -d: -f1 /etc/passwd|grep $User`
  if [[ $IsUser == '' ]]; then
    echo "create"
  fi  
}

in4func_getgroup () {
  Group=$1
  IsGroup=`cut -d: -f1 /etc/group|grep $Group`
  if [[ $IsGroup == '' ]]; then
    echo "create"
  fi  
}

## USAGE
#ExecStart=/bin/bash rsync  blkio throttle.read_bps_device 2000 system
##

cgroup_applyer () {
    service=$1
    cgoupModule=$2
    cgoupMetric=$3
    value=$4
    context=$5


    if [[ $cgoupModule == "blkio" ]]; then

        blockDeviceName= in4func_execute_as_root ls -l -H /dev/disk/by-label/|grep -m 1 $context|awk '{print $11}'|cut -d"/" -f 3
        if ! [[ $blockDeviceName =~ "drbd" ]]; then
            blockDeviceName=`echo $blockDeviceName|sed "s/[0-9]//"`
        fi
        diskNumber=`ls -l -H /dev/$blockDeviceName|awk '{print substr($5, 1, length($5) - 1)":"$6}'`;

        in4func_execute_as_root cgset -r $cgoupModule.$cgoupMetric="$diskNumber $value" system.slice/$service.service
        in4func_execute_as_root cgget -a system.slice/$service.service |grep $cgoupMetric
    fi
}

in4func_validate_url(){
  if [[ `wget -S --spider $1  2>&1 | grep 'HTTP/1.1 200 OK'` ]]; then echo "true"; fi
}

in4func_wget() {
DownloadFrom=$1
DownloadTo=$2
WgetParams=$3

    if [[ -f $DownloadTo ]]; then 
        echo "File $DownloadTo exists, using cached"
    else
        echo "File $DownloadTo does not exists, downloading from $DownloadFrom"
        if `in4func_validate_url $DownloadFrom`; then wget -O $DownloadTo $WgetParams $DownloadFrom; else echo "File does not exist on downloaded source!"; exit 1; fi
        
    fi
}

in4func_SetAreaFlag(){
    if ! [[ -z $1 ]]; then
    In4Area=$1
    In4Task=$2
    In4Context=$3
    fi
    
    if ! [[ -e /media/sysdata/app/in4_areas/$In4Area/$In4Task ]]; then 
        mkdir -p /media/sysdata/app/in4_areas/$In4Area/$In4Task
        if [[ $In4DeployOsMode =~ "container_" ]]; then
            mkdir -p /media/sysdata/app/in4_areas/$In4Area/$In4Task/$In4Context
        else
            btrfs subvolume create /media/sysdata/app/in4_areas/$In4Area/$In4Task/$In4Context
        fi
    fi
}

in4func_isDeployedImageExists() { In4ImageRedeploy="Yes"; }
in4func_isDeployedImageExists2 () {
  if [[ -f $In4BuildEnv/../${In4OsBuild}_${In4OsSrvType}.raw  ]]; then        
        In4OsBuildDateWeek=`echo $In4OsBuildDate|cut -d "y" -f 1|cut -d "w" -f 2`	        
        if [[ -z $In4ImageRedeploy ]]; then
            DialogMsg="Image is up-to-date, force redeploy?"
            echo $DialogMsg; select  In4ImageRedeploy in Yes No ;  do  break ; done;
        fi   
    else
        In4ImageRedeploy="Yes"
    fi
}

### CLASSIC ###
_umount() {
    Mount=$1
    if [[ $In4Test == "Yes" ]]; then
        echo "Test mode, no umounts"
    else
        while in4func_execute_as_root mountpoint -q $Mount; do
            in4func_execute_as_root umount $Mount || sleep 1
        done
    fi
}
###
