#!/bin/bash
set -e
## PRE - ENV ##
. /media/sysdata/in4/platform/in4_core/internals/env/common_env.sh
## PRE - FUNCTIONS ##
. $In4CorePath/internals/helpers/in4func.sh
. /etc/os-release

In4Context=$1
In4Area=deploy
In4Task=os
In4OsVendor="in4"
In4OsRelease="15_1"
In4OsArch="x86_64"
DataDestroy='--'
In4OsBuildGitTag="v0.1"
In4ShellDebug="Yes"
In4Debug="Yes"

if [[ -z $In4Context ]]; then
    DialogMsg="Please specify In4 context"
    echo $DialogMsg; select In4Context in `in4func_findobj "$In4CorePath/areas/$In4Area/$In4Task/interfaces/" 'f'`;  do  break ; done
fi

. /media/sysdata/in4/platform/in4.sh
