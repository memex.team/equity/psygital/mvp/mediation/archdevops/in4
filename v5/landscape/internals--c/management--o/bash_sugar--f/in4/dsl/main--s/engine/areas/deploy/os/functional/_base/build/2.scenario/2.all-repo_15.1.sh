#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

### CP REPOS FROM OFFLINE ###
if [[ -d /media/sysdata/offline/zypper/repos.d ]]; then
     in4func_execute_as_is mkdir /etc/zypp/repos.d_offline 
     in4func_execute_as_is cp -r /media/sysdata/offline/zypper/repos.d/*  /etc/zypp/repos.d_offline/
     in4func_execute_as_is sed -i 's/keeppackages=.*/keeppackages=1/g' /etc/zypp/repos.d_offline/*.repo
fi  
###

### ZYPPER ###
in4func_execute_as_is rm -rf /etc/zypp/repos.d/*
if [[ -d /etc/zypp/repos.d_offline ]]; then
    in4func_execute_as_is rm -rf /etc/zypp/repos.d_offline/repo-*
    in4func_execute_as_is cp -r /etc/zypp/repos.d_offline/* /etc/zypp/repos.d/
fi

## SYMLINK BUG FIX
in4func_execute_as_is rm /var/lib/rpm
in4func_execute_as_is ln -s /usr/lib/sysimage/rpm /var/lib
##

#if ! find /etc/zypp/repos.d/ -type f -name '*.repo'| read; then
### 15.1###
    in4func_ZypperRepo add  "http://download.opensuse.org/repositories/openSUSE:/Leap:/15.1/standard standard::leap15.1"
    in4func_ZypperRepo add  "http://download.opensuse.org/update/leap/15.1/oss update_oss::leap15.1"
    in4func_ZypperRepo add  "http://download.opensuse.org/update/openSUSE-stable update_oss::stable"

    ##SOME STANDARD
    in4func_ZypperRepo add  http://download.opensuse.org/repositories/network/openSUSE_Leap_15.1/network.repo
    in4func_ZypperRepo add  http://download.opensuse.org/repositories/shells/openSUSE_Leap_15.1/shells.repo
#fi


### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
