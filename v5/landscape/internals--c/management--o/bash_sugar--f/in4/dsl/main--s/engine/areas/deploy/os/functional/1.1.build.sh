#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

in4func_bash_include $In4ExecPath/_base/build/1.init/clean.sh
if in4func_execute_as_root_wo_tests mountpoint -q /tmp/_os_build ; then _umount /tmp/_os_build; fi

in4func_bash_include $In4ExecPath/_base/build/1.init/1.pre.sh
in4func_bash_include $In4ExecPath/_base/build/1.init/disk_partitioning.sh
in4func_bash_include $In4ExecPath/_base/build/1.init/3.exec.sh
in4func_bash_include $In4ExecPath/_base/build/1.init/4.post.sh

tput setaf 2
echo -e "${green}\n\n\n ################# 1.1 BUILDED OK #################"
tput setaf 9

in4func_bash_include $In4ExecPath/1.2.test.sh

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
