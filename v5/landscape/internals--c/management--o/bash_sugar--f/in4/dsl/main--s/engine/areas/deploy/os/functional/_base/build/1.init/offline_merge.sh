#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

OfflineCpFlow=$1

case $OfflineCpFlow in
    "loop_to_main")
    if  [[ $In4OfflineBuildMode == "Yes" ]]; then
      
      if test -d $In4BuildEnv/loop/media/sysdata/linux_sys/etc/zypp/repos.d; then
        echo "Downloaded repo will be copied to offline dir: $In4OfflineBuildDir"
        in4func_cpsync "$In4BuildEnv/loop/media/sysdata/linux_sys/etc/zypp/repos.d" "$In4OfflineBuildDir/zypper/"
        syncPackages=1
      fi
      
      if test -d $In4BuildEnv/loop/etc/zypp/repos.d; then
        echo "Downloaded repo will be copied to offline dir: $In4OfflineBuildDir"
        in4func_cpsync "$In4BuildEnv/loop/etc/zypp/repos.d" "$In4OfflineBuildDir/zypper/"
        syncPackages=1
      fi
      
      if [[ $syncPackages == 1 ]]; then
        echo "Downloaded packages will be copied to offline dir: $In4OfflineBuildDir"
        in4func_cpsync "$In4BuildEnv/loop/media/sysdata/linux_sys/var/cache/zypp_offline" "$In4OfflineBuildDir/zypper/"
      fi      
      
    fi     
    in4func_execute_as_root rm -rf $In4BuildEnv/loop/media/sysdata/linux_sys/var/cache/zypp*    
    ;;
    "main_to_loop")
    if  [[ $In4OfflineBuildMode == "Yes" ]] ||  [[ $In4OfflineCliMode == "Yes" ]] ; then
            echo "Offline data will be copied to offline dir: $In4OfflineBuildDir"
            in4func_cpsync "$In4OfflineBuildDir" "$In4BuildEnv/loop/media/sysdata"
    fi     
    ;;
esac    

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
