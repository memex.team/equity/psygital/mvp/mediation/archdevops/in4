"git" )
                if [[ -z $In4TaskVars ]]; then
                    DialogMsg="\n### Please specify dev task ###"   
                    echo -e $DialogMsg; select In4TaskVars in c dev staging stable;  do  break ; done
                fi   
                in4func_bash_include $In4GitPath/in4_core/internals/helpers/git.sh
                
                case $In4TaskVars in                    
                    "c")
                        In4gitCommitMessage="in4"
                        gitOffline
                    ;;
                    "dev")
                        echo "Please enter a commit message"; read In4gitCommitMessage                    
                        gitIn4CycleDev
                    ;;
                    "staging")
                        In4gitCommitMessage="merging master->staging"                    
                        gitIn4CycleDevStaging
                    ;;
                    "stable")
                        In4gitCommitMessage="merging staging->v1-stable"                                        
                        gitIn4CycleV1Stable                      
                    ;;
                esac
            ;;             
