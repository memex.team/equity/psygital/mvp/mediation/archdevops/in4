#!/bin/bash
## OS
In4OsVendor="in4"
In4OsReleaseAvailable=("15_1")
In4OsArchAvailable=("x86_64" "i586" "armv7l")
##

## VM/HW
In4GitRepoStable="https://gitlab.com/Innosense/Archestry/in4.git"
In4GitRepoDev="https://gitlab.com/Innosense/Archestry/in4.git"
In4GitStablePath="v1-stable"
In4Disk_LoopSystem="loop250"
In4Disk_LoopSysdata="loop251"
##

##TMP
In4OsBuildGitTagAvailable=("v0.1")
##
