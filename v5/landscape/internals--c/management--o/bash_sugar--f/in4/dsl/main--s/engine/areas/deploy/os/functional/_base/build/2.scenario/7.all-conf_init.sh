#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

### BOOT OPTIONS ###
in4func_execute_as_is echo 'LOADER_TYPE="none"' /etc/sysconfig/bootloader
in4func_execute_as_is echo 'LOADER_LOCATION="none"' /etc/sysconfig/bootloader
###

### MISC ###
if ! [[ $In4DeployOsMode =~ "container_" ]]; then
    in4func_execute_as_is sed -i "s/FAIL_DELAY.*/FAIL_DELAY\x20\x20\x2010/" /etc/login.defs
    in4func_execute_as_is sed -i "s/UMASK.*/UMASK\x20\x20\x20077/" /etc/login.defs
    in4func_execute_as_is sed -i "s/NETCONFIG_DNS_POLICY=.*/NETCONFIG_DNS_POLICY=\"auto\"/" /etc/sysconfig/network/config
    in4func_execute_as_is sed -i "s/CHECK_DUPLICATE_IP=.*/CHECK_DUPLICATE_IP='yes'/"    /etc/sysconfig/network/config
    #in4func_execute_as_is sed -i "s/DEVICE_NAMES=.*/DEVICE_NAMES=\"label\"/"    /etc/sysconfig/storage
    in4func_execute_as_is sed -i "s/DAILY_TIME=.*/DAILY_TIME=\"00:00\"/" /etc/sysconfig/cron
    in4func_execute_as_is sed -i  "s/set\x20term\x20xy/#/" /etc/inputrc
    in4func_execute_as_is sed -i "s/active\x20=.*/active\x20=\x20yes/" /etc/audisp/plugins.d/syslog.conf
    
###
fi

### PROXY ###
in4func_execute_as_is sed -i "s/HTTP_PROXY=.*/HTTP_PROXY=\"http:\/\/x:55555\"/" /etc/sysconfig/proxy
in4func_execute_as_is sed -i "s/HTTPS_PROXY=.*/HTTPS_PROXY=\"http:\/\/x:55555\"/" /etc/sysconfig/proxy
in4func_execute_as_is sed -i "s/FTP_PROXY=.*/FTP_PROXY=\"http:\/\/x:55555\"/" /etc/sysconfig/proxy
in4func_execute_as_is sed -i "s/NO_PROXY=.*/NO_PROXY=\"localhost,\x20127.0.0.1,\x20.pool\"/" /etc/sysconfig/proxy
###

### PAM SETTINGS ###
if ! [[ $In4DeployOsMode =~ "container_" ]]; then
  in4func_execute_as_is pam-config --add --mkhomedir
  in4func_execute_as_is pam-config --add --systemd
fi
###		

### CA CERT HACK ###
in4func_execute_as_is rm /etc/ssl/certs
in4func_execute_as_is ln -s /var/lib/ca-certificates/openssl /etc/ssl/certs
###

if ! [[ $In4DeployOsMode =~ "container_" ]]; then
  ### SERVICES FIRST ENABLE/START ###
  ! in4func_execute_as_is /sbin/yast security level server ## BUG  - exits only in containers
  echo "alias /media/sysdata/in4/context/_me/ -> /media/sysdata/in4/context/v5/`hostname`/," >> /etc/apparmor.d/tunables/alias
  #/sbin/rcapparmor start
  ! in4func_execute_as_is systemctl disable ntpd
  ! in4func_execute_as_is systemctl mask ntpd
  ###

  ### AUDITD
  in4func_execute_as_is mkdir /var/log/audit
  in4func_execute_as_is echo 'write_logs = no' >> /etc/audit/auditd.conf
  ###
fi

### LOCALE
in4func_execute_as_is echo "LC_CTYPE=en_US.UTF-8" > /etc/locale.conf

in4func_ln "in4::internals--c--management--o--bash_sugar--f--kitchen--g" "5_service/tmpfilesd/in4.conf" "/etc/tmpfiles.d/"

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
