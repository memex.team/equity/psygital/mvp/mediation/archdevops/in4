#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ $In4DeployOsMode == "vm_xen" ]] ; then in4func_bash_include $In4ExecPath/vm_xen/build/1.init/5.vm_xen-start.sh; fi

tput setaf 2
echo -e "${green}\n\n\n ################# 1.2 TESTED OK #################"
tput setaf 9

in4func_bash_include $In4ExecPath/1.3.store.sh

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
