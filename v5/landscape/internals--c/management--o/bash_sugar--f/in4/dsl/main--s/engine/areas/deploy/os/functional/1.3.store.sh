#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

in4func_isDeployedImageExists
    
if [[ $In4ImageRedeploy == "Yes" ]]; then

    In4Disk_Labels=($In4OsBuild sysdata swap)
    case $In4ExportType in
    
    "file_tar.gz")
        in4func_execute_as_root rm -rf /tmp/_os_build/in4_$In4Context/media/sysdata/offline 
        in4func_execute_as_root tar czf /tmp/_os_build/$In4OsBuild.tar.gz  -C /tmp/_os_build/in4_$In4Context .
        in4func_execute_as_root cp -pruf /tmp/_os_build/$In4OsBuild.tar.gz $In4BuildEnv/images/$In4DeployOsMode/
    ;;

    "file_iso")
        in4func_execute_as_root cp -pruf /tmp/_os_build/$In4OsBuild.iso $In4BuildEnv/images/$In4DeployOsMode/
    ;;

    "vm_raw")
        in4func_execute_as_root cp -pruf /tmp/_os_build/*.raw $In4BuildEnv/images/$In4DeployOsMode/
    ;;

    "vm_vmware")
            for disk in "${In4Disk_Labels[@]}" ; do
                in4func_execute_as_root qemu-img convert -f raw -O vmdk -o adapter_type=lsilogic,subformat=streamOptimized,compat6 $In4BuildEnv/images/vm_hvm/$disk.raw $In4BuildEnv/images/$In4DeployOsMode/$disk.vmdk
            done
    ;;

    "vm_hyperv")
        for disk in "${In4Disk_Labels[@]}" ; do
            in4func_execute_as_root qemu-img convert -f raw -O vpc -o subformat=dynamic $In4BuildEnv/images/vm_hvm/$disk.raw $In4BuildEnv/images/$In4DeployOsMode/$disk.vmdk
        done    
        #-o subformat=fixed - for Azure
    ;;

    "vm_virtualbox")
        for disk in "${In4Disk_Labels[@]}" ; do
            in4func_execute_as_root qemu-img convert -f raw -O vdi $In4BuildEnv/images/vm_hvm/$disk.raw $In4BuildEnv/images/$In4DeployOsMode/$disk.vmdk
        done     
    ;;
    esac
    
tput setaf 2
echo -e "${green}\n\n\n ################# 1.3 STORED OK #################"
tput setaf 9

fi

in4func_execute_as_root chmod 755 $In4BuildEnv/images/$In4DeployOsMode/*
if in4func_execute_as_root_wo_tests mountpoint -q /tmp/_os_build ; then _umount /tmp/_os_build; fi


### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
