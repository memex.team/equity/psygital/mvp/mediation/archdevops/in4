CLASS=$1
PRODUCT=$2
TYPE=$3
INSTANCE=$4

if [[ !-z $INSTANCE ]]; then
    NAMING="in4__$PRODUCT_$TYPE@$INSTANCE"
else
    NAMING="in4__$PRODUCT_$TYPE"
fi

rm -f /etc/systemd/system/in4__$NAMING.service
cp /media/sysdata/in4/techpool/ontology/$CLASS/$PRODUCT/_in4/5_systemd/in4__$NAMING.service 	/etc/systemd/system/

systemctl daemon-reload
systemctl enable in4_$NAMING.service && systemctl restart in4_$NAMING.service 
systemctl status in4_$NAMING.service 
