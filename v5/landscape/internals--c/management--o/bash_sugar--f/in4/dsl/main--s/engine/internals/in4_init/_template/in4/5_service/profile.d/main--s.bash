########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash DSL, profiles
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########


_in4__rsyslog--g () {
    full_stack=$@
    SKIP=()


    for i in "$@"; do
        if [[ $i =~ "--" ]]; then
            SKIP+=("${i#*=}")
            shift
            PARAMS=$@
        else
            command=$i
            shift
            break
        fi
    done

        case $command in
        first_arg)
            $EXEC rsyslog--g ${SKIP[*]} $command --details $@
        ;;
        *)
            $EXEC rsyslog--g $full_stack
        ;;
    esac
}
alias rsyslog--g=_in4__rsyslog--g
alias srsyslog--g='EXEC="sudo";_in4__rsyslog--g'


