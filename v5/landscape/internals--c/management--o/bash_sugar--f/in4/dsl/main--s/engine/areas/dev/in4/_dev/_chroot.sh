            "chroot" )
                In4ExecPath="$In4GitPath/in4_core/internals/deploy/in4_bash/os"
                In4RunType="prod"
                if [[ -z $In4DeployOsMode ]]; then
                    DialogMsg="Please specify deploy OS mode"
                    echo $DialogMsg; select In4DeployOsMode in container_docker vm_xen hw_chroot hw_bootdrive;  do  break ; done
                fi
                
                in4func_bash_include $In4ExecPath/deploy_vars.sh
                
                case $In4DeployOsMode in
                "hw_bootdrive")
                    if [[ -z $In4HWBaseDisk ]]; then
                        DialogMsg="Please specify disk name for OS install"
                        echo $DialogMsg; select In4HWBaseDisk in sdb sdc sdd sde sdd;  do  break ; done
                    fi
                    mkdir -p $In4BuildEnv/loop
                    ! in4func_execute_as_root  mount /dev/${In4HWBaseDisk}2 $In4BuildEnv/loop/
                    ! in4func_execute_as_root  mount /dev/${In4HWBaseDisk}4 $In4BuildEnv/loop/media/sysdata                    
                ;;
                esac
                
                ! in4func_execute_as_root  mount -t proc proc $In4BuildEnv/loop/proc/
                ! in4func_execute_as_root  mount -t sysfs sys $In4BuildEnv/loop/sys/
                ! in4func_execute_as_root  mount -o bind /dev $In4BuildEnv/loop/dev/
                
                in4func_execute_as_root  chroot $In4BuildEnv/loop
            ;;
