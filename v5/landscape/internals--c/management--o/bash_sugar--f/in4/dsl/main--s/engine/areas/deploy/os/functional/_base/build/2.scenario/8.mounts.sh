#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

 ### MOUNTS  ###
#mounts
in4func_execute_as_is echo 'LABEL=system           /          btrfs       rw,noatime,acl,nodatacow,usebackuproot 1 1' > /etc/fstab ## TODO - remount to ro after firstboot
in4func_execute_as_is echo 'LABEL=swap           swap                 swap       defaults              0 0' >> /etc/fstab
in4func_execute_as_is echo 'tmpfs /tmp tmpfs defaults,noatime,mode=1777 0 0' >> /etc/fstab

in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "add" "mount" "media-storage"
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "add" "mount" "media-sysdata"
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "enable" "mount" "media-sysdata"
###

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
