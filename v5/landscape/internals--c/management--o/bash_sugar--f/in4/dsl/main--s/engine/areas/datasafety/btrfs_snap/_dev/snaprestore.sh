if [[ -z $In4TaskVars ]]; then
    DialogMsg="\n### Please specify mountpoint ###"   
    echo -e $DialogMsg; select In4SnapMountpoint in `mount|grep btrfs|awk '{print $3}'`;  do  break ; done
    DialogMsg="\n###  Please specify path to restore ###"   
    echo -e $DialogMsg; select In4SnapDirPathWoMounpoint in `btrfs subvolume list /media/storage|grep "_snap/"|awk '{print $9}'|sed  "s/_snap\/.*//"|uniq`;  do  break ; done              
    In4SnapDirPath=$In4SnapMountpoint/$In4SnapDirPathWoMounpoint
else
    In4SnapDirPath=$In4TaskVars
fi                   
in4func_bash_include $In4GitPath/v4/data_safety:c/fs:o/btrfs:f/in4_btrfs--g/snaprestore.sh
