#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###
in4func_bash_include $In4GitPath/in4_core/internals/in4_init/func.sh
in4func_bash_include $In4GitPath/in4_core/internals/env/v5_obs.sh
in4func_execute_as_is in4obs

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
