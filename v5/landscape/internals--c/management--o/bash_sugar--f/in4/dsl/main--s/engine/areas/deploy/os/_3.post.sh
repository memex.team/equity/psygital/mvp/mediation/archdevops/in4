#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

### offline mode ###
if [[ -z $In4OfflineCliMode ]]; then
    DialogMsg="Activate offline mode (as a client)?"   
    echo $DialogMsg; select In4OfflineCliMode in Yes No;  do  break ; done;
fi

if [[ $In4OfflineCliMode == "No" ]]; then

    if [[ -z $In4OfflineBuildMode ]]; then
        DialogMsg="Activate offline mode (as a offline pack builder)?"   
        echo $DialogMsg; select In4OfflineBuildMode in Yes No;  do  break ; done;
        
        if [[ $In4OfflineBuildMode == "Yes" ]]; then
        
            if [[ -z $In4OfflineBuildDir ]]; then
                DialogMsg="Please specify offline dir"   
                echo $DialogMsg; read In4OfflineBuildDir
            fi
            
        fi
    fi
fi


### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
