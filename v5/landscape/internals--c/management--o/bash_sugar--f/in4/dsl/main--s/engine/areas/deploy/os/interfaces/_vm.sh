#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###


In4OfflineBuildDir="$In4BuildEnv/offline"
In4OfflineCliMode="No"
In4OfflineBuildMode="Yes"
In4BuildLayers=(os)
In4ExportType="vm_raw"
if [[ -z $In4OsBuildMode ]];  then In4OsBuildMode="ram"; fi


### DISKS
In4Disk_Mode="per-disk"
In4Disk_RecreatePartitions="Yes"
In4Disk_SysdataOnBaseDisk="no"
In4Disk_StorageOnBaseDisk="No"
In4Disk_BaseDisk=$In4Disk_LoopSystem


if [[ -z $In4Disk_SizingUnit ]];  then In4Disk_SizingUnit="MiB"; fi
if [[ -z $In4Disk_SystemSize ]];  then In4Disk_SystemSize="1500"; fi
if [[ -z $In4Disk_SwapSize ]];    then In4Disk_SwapSize="50"   ; fi
if [[ -z $In4Disk_SysdataSize ]]; then In4Disk_SysdataSize="800"; fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
