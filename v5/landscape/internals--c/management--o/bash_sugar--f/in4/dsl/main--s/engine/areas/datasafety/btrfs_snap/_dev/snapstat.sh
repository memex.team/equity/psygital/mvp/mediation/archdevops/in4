if [[ -z $In4TaskVars ]]; then
    DialogMsg="\n### Please specify mountpoint ###"   
    echo -e $DialogMsg; select In4SnapMountpoint in `mount|grep btrfs|awk '{print $3}'`;  do  break ; done
else            
    In4SnapMountpoint=$In4TaskVars
fi
if [[ -z $In4SnapStatRescan ]]; then
        DialogMsg="Rescan statistics (caused to high CPU & Disk load)?"   
        echo $DialogMsg; select In4SnapStatRescan in Yes No;  do  break ; done
fi;
        
ruby $In4GitPath/v4/data_safety--c/fs--o/btrfs--f/in4_btrfs--g/snapstat.rb $In4SnapMountpoint $In4SnapStatRescan
