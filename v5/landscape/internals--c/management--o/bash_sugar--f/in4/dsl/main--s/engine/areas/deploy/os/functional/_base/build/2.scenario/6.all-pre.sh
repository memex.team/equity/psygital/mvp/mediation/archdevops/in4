#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###
if [[ $In4DeployOsMode =~ "container_" ]]; then
    in4func_execute_as_is rm -rf /media/sysdata/app
    in4func_execute_as_is mkdir -p /media/sysdata/app
    in4func_execute_as_is mkdir -p /media/sysdata/logs

else
    if [[ `btrfs subvolume list /media/sysdata |grep app` == '' ]]; then
      in4func_execute_as_is rm -rf /media/sysdata/app    
      in4func_execute_as_is btrfs subvolume create /media/sysdata/app
    fi
    if [[ `btrfs subvolume list /media/sysdata |grep logs` == '' ]]; then
      in4func_execute_as_is btrfs subvolume create /media/sysdata/logs
    fi
fi

in4func_execute_as_is mkdir -p /media/sysdata/logs/syslog /media/sysdata/logs/syslog_bus/_client
in4func_execute_as_is mkdir -p /media/sysdata/logs/var_log && ! rm -rf /var/log && ln -s /media/sysdata/logs/var_log /var/log
in4func_execute_as_is mkdir -p /media/sysdata/logs/app/atop /media/sysdata/logs/files	
###

### PASSWORD CHANGE & USRR/GROUP CREATION ###
#base user + group for applications
if [[ $(in4func_getgroup sysdata) == 'create' ]]; then
  in4func_execute_as_is groupadd -g 999 sysdata
fi
if [[ $(in4func_getuser sysdata) == 'create' ]]; then
  in4func_execute_as_is useradd -g sysdata -u 999 -M -d /media/sysdata sysdata
fi

#log viewer group, log writer user
if [[ $(in4func_getgroup log) == 'create' ]]; then
  in4func_execute_as_is groupadd -g 998 log
fi

if [[ $(in4func_getuser log) == 'create' ]]; then
  in4func_execute_as_is useradd -g log -u 998 -M -d /media/sysdata/logs/ log
fi

in4func_execute_as_is echo 'root:$6$nSVmX89/t4d3FeEj$SsxYBoKFzKmHCH3blLD.5wGKvGAt0Kgsdhw5jSfIYn.toup3rOhpGPd1VJklzrOs0Sh9Rxpb/rdodwQ52G0VE0' |chpasswd -e

#in4 app + git rw user + group
if [[ $(in4func_getgroup in4) == 'create' ]]; then
  in4func_execute_as_is groupadd -g 1000 in4
fi

if [[ $(in4func_getuser in4) == 'create' ]]; then
  in4func_execute_as_is useradd -g in4 -G log -u 1000 -m in4
fi
in4func_execute_as_is echo 'in4:$6$/s6M9tRSrvjfFm4j$ycq6WMdfiZTB27HErvJU49HpaL7KpRLUzFmcNlcCV1fdPR9KPI8owoQ7NcpTfbFXFwaqOvjcYrbEVnOWsD6zc1' |chpasswd -e
###


### PERMISSIONS ###
in4func_execute_as_is chmod 755 /media/sysdata/linux_sys/

in4func_execute_as_is chown -R sysdata:sysdata  /media/sysdata/app
in4func_execute_as_is setfacl -R -k /media/sysdata/app
in4func_execute_as_is setfacl -R -b /media/sysdata/app

in4func_execute_as_is setfacl -R -m u:sysdata:rwx /media/sysdata/app
in4func_execute_as_is setfacl -R -m d:u:sysdata:rwx /media/sysdata/app
in4func_execute_as_is setfacl -R -m g:sysdata:rx /media/sysdata/app
in4func_execute_as_is setfacl -R -m d:g:sysdata:rx /media/sysdata/app
in4func_execute_as_is setfacl -R -m u::rwx /media/sysdata/app	   
in4func_execute_as_is setfacl -R -m d:u::rwx /media/sysdata/app	        
in4func_execute_as_is setfacl -R -m g::rx /media/sysdata/app	   
in4func_execute_as_is setfacl -R -m d:g::rx /media/sysdata/app	        


in4func_execute_as_is setfacl -R -k $In4Path
in4func_execute_as_is setfacl -R -b $In4Path

in4func_execute_as_is chown -R in4:in4  $In4Path
in4func_execute_as_is setfacl -R -m u:in4:rwx $In4Path
in4func_execute_as_is setfacl -R -m d:u:in4:rwx $In4Path
in4func_execute_as_is setfacl -R -m g:in4:rwx $In4Path
in4func_execute_as_is setfacl -R -m d:g:in4:rwx $In4Path
in4func_execute_as_is setfacl -R -m u::rwx $In4Path	   
in4func_execute_as_is setfacl -R -m d:u::rwx $In4Path	        
in4func_execute_as_is setfacl -R -m g::rwx $In4Path	   
in4func_execute_as_is setfacl -R -m d:g::rwx $In4Path	             

in4func_execute_as_is setfacl -R -m o::rx $In4Path
in4func_execute_as_is setfacl -R -m d:o::rx $In4Path	        


in4func_execute_as_is chown -R log:log  /media/sysdata/logs
in4func_execute_as_is setfacl -R -k /media/sysdata/logs
in4func_execute_as_is setfacl -R -b /media/sysdata/logs

in4func_execute_as_is setfacl -R -m u:log:rwx /media/sysdata/logs
in4func_execute_as_is setfacl -R -m d:u:log:rwx /media/sysdata/logs
in4func_execute_as_is setfacl -R -m g:log:rx /media/sysdata/logs
in4func_execute_as_is setfacl -R -m d:g:log:rx /media/sysdata/logs
in4func_execute_as_is setfacl -R -m u::rwx /media/sysdata/logs	   
in4func_execute_as_is setfacl -R -m d:u::rwx /media/sysdata/logs	        
in4func_execute_as_is setfacl -R -m g::rx /media/sysdata/logs	   
in4func_execute_as_is setfacl -R -m d:g::rx /media/sysdata/logs	     


###

### SYSDATA PERMS OVERRIDE ###
in4func_execute_as_is mkdir -p  /var/lib/empty && chmod 700 /var/lib/empty
# in4func_execute_as_is usermod -G sysdata man
# in4func_execute_as_is usermod -G sysdata mail
###


### SYNC SERVICE ###
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "add" "service" "in4__sync"
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "add" "timer" "in4__sync"
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "enable" "timer" "in4__sync"
###

### FIRSTBOOT SERVICE ###
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "add" "service" "firstboot"
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "enable" "service" "firstboot"
###

### FIRSTBOOT SERVICE ###
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "add" "service" "everyboot"
in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "enable" "service" "everyboot"
###

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
