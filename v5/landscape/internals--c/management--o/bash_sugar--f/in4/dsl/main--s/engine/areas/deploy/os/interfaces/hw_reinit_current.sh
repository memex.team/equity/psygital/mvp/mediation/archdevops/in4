#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###
In4Disk_BaseDisk='No'
in4func_bash_include $In4CorePath/areas/deploy/os/interfaces/_hw.sh
In4OfflineCliMode="No"
In4OfflineBuildMode="No"
In4Disk_Mode="current"
In4Disk_RecreatePartitions="No"
DataDestroy="Skip"
In4BuildEnv='/'
In4BuildEnvPostfix=''
### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
