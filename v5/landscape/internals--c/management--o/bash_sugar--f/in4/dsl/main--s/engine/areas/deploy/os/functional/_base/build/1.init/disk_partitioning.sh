#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###
if [[ -z $In4Disk_Mode ]]; then echo "Please use correct In4Disk_Mode" && exit 1; fi
if [[ $In4Disk_BaseDisk == "sda" ]]; then echo "!!!! NO !!!!! THIS IS sda DISK !!!!" && exit 1; fi
if [[ $In4Disk_BaseDisk =~ "loop" ]]; then 
    DataDestroy="Yes"
    PartedPostfix=" -s "
fi

if [[ -z $In4Disk_RecreatePartitions ]]; then
    echo "Do you need to make disk partitions?"
    select In4Disk_RecreatePartitions in Yes No
    do  break; done        
fi

if [[ $In4Disk_RecreatePartitions == "Yes" ]]; then

    if [[ -z $In4Disk_SizingUnit ]]; then
        In4Disk_SizingUnit="GiB"
    fi
    In4Disk_SizingUnitShort="${In4Disk_SizingUnit:0:1}"

    if [[ -z $In4Disk_SystemSize ]]; then
        echo "Please enter in4 linux main image size in GiB (label = system): "
        select In4Disk_SystemSize in 10 15 20 30
        do  break; done        
    fi

    if [[ -z $In4Disk_SwapSize ]]; then
        echo "Please enter in4 linux swap size in GiB (label = swap): "
        select In4Disk_SwapSize in 1 5 10 15 20 
        do  break; done
    fi

    if [[ -z $In4Disk_SysdataSize ]]; then        
        echo "Please enter in4 linux sysdata size in GiB (label = sysdata): "in4func_bash_include $In4CorePath/areas/deploy/os/functional/deploy_vars.sh

        select In4Disk_SysdataSize in 5 10 15 20 30 40 50 80 100 ALL
        do  break; done
    fi
            

    if [[ -z $In4Disk_StorageOnBaseDisk ]]; then        
        echo "If storage disk belongs to in4 linux base disk ($In4Disk_BaseDisk) "
        select In4Disk_StorageOnBaseDisk in Yes No
        do break; done
    fi
    
    if [[ $In4Disk_StorageOnBaseDisk == "Yes" ]]; then
        if [[ -z $In4Disk_StorageSize ]]; then        
            echo "Please enter in4 linux storage size in GiB (label = storage): "
            select In4Disk_StorageSize in 5 10 15 20 30 40 50 80 100 ALL
            do  break; done
        fi                        
    else
        case $In4Disk_SizingUnit in
        "GiB" )
            In4Disk_AllocationM=$((($In4Disk_SystemSize + $In4Disk_SwapSize + $In4Disk_SysdataSize)*1024+200))
        ;;
        "MiB")
            In4Disk_AllocationM=$(($In4Disk_SystemSize + $In4Disk_SwapSize + $In4Disk_SysdataSize+200))
        ;;
        esac
        In4Disk_StorageSize=0
    fi
fi
    
    
                    
if [[ -z $DataDestroy ]]; then        
    echo "!!!   DATA WILL BE DESTROYED ON partition $In4Disk_BaseDisk using $In4Disk_Mode method !!"
    select DataDestroy in Yes No
    do  break; done
fi
    
case $DataDestroy in
"Yes") 
    ### all data in the same partition
    ## % in parted
    case $In4Disk_Mode in
        
    "single_reinit_ext_system")
        in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk set 2 boot on                                               
        in4func_execute_as_root mkfs.btrfs -f -L "system" /dev/${In4Disk_BaseDisk}${PartitionPrefix}2    
    ;;
    
    "single_reinit_ext_all")
        in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk set 2 boot on                                               
        in4func_execute_as_root mkfs.btrfs -f -L "system" /dev/${In4Disk_BaseDisk}${PartitionPrefix}2 
        in4func_execute_as_root mkfs.btrfs -f -L "sysdata" /dev/${In4Disk_BaseDisk}${PartitionPrefix}4        
    ;;  
    
    "single")
        if [[ $In4Disk_BaseDisk =~ "loop" ]]; then   
            PartitionPrefix="p" 
            In4Disk_IsoPath="/tmp/_os_build/$In4OsBuild.iso"
            case $In4OsBuildMode in
            "ram")
                in4func_execute_as_root mount -t tmpfs -o size=${In4Disk_AllocationM}M,nr_inodes=100,mode=777 tmpfs /tmp/_os_build
            ;;
            esac
            in4func_execute_as_root fallocate -l${In4Disk_AllocationM}MiB $In4Disk_IsoPath
            in4func_execute_as_root chmod 755 $In4Disk_IsoPath
            in4func_execute_as_root losetup /dev/$In4Disk_BaseDisk $In4Disk_IsoPath
        fi
        in4func_execute_as_root parted  $PartedPostfix /dev/$In4Disk_BaseDisk mklabel gpt
        in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk mkpart primary 1MiB 4MiB
        in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk set 1 bios_grub on
        in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk mkpart primary btrfs 5MiB ${In4Disk_SystemSize}$In4Disk_SizingUnit             
        in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk set 2 boot on                                               
        sleep 1
        in4func_execute_as_root mkfs.btrfs -f -L "system" /dev/${In4Disk_BaseDisk}${PartitionPrefix}2
        in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk mkpart primary linux-swap ${In4Disk_SystemSize}$In4Disk_SizingUnit $(($In4Disk_SystemSize+$In4Disk_SwapSize))$In4Disk_SizingUnit
        sleep 1
        in4func_execute_as_root mkswap -f -L "swap" /dev/${In4Disk_BaseDisk}${PartitionPrefix}3
        if [[  $In4Disk_SysdataSize == "ALL" ]]; then
            FdiskEnd="100%"
        else
            FdiskEnd="$(($In4Disk_SystemSize+$In4Disk_SwapSize+$In4Disk_SysdataSize+$In4Disk_StorageSize))$In4Disk_SizingUnit"
        fi                         
        in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk mkpart primary btrfs $(($In4Disk_SystemSize+$In4Disk_SwapSize))$In4Disk_SizingUnit $FdiskEnd

        sleep 1
        in4func_execute_as_root mkfs.btrfs -f -L "sysdata" /dev/${In4Disk_BaseDisk}${PartitionPrefix}4
        
        if [[ $In4Disk_StorageOnBaseDisk == "Yes" ]]; then
            if [[  $In4Disk_StorageSize == "ALL" ]]; then
                FdiskEnd="100%"
            else
                FdiskEnd="$(($In4Disk_SystemSize+$In4Disk_SwapSize+$In4Disk_SysdataSize+$In4Disk_StorageSize))$In4Disk_SizingUnit"
            fi                
            in4func_execute_as_root parted  /dev/$In4Disk_BaseDisk mkpart primary btrfs $(($In4Disk_SystemSize+$In4Disk_SwapSize+$In4Disk_SysdataSize))$In4Disk_SizingUnit $FdiskEnd
            sleep 1
            in4func_execute_as_root mkfs.btrfs -f -L "storage" /dev/${In4Disk_BaseDisk}${PartitionPrefix}5
        fi
    ;;  
    
    "per-disk")
        case $In4OsBuildMode in
        "ram")
            in4func_execute_as_root mount -t tmpfs -o size=${In4Disk_AllocationM}M,nr_inodes=100,mode=777 tmpfs /tmp/_os_build
        ;;
        esac
        in4func_execute_as_is fallocate -l$In4Disk_SystemSize$In4Disk_SizingUnit /tmp/_os_build/$In4OsBuild.raw
        in4func_execute_as_root mkfs.btrfs -f -L "system" /tmp/_os_build/$In4OsBuild.raw
        
        #in4func_execute_as_root parted  $PartedPostfix /tmp/_os_build/$In4OsBuild.raw mklabel gpt
        #in4func_execute_as_root parted  /tmp/_os_build/$In4OsBuild.raw mkpart primary btrfs 2048s 100%         
        #in4func_execute_as_root parted  /tmp/_os_build/$In4OsBuild.raw set 1 boot on                  

        in4func_execute_as_is fallocate -l $In4Disk_SysdataSize$In4Disk_SizingUnit /tmp/_os_build/sysdata.raw
        in4func_execute_as_root mkfs.btrfs -f -L "sysdata" /tmp/_os_build/sysdata.raw

        in4func_execute_as_is fallocate -l $In4Disk_SwapSize$In4Disk_SizingUnit /tmp/_os_build/swap.raw
        in4func_execute_as_root mkswap -f -L "swap" /tmp/_os_build/swap.raw 
    ;;
            
esac

  case $In4Disk_Mode in
  "per-disk")
      ### GENERATE LOOP MOUNT & UNTAR ###
      in4func_execute_as_root mkdir -p  $In4BuildEnv/loop
      #in4func_execute_as_root losetup -o 2048 /dev/$In4Disk_LoopSystem /tmp/_os_build/$In4OsBuild.raw
      #in4func_execute_as_root mkfs.btrfs -A 1k -f -L "system" /dev/$In4Disk_LoopSystem    
      in4func_execute_as_root losetup /dev/$In4Disk_LoopSystem /tmp/_os_build/$In4OsBuild.raw    
      in4func_execute_as_root mount   /dev/$In4Disk_LoopSystem $In4BuildEnv/loop
      
      in4func_execute_as_root mkdir -p  $In4BuildEnv/loop/media/sysdata
      in4func_execute_as_root losetup /dev/$In4Disk_LoopSysdata /tmp/_os_build/sysdata.raw
      in4func_execute_as_root mount /dev/$In4Disk_LoopSysdata $In4BuildEnv/loop/media/sysdata    
  ;;

  single*)
      in4func_execute_as_root mount /dev/${In4Disk_BaseDisk}${PartitionPrefix}2  $In4BuildEnv/loop/
      in4func_execute_as_root mkdir -p  $In4BuildEnv/loop/media/sysdata
      in4func_execute_as_root mount /dev/${In4Disk_BaseDisk}${PartitionPrefix}4 $In4BuildEnv/loop/media/sysdata    
  ;;

  "dir_based")
      ! in4func_execute_as_root btrfs subvolume delete $In4BuildEnv/loop/media/sysdata/linux_sys/*
      in4func_execute_as_root rm -rf $In4BuildEnv/loop /tmp/_os_build/in4_$In4Context 
      in4func_execute_as_root mkdir -p /tmp/_os_build/in4_$In4Context 
      in4func_execute_as_root chmod -R 777 /tmp/_os_build
      in4func_execute_as_root ln -s /tmp/_os_build/in4_$In4Context $In4BuildEnv/loop 
  ;;
  *)
      echo "Not mounted!! Exit"
      exit 1
  ;;

  esac
;;
"Skip")
  echo 'Disk partitioning skipped'
;;
"No") exit 1;;
esac



### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
