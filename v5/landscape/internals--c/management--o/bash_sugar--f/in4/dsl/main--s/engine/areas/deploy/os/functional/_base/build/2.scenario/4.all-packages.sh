    #!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ $In4DeployOsMode =~ "container_" ]]; then
    in4func_Zypper $In4ExecPath/_base/build/2.scenario/4.container-packages.suse
else
    in4func_Zypper $In4ExecPath/_base/build/2.scenario/4.container-packages.suse
    in4func_Zypper $In4ExecPath/_base/build/2.scenario/4.os-packages.suse
fi

if [[ " ${In4BuildLayers[@]} " =~ " unit " ]]; then
    in4func_Zypper $In4ExecPath/_base/build/1.init/1.pre_packages.suse
fi

if [[ " ${In4BuildLayers[@]} " =~ " os " ]] && ! [[ $In4DeployOsMode =~ "container_" ]]; then 
    #in4func_ZypperRepo add  "in4::internals--c--linux_sys--o--kernel--f--kernel_4.12--g" ##4.12
    in4func_ZypperRepo add  "in4::internals--c--linux_sys--o--kernel--f--kernel_mainline--g" ##mainline # BUG
    in4func_Zypper $In4ExecPath/_base/build/2.scenario/4.kern-packages.suse
fi

if [[ $In4DeployOsMode =~ "vm_" ]]; then in4func_bash_include $In4ExecPath/vm/build/2.scenario/4.vm-packages.sh; fi
if [[ " ${In4BuildLayers[@]} " =~ " unit " ]]; then in4func_bash_include $In4ExecPath/hw/build/2.scenario/4.hw-packages.sh; fi

 #+ pam + policy*


### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
