#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ -z $In4LandscapeFQN ]]; then
    In4LandscapeFQN=$1
fi            
if [[ -z $In4RunPath ]]; then
    In4RunPath=$2
fi       
if [[ -z $In4RunName ]]; then
    In4RunName=$3
fi  

in4func_run

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
