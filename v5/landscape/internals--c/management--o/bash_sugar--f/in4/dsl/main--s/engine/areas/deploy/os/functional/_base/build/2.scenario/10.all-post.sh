#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ $In4OfflineCliMode != "Yes" ]]; then
    in4func_execute_as_is rpm --rebuilddb
    ZypperOperation="dup"
    in4func_Zypper
fi

if ! [[ $In4DeployOsMode =~ "container_" ]]; then
  in4func_execute_as_is chown root:root /etc/sssd/sssd.conf
  in4func_execute_as_is chmod 700 /etc/sssd/sssd.conf
  
  in4func_execute_as_is chown root:root /etc/sudoers
fi
###

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
