#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ -z $In4DeployAppRecipe ]]; then
    DialogMsg="Please specify In4 application recipe"
    echo $DialogMsg; select In4DeployAppRecipe in `in4func_findobj "$In4GitPath/$In4Context/$In4ContextPrefix/$In4DeployAppClass/$In4DeployAppOrder/$In4DeployAppFamily/$In4DeployAppGenus/_recipes/" 'f'`;  do  break ; done
fi

if [[ -f $In4GitPath/$In4Context/$In4ContextPrefix/$In4DeployAppClass/$In4DeployAppOrder/$In4DeployAppFamily/$In4DeployAppGenus/_recipes/$In4DeployAppRecipe.sh ]]; then
    in4func_bash_include $In4GitPath/$In4Context/$In4ContextPrefix/$In4DeployAppClass/$In4DeployAppOrder/$In4DeployAppFamily/$In4DeployAppGenus/_recipes/$In4DeployAppRecipe.sh
else
    echo "No recipe!!" && exit 1
fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
