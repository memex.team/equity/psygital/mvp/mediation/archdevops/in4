#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

In4DeployOsMode=$In4Context
In4OsVendor="in4"
In4OsRelease="15_1"

In4OsReleaseWDot=`echo $In4OsRelease|sed -e "s/_/\./"`
In4OsSrvType="$In4OsRelease-$In4OsVendor-l"

if [[ -z $In4OsArch ]]; then
    DialogMsg="Please specify platform arch"   
    echo $DialogMsg; select In4OsArch in $In4OsArchAvailable;  do  break ; done;
fi                

if [[ -z $In4OsBuildGitContextPassword ]]; then
    DialogMsg="Please enter a password for git context repo or press enter to skip context init"
    echo $DialogMsg; read In4OsBuildGitContextPassword
fi

if [[ -z $In4OsBuildGitTag ]]; then
    DialogMsg="Please specify GIT tag for build type: "
    #echo $DialogMsg; select  OsBuildGitTag in `git -C $In4GitPath  -C $GitPath name-rev --tags --name-only $(git -C $In4GitPath  -C $GitPath rev-parse HEAD)` ;  do  break ; done;
    echo $DialogMsg; select  In4OsBuildGitTag in $In4OsBuildGitTagAvailable ;  do  break ; done;                    
fi                
In4OsBuildDate=`date +"w"%W"y"%y`
In4OsBuildGitTagWoDot=`echo $In4OsBuildGitTag|sed -e "s/\./_/"`
In4OsBuild="$In4OsBuildDate-$In4OsBuildGitTagWoDot-in4"

if [[ -z $In4BuildEnv ]];  then In4BuildEnv="/media/sysdata/_os_build"; fi
if [[ -z $In4BuildEnvPostfix ]]; then In4BuildEnvPostfix='loop'; fi
if [[ -z $In4OfflineBuildDir ]];  then In4OfflineBuildDir="$In4BuildEnv/offline"; fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
