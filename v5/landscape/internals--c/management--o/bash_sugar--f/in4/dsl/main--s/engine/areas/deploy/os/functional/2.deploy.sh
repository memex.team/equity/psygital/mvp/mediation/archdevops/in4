#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ -z $In4OsDeployMode ]]; then
    DialogMsg="Please select deploy mode: "
    echo $DialogMsg; select In4OsDeployMode in xen docker;  do  break ; done
fi

if [[ $In4OsDeployMode == "xen" ]] ; then in4func_bash_include $In4ExecPath/vm/run/4.run/deploy_xen.sh; fi

tput setaf 2
echo -e "${green}\n\n\n ################# 2.DEPLOYED OK #################"
tput setaf 9

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###


