########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash DSL, profiles
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

gitOffline () {
    ! git -C $In4GitPath add $In4GitPath/*
    ! git -C $In4GitPath commit -m "$In4gitCommitMessage"
}

gitPush () {
    git -C $In4GitPath push $gitRepo $gitBranch 
}

gitPullMergePushCycle () {
    ! gitOffline
    git -C $In4GitPath pull $gitRepo $gitBranch
    git -C $In4GitPath checkout $gitBranch
    if ! [[ -z $gitMergeWithBranch ]]; then
        git -C $In4GitPath merge $gitMergeWithBranch -m "$In4gitCommitMessage"
    fi
    if ! [[ $gitManualAddPush == "yes" ]]; then
        ! gitOffline
        gitPush
    fi
}

gitIn4CycleDev () {
    gitRepo="dev"
    gitBranch="master"
    gitPullMergePushCycle
}


gitIn4CycleDevStaging () {
    gitRepo="dev"
    gitBranch="master"
    gitMergeWithBranch="staging"
    gitPullMergePushCycle

    gitRepo="dev"
    gitBranch="staging"
    gitMergeWithBranch="master"    
    gitPullMergePushCycle
    
    gitRepo="origin"
    gitBranch="staging"
    gitPush
    
    git -C $In4GitPath checkout master
}   


gitIn4CycleV1Stable () {
    gitRepo="dev"
    gitBranch="v1-stable"
    gitMergeWithBranch="staging"
    #gitManualAddPush="yes"
    gitPullMergePushCycle

    
    gitRepo="origin"
    gitBranch="v1-stable"
    gitPush
    
    git -C $In4GitPath checkout master
}   

