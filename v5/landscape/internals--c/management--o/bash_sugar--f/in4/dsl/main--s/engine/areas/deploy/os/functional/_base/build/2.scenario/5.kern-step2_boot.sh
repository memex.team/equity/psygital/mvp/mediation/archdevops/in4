#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

 ### DRACUT  ###
in4func_run "internals--c--linux_sys--o--boot--f--dracut--g--main--s" "2_init/opensuse" "in4__main--s.package.zypper.sh"
in4func_cp "dsl::internals--c--linux_sys--o--boot--f--dracut--g--main--s" "simple/dracut.conf-vm" "/etc/dracut.conf"
 ### 
 
 ### GRUB2  ###
in4func_run "internals--c--linux_sys--o--boot--f--grub2--g--main--s" "2_init/opensuse" "in4__main--s.package.zypper.sh"
in4func_cp "dsl::internals--c--linux_sys--o--boot--f--grub2--g--main--s" "simple/etc_default_grub--vm_xen" "/etc/default/grub"
in4func_execute_as_is mkdir -p  /boot/grub2/
in4func_cp "dsl::internals--c--linux_sys--o--boot--f--grub2--g--main--s" "simple/boot_grub2_grub.cfg"  "/boot/grub2/grub.cfg"
 ### 
 
if ! [[ $In4DeployOsMode =~ "current" ]]; then
  ### BOOT, GRUB2 init ###
  case $In4DeployOsMode in
      vm_*) export _GRUB2_DISK=$In4Disk_LoopSystem; export _GRUB2_DEFAULT_BOOT="In4 - VM"; in4func_run "internals--c--linux_sys--o--boot--f--grub2--g--main--s" "3_recipe/in4_shell" "grub2_install.recipe.sh"
    ;; 
      hw_*) export _GRUB2_DISK=$In4Disk_BaseDisk; export _GRUB2_DEFAULT_BOOT="In4 - HW"; in4func_run "internals--c--linux_sys--o--boot--f--grub2--g--main--s" "3_recipe/in4_shell" "grub2_install.recipe.sh"
    ;; 
      esac  
fi

#DOUBLE
in4func_execute_as_is depmod `ls -la /boot/vmlinuz|awk '{print $11}'|sed 's/vmlinuz-//'`
#in4func_execute_as_is mkinitrd -A ## will be exec by DRBD
###

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
