#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###
if [[ $In4OfflineCliMode == "Yes" ]]; then
    echo "Offline mode, all packages are cached"
elif  [[ $In4OfflineBuildMode == "Yes" ]]; then
    ZypperFlag=1
else
    ZypperFlag=1
fi     

# if [[ $ZypperFlag == 1 ]]; then ! in4func_Zypper $In4ExecPath/_base/build/1.init/1.pre_packages.suse; fi # BUG - slowdown
In4OsOBSName="openSUSE_Leap_$In4OsReleaseWDot"

if [[ $In4Disk_Mode == 'current' ]]; then
  echo 'Ok'
else
  in4func_execute_as_root rm -rf $In4BuildEnv/loop
  in4func_execute_as_root mkdir -p $In4OfflineBuildDir    
  in4func_execute_as_is mkdir -p /tmp/_os_build
  
  in4func_execute_as_root mkdir -p $In4BuildEnv/images/$In4DeployOsMode
  in4func_execute_as_root mkdir -p $In4BuildEnv/loop
  in4func_execute_as_root chmod -R 777 $In4BuildEnv
  in4func_execute_as_is cd $In4BuildEnv

  ### docker image URI ###

  case $In4OsVendor in
      "in4")
          In4OsImageFilename="opensuse-leap-$In4OsReleaseWDot-image.x86_64-docker.tar.xz"
          OsImageURI="https://download.opensuse.org/repositories/home:/in4ops:/base/images/$In4OsImageFilename"
      ;;
  esac
  ### 

  in4func_wget "$OsImageURI" "$In4OfflineBuildDir/$In4OsImageFilename"
fi 

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
