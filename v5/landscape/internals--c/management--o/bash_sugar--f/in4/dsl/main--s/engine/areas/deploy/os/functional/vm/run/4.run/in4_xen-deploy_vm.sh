#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

### CREATE XL CONF IN SVN ###
VMConfPath="/media/sysdata/app/xen/$VM_HV_ORG/$VM_HV_NAME/$Org/$SrvRole/$DeplType"
VMDiskPath="/media/storage/vm/$Org/$SrvRole/$DeplType/$SrvName"

in4func_execute_as_root mkdir -p $VMConfPath
in4func_bash_include  $In4CorePath/areas/deploy/os/functional/vm/run/4.run/vm_xen_tmpl.xl
 ###
 
### PROMOTE VM ON HV ###
#systemctl -H $VM_HV_NAME start in4_vm-deploy@$FullSrvName
##


if [[ -d $VMDiskPath ]]; then
    echo "VM is already exists!!! "; exit 1
else
    in4func_execute_as_root mkdir -p $VMDiskPath
    in4func_execute_as_root cp --sparse=always $In4BuildEnv/images/$In4DeployOsMode/${In4OsBuild}.raw $VMDiskPath/${In4OsBuild}.raw
    in4func_execute_as_root fallocate -l 10G $VMDiskPath/${In4OsBuild}.raw
    in4func_execute_as_root cp --sparse=always $In4BuildEnv/images/$In4DeployOsMode/swap.raw $VMDiskPath/swap.raw
    in4func_execute_as_root cp --sparse=always $In4BuildEnv/images/$In4DeployOsMode/sysdata.raw $VMDiskPath/sysdata.raw
    in4func_execute_as_root fallocate -l 20G $VMDiskPath/sysdata.raw
    #VM_DISK_STORAGE_SIZE=$VM_DISK_STORAGE_SIZE
    #in4func_execute_as_root fallocate -l `cat $SVN_VMConfPath|grep "VM_DISK_STORAGE_SIZE"|cut -d= -f2` $VMDiskPath/storage.raw ## FROM SVN
    in4func_execute_as_root fallocate -l  ${VM_DISK_STORAGE_SIZE_OVERALL}G $VMDiskPath/storage.raw
    in4func_execute_as_root mkfs.btrfs -f -L "storage" $VMDiskPath/storage.raw
    in4func_execute_as_root fallocate -l 1G $VMDiskPath/storage_meta.raw
    sleep 1
    in4func_execute_as_root xl create $VMConfPath/${SrvName}.xl
     #in4func_execute_as_root systemctl start in4_vm@$FullSrvName
fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
