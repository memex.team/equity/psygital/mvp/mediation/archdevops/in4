#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash DSL, profiles
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if ! [[ $In4Disk_Mode == 'current' ]]; then

  ### UNTAR ###
  in4func_execute_as_root tar xf $In4OfflineBuildDir/$In4OsImageFilename -C $In4BuildEnv/$In4BuildEnvPostfix/
  # PackedTar=`find /tmp/_os_build/ -type f  -size +10M -name "sha*"`
  # in4func_execute_as_root tar xf $PackedTar -C $In4BuildEnv/$In4BuildEnvPostfix/
  in4func_execute_as_root rm -rf $In4BuildEnv/$In4BuildEnvPostfix/media/sysdata/*
  ###

  ###  CP OWN FILES ###
  in4func_execute_as_root cp /etc/resolv.conf $In4BuildEnv/$In4BuildEnvPostfix/etc/
  in4func_execute_as_root cp /etc/hosts $In4BuildEnv/$In4BuildEnvPostfix/etc/
  ! in4func_execute_as_root cp /etc/sysconfig/proxy $In4BuildEnv/$In4BuildEnvPostfix/etc/sysconfig/
  in4func_execute_as_root chmod 744  $In4BuildEnv/$In4BuildEnvPostfix/etc/sysconfig/
  ### 
  
  In4Path="$In4BuildEnv/$In4BuildEnvPostfix$In4Path"
  In4GitChrootPath="$In4Path/platform"
  in4func_execute_as_root mkdir -p $In4Path
  ### GIT INIT ###
  if [[ $In4OfflineCliMode == "Yes" ]] ||  [[ $In4OfflineBuildMode == "Yes" ]] ; then
      if in4func_execute_as_root_wo_tests test -d $In4GitPath ; then
          in4func_execute_as_root git init $In4GitChrootPath
          in4func_execute_as_root git  -C $In4GitChrootPath pull $In4GitPath
          in4func_execute_as_root git  -C $In4GitChrootPath remote add origin $In4GitRepoStable
          in4func_execute_as_root git  -C $In4GitChrootPath remote add dev $In4GitRepoDev
          in4func_execute_as_root git  -C $In4GitChrootPath config core.filemode false
          #in4func_execute_as_root git  -C $In4GitChrootPath checkout tags/$In4OsBuildGitTag ## BUG
      else
          gitOnline="Yes"
      fi
  else
      gitOnline="Yes"
  fi    

  if [[ $gitOnline == "Yes" ]]; then
      in4func_execute_as_root git -C $In4Path clone -b $In4GitStablePath  $In4GitRepoStable
      in4func_execute_as_root mv $In4Path/in4 $In4GitChrootPath
      in4func_execute_as_root git -C $In4GitChrootPath remote add dev $In4GitRepoDev  
      in4func_execute_as_root git  -C $In4GitChrootPath fetch dev    
      in4func_execute_as_root git -C $In4GitChrootPath config core.filemode false
      #in4func_execute_as_root git  -C $In4GitChrootPath checkout tags/$In4OsBuildGitTag ## BUG
  fi     

  if [[ $RunType == "dev" ]]; then
      in4func_execute_as_root git -C $In4GitChrootPath checkout dev/master -B master
      in4func_execute_as_root git -C $In4GitChrootPath reset --hard remotes/master
      #in4func_execute_as_root git -C $In4GitChrootPath branch --set-upstream-to=remotes/master    
  fi
  ###
  view=`hostname -f|cut -d '.' -f 4` # BUG - use naming 

  if ! [[ $In4OsBuildGitContextPassword == "" ]]; then
    in4func_execute_as_root git -C $In4GitChrootPath branch --set-upstream-to=origin/master 
    in4func_execute_as_root git clone https://infra_$view:$In4OsBuildGitContextPassword@gitlab.com/Innosense/infrastructure/$view.git $In4ContextPath
    in4func_execute_as_root git -C $In4ContextPath config credential.helper store
  fi

  in4func_bash_include $In4ExecPath/_base/build/1.init/offline_merge.sh main_to_loop

  ###  CHROOT TO LOOP ###
  in4func_execute_as_root mkdir -p $In4BuildEnv/$In4BuildEnvPostfix/proc/ $In4BuildEnv/$In4BuildEnvPostfix/sys/ $In4BuildEnv/$In4BuildEnvPostfix/dev/
  in4func_execute_as_root mount -t proc proc $In4BuildEnv/$In4BuildEnvPostfix/proc/ &&  in4func_execute_as_root mount -t sysfs sys $In4BuildEnv/$In4BuildEnvPostfix/sys/ && in4func_execute_as_root mount -o bind /dev $In4BuildEnv/$In4BuildEnvPostfix/dev/

fi

### ENV ##
# env|grep "In4" > $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env_v2.sh
echo "#!/bin/bash" > $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4GitPath=\"$In4GitPath\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4ExecPath=\"$In4ExecPath\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OsVendor=\"$In4OsVendor\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OsRelease=\"$In4OsRelease\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OsOBSName=\"$In4OsOBSName\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OsArch=\"$In4OsArch\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OsBuildGitTag=\"$In4OsBuildGitTag\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OsBuildGitTagWoDot=\"$In4OsBuildGitTagWoDot\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OsBuild=\"$In4OsBuild\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh

echo "In4OfflineCliMode=\"$In4OfflineCliMode\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OfflineBuildMode=\"$In4OfflineBuildMode\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OfflineBuildDir=\"$In4OfflineBuildDir\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4DeployOsMode=\"$In4DeployOsMode\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4BuildLayers=(${In4BuildLayers[@]})" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
echo "In4OBSBuildRepo=\"$In4OBSBuildRepo\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh



if [[ " ${In4BuildLayers[@]} " =~ " unit " ]]; then 
    echo "In4Disk_BaseDisk=\"$In4Disk_BaseDisk\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
fi

if [[ $In4DeployOsMode =~ "vm_" ]] ; then 
    echo "In4Disk_LoopSystem=\"$In4Disk_LoopSystem\"" >> $In4BuildEnv/$In4BuildEnvPostfix/tmp/in4_env.sh
fi
##

if [[ $In4ShellDebug == "Yes" ]]; then In4ShellRunMode=" -x "; else In4ShellRunMode=""; fi
In4Runner="sh $In4ShellRunMode $In4CorePath/areas/deploy/os/functional/_base/build/2.scenario/scenario_${In4OsVendor}_${In4OsReleaseWDot}.sh"

if [[ $In4Disk_Mode == 'current' ]]; then
 /bin/bash -c "$In4Runner"
else
  sudo chroot $In4BuildEnv/$In4BuildEnvPostfix /bin/bash -c "$In4Runner" ## BUG - needs to be "in4func_execute_as_root"
  echo -e "\n\n##### END OF CHROOT ####\n\n"
fi

if in4func_execute_as_root test -d ; then 
    echo "!!! Scenario Succeed !!!"
else
    echo "!!! Scenario failed !!!" && exit 1
fi
 
### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###

