#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

### ZYPPER CONF ###
in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g--main--s" "3_recipe/in4_shell" "zypper.recipe.sh"
###

### PRE-PACKAGE - STRICTLY NEEDED ###
zypper --non-interactive  --no-gpg-checks --no-refresh -C /media/sysdata/offline/zypper/zypp_offline in --force-resolution btrfsprogs
###

 in4func_execute_as_is mkdir -p /media/sysdata/linux_sys
 
 _relink_to_linux_sys(){
  Src=$1
  if [[ `btrfs subvolume list /media/sysdata |grep $Src` == '' ]]; then 
    if [[ $In4DeployOsMode =~ "container_" ]]; then
        in4func_execute_as_is mkdir -p /media/sysdata/linux_sys/$Src
    else
        in4func_execute_as_is btrfs subvolume create /media/sysdata/linux_sys/$Src
    fi
    if [[ -L /$Src ]]; then in4func_execute_as_is rm /$Src ; fi
    if [[ -e /$Src ]]; then in4func_execute_as_is mv /$Src /${Src}_prev && in4func_execute_as_is cp -pR /${Src}_prev/* /media/sysdata/linux_sys/$Src ; rm -rf /${Src}_prev; fi
    in4func_execute_as_is ln -s /media/sysdata/linux_sys/$Src /$Src
  fi
 }
 

#_relink_to_linux_sys '/etc' ## BUG
_relink_to_linux_sys '/var'
_relink_to_linux_sys '/root'
! _relink_to_linux_sys '/home'
in4func_execute_as_is chmod 755  /media/sysdata/linux_sys/home

# ! in4func_execute_as_is rm -r /var/tmp; in4func_execute_as_is ln -s /tmp /var/tmp ## BUG - systemd private tmp bug
 ! in4func_execute_as_is rm -r /var/run; in4func_execute_as_is ln -s /run /var/run

in4func_execute_as_is chmod a+x /media/sysdata/linux_sys/var/

### ZYPPER OFFLINE ###
if [[ -d /media/sysdata/offline/zypper/zypp_offline ]]; then
     in4func_execute_as_is rm -rf /var/cache/zypp_offline
     in4func_execute_as_is mv /media/sysdata/offline/zypper/zypp_offline /var/cache/
fi
### 

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
