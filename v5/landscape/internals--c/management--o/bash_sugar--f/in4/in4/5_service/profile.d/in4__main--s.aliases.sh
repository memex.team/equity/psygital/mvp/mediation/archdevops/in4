########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash DSL, profiles
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

alias in4__logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s__sdo-...='sudo FULL_PATH ARGS ...'
alias in4__short__rsyslog--g__sdo-...=FULL_ALIAS

alias in4__logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s__do-...='FULL_PATH ARGS ...'
alias in4__short__rsyslog--g__do-...=FULL_ALIAS


