#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ -f /etc/systemd/system/firstboot.service ]]; then  

    ! in4func_execute_as_is btrfs filesystem resize max / 
    ! in4func_execute_as_is btrfs filesystem resize max /media/sysdata/

    ### /dev/xvd* blacklisting   ###
    #in4func_cp "internals--c--linux_sys--o--boot--f--dracut--g--main--s" "simple/51-block-xenvm_blacklist.conf" "/etc/modprobe.d/"
    ###
    in4func_execute_as_is depmod `ls -la /boot/vmlinuz|awk '{print $11}'|sed 's/vmlinuz-//'`
    in4func_execute_as_is mkinitrd
    
    hostnamectl status > /tmp/hostnamectl_status
    Chassis=`cat /tmp/hostnamectl_status|grep "Chassis:"|awk '{print $2}'` #desktop, laptop, vm, n/a
    
    case $Chassis in
    vm)
        Virtualized=`cat /tmp/hostnamectl_status|grep "Virtualization:"|awk '{print $2}'`
        case $Virtualized in 
        "xen") in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g" "3_recipe/runners/vm" "init_auto_xen.sh"
        ;;
        
        esac
    ;;
    
    *)
        in4func_run "internals--c--management--o--bash_sugar--f--kitchen--g" "3_recipe/runners/hw" "init_auto_hw.sh"
    ;;
    esac
    in4func_bash_include $In4CorePath/areas/deploy/os/functional/_base/build/2.scenario/99.post_once.sh

    ## REGENERATE machine-id
    in4func_execute_as_is rm /etc/machine-id  
    in4func_execute_as_is systemd-machine-id-setup
    ## 

    
    ## TODO - place for init / snapshot
    in4func_execute_as_is

    ## DISABLE FIRSTBOOT
    in4func_execute_as_is systemctl disable firstboot
    in4func_execute_as_is rm /etc/systemd/system/firstboot.service
    
    ### SECONDBOOT SERVICE ###
    in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "add" "service" "secondboot"
    in4func_systemd "internals--c--management--o--bash_sugar--f--in4--g--main--s" "enable" "service" "secondboot"    
    
    in4func_execute_as_is reboot
    
fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
