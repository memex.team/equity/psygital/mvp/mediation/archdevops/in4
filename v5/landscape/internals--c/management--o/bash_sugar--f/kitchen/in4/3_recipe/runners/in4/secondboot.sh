#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

if [[ -f /etc/systemd/system/secondboot.service ]]; then  

  ## GIT
  git -C /media/sysdata/in4/platform fetch
  git -C /media/sysdata/in4/platform branch --set-upstream-to=origin/master
  ##
    
  in4func_execute_as_is systemctl disable secondboot
  in4func_execute_as_is rm /etc/systemd/system/secondboot.service
    
fi

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
