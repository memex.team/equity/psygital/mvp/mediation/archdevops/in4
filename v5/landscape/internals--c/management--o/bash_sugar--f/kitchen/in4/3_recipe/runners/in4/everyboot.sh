#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

systemctl daemon-reexec

### HEAVY WAY
# printf "+cpu +memory +io +pids" > /sys/fs/cgroup/cgroup.subtree_control
# printf "+cpu +memory +io +pids" > /sys/fs/cgroup/user.slice/cgroup.subtree_control
# bash -c 'printf "+cpu +memory +io +pids" > /sys/fs/cgroup/user.slice/user-*.slice/cgroup.subtree_control'

### LIGHT WAY
sed -i 's/Delegate=.*/Delegate=all/' /usr/lib/systemd/system/user@.service

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
