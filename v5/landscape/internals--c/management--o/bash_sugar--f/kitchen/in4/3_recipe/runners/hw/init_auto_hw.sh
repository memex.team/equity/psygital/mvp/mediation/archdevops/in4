#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

in4func_execute_as_is timedatectl set-timezone Europe/Moscow

INTERFACES=`wicked show-xml all|grep "<name>"|sed "s/<name>//; s/<\/name>//"|awk '{print $1}'|grep -v "^lo$"`
for INTERFACE in $INTERFACES; do
    in4func_execute_as_is cp $In4CorePath/areas/deploy/os/functional/hw/build/3.env/dhcp /etc/sysconfig/network/ifcfg-$INTERFACE ## BUG
done

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
