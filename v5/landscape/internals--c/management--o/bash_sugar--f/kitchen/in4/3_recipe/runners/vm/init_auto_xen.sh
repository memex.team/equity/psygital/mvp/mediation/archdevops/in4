#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

### IN4 BASH HEADER ###
set -e
in4func_logit BEGIN ${BASH_SOURCE[0]}
###

###  DISABLE NET DHCP ### 
in4func_execute_as_is cp /media/sysdata/in4/platform/v5/landscape/internals--c/management--o/bash_sugar--f/in4/dsl/main--s/engine/areas/deploy/os/functional/_base/build/3.env/wtf/wickedd-dhcp /usr/lib/systemd/system/wickedd-auto4.service
in4func_execute_as_is cp /media/sysdata/in4/platform/v5/landscape/internals--c/management--o/bash_sugar--f/in4/dsl/main--s/engine/areas/deploy/os/functional/_base/build/3.env/wtf/wickedd-dhcp /usr/lib/systemd/system/wickedd-dhcp4.service
in4func_execute_as_is systemctl disable wickedd-dhcp6
in4func_execute_as_is systemctl mask wickedd-dhcp6

### 
DOMID=`xenstore-read domid`
NAME=`xenstore-read /local/domain/$DOMID/name`
in4func_bash_include /media/sysdata/in4/platform/in4_core/internals/naming/naming.sh os $NAME

if [[ $View == "os" ]]; then 
        . /media/sysdata/in4/platform/in4_core/internals/helpers/context_naming.sh
fi

### Xenstore
in4func_execute_as_is xenstore-exists /local/domain/$DOMID/in4/net/0
if [[ `echo $?` == 0 ]]; then 
    IP=`xenstore-read /local/domain/$DOMID/in4/net/0/ip`
    NETMASK=`xenstore-read /local/domain/$DOMID/in4/net/0/netmask`
    MTU=`xenstore-read /local/domain/$DOMID/in4/net/0/mtu`
    GATE=`xenstore-read /local/domain/$DOMID/in4/net/0/gate`

    #net
    in4func_execute_as_is cp /media/sysdata/in4/platform/v4/internals--c/linux_sys--o/network--f/suse-network--g/ifcfg-tmpl /etc/sysconfig/network/ifcfg-eth0
    sed -i "s/{IP}/$IP/"  /etc/sysconfig/network/ifcfg-eth0
    sed -i "s/{NETMASK}/$NETMASK/"  /etc/sysconfig/network/ifcfg-eth0
    sed -i "s/{MTU}/$MTU/"  /etc/sysconfig/network/ifcfg-eth0	

    #gate
    echo "default $GATE - -" > /etc/sysconfig/network/routes

    #dns
    echo "search s.pool" > /etc/resolv.conf
    echo "nameserver $GATE" >> /etc/resolv.conf

    echo "PROXY_ENABLED=\"yes\"" > /etc/sysconfig/proxy
    echo "HTTP_PROXY=\"http://x:55555\"" >> /etc/sysconfig/proxy
    echo "HTTPS_PROXY=\"http://x:55555\"" >> /etc/sysconfig/proxy                                                                                                                                                                                                                                                                                                                                       
    echo "FTP_PROXY=\"http://x:55555\"" >> /etc/sysconfig/proxy                                                                                                                                                                                                                                                                                                                                                                                                         
    echo "NO_PROXY=\"localhost, 127.0.0.1, .pool\"" >> /etc/sysconfig/proxy                              

fi
###

### IN4 BASH FOOTER ###
in4func_logit END ${BASH_SOURCE[0]}
###
