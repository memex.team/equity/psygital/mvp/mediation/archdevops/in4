########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash DSL, profiles
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

export FQDN=`hostname -f`
export $(grep -v '^#' /etc/in4-release | xargs)
export MACHINE_ID=`cat /etc/machine-id`

in4_pre ()
{
    env|grep "In4" > /tmp/in4_env; echo "## In4 GIT path - $In4GitPath ###"; git -C $In4GitPath status;  while read -r line; do a=`echo $line|cut -d '=' -f 1`; unset $a ; done < /tmp/in4_env;
    export $(grep -v '^#' /tmp/in4_env | xargs)
}

alias in4='in4_pre && sh $In4GitPath/in4.sh'
alias in4_d='in4_pre   && export In4Debug=Yes && sh $In4GitPath/in4.sh'
alias in4_dd='in4_pre  && export In4Debug=Yes && export In4ShellDebug=Yes && sh -x $In4GitPath/in4.sh'
alias in4_td='in4_pre  && export In4Debug=Yes && export In4Test=Yes && sh $In4GitPath/in4.sh'
alias in4_tdd='in4_pre && export In4Debug=Yes && export In4ShellDebug=Yes && export In4Test=Yes && sh -x $In4GitPath/in4.sh'
alias in4_sync='git -C $In4GitPath pull; git -C $In4GitPath/../context pull'
alias in4_diff='git -C $In4GitPath diff; git -C $In4GitPath/../context diff'

alias in4_deploy='in4_pre  && sh $In4GitPath/in4_core/internals/helpers/shortcuts_deploy.sh'
alias in4_deploy_d='in4_pre  && export In4Debug=Yes && sh $In4GitPath/in4_core/internals/helpers/shortcuts_deploy.sh'
alias in4_deploy_dd='in4_pre  && export In4Debug=Yes && export In4ShellDebug=Yes && sh -x $In4GitPath/in4_core/internals/helpers/shortcuts_deploy.sh'


alias syslog='cd /media/sysdata/logs/syslog/`hostname`/`date +%Y.%m.%d`'

#GIT
alias gita="git add *"
alias gits="git status"
alias gitc="git add *; git commit -m $arg"
alias gitca="git commit --amend"
