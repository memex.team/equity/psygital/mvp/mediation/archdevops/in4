resource storage1 {

    net {
          shared-secret "app-askldzxczxkdfh1";  
    }

    volume 1 {
        device minor 1;
        disk /dev/disk/by-label/storage1;
        meta-disk /dev/sdg;
    }
    
    on node1 {
        node-id   0;
        address 10.100.101.135:7703;
    }

    on node2 {
        node-id   1;
        address 10.100.101.136:7703;
    }

    on node3 {
        node-id   2;
        address 10.100.101.137:7703;
    }

     connection {
          host node1 port 7703;
          host node2 port 7703;
      }

      connection {
          host node1 port 7703;
          host node3 port 7703;
      }

      connection {
         host node2 port 7703;
          host node3 port 7703;
       }



 }

