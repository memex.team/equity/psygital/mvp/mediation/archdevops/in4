#!/bin/bash 

## EXIM ##
rm -f /etc/systemd/system/in4_exim-ha.service 			&& cp /media/sysdata/in4/techpool/ontology/mail/exim/mta-router/_systemd/in4_exim-ha.service 	/etc/systemd/system/ 
rm -f /etc/sysconfig/SuSEfirewall2.d/services/in4_exim && ln -s /media/sysdata/in4/techpool/ontology/mail/exim/mta-router/_firewall/in4_exim /etc/sysconfig/SuSEfirewall2.d/services/
systemctl daemon-reload
systemctl enable in4_exim-ha.service && systemctl restart in4_exim-ha.service 
systemctl status in4_exim-ha.service 
##
