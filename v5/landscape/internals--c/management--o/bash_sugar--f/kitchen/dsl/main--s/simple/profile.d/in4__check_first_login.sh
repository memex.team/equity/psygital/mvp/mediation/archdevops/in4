#!/bin/bash
# only do this for interactive shells
if [ "$-" != "${-#*i}" ]; then
    if [ -f "$HOME/.first_login" ]; then
        echo "Welcome, this is your first login"
        . "$HOME/.first_login"
        rm "$HOME/.first_login"
    fi
fi
