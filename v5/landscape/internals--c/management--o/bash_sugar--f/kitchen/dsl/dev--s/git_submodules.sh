mkdir chef_tmp
mkdir etc_cmdb; cd etc_cmdb

git init ##will be git clone
mkdir modules; cd modules
git submodule add /prototype/1.developer/modules/linux_sys/zypp
git submodule add /prototype/1.developer/modules/management/rev4

cd ..
mkdir data; cd data
mkdir generated
git submodule add --force /prototype/2.support/clients/comp/ci
git submodule add --force /prototype/2.support/clients/comp/conf

chef-client --no-fork -c /_dev/chef/client_dev.rb -z -j /deploy_example/etc_cmdb/data/ci/chef-run/evis.pool.json 
