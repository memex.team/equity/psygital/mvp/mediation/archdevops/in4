########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

Summary:        Duc, a library and suite of tools for inspecting disk usage
License:        MIT
Group:          System/Management
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  pango-devel cairo-devel libtokyocabinet-devel ncurses-devel

%description
Duc is a small library and a collection of tools for inspecting and visualizing
disk usage.
Duc maintains a database of accumulated sizes of directories of your file system,
and allows you to query this database with some tools,
or create fancy graphs showing you where your bytes are.

%package          devel
Summary:          Development tools for %{name}
Group:            Development/Libraries
Requires:         %{name} = %{version}-%{release}

%description devel
Duc is a small library and a collection of tools for inspecting and visualizing
disk usage.
This is devel version.


%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

libtoolize
autoreconf --install
%configure --disable-static --with-pic
make %{?_smp_mflags}

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

%makeinstall

%post
/sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files 
%defattr(-,root,root)
%{_bindir}/%{name}
%{_mandir}/man1/duc.1.gz

%changelog
* Wed Feb 07 2018 Eugene Istomin 
- Initial
