#!/usr/bin/env ruby
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = ruby
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

require_relative 'lib'
####
run = {}
run[:method] = :threads
run[:debug] = false
run[:syslog] = Syslog::Logger.new 'in4__observer'
####

def monitoring(run)
  services = in4_services
#   services = ['sshd', 'sshd', 'sshd', 'sshd', 'sshd', 'sshd', 'sshd', 'sshd'] #if run[:debug] == true  
  require 'concurrent' if run[:method] == :promises
  engine(run, services)
end

monitoring(run)
