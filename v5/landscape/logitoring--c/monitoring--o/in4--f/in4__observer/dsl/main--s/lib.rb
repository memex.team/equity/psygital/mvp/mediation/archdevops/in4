#!/usr/bin/env ruby
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = ruby
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

require 'dbus'
require 'json'
require 'syslog/logger'
####

class Pool
  def initialize(size)
    @size = size
    @jobs = Queue.new
    @pool = Array.new(@size) do |i|
      Thread.new do
      Thread.current.abort_on_exception = true              
        Thread.current[:id] = i
        catch(:exit) do
          loop do
            job, args = @jobs.pop
            job.call(*args)
          end
        end
      end
    end
  end

  def schedule(*args, &block)
    @jobs << [block, args]
  end

  def shutdown
    @size.times do
      schedule { throw :exit }
    end

    @pool.map(&:join)
  end
end 


##systemd root services
def in4_services
  services = []
  dotHex = '.'.unpack('H*')[0]
  undScHex = '_'.unpack('H*')[0]
  sys_bus = DBus.system_bus
  service = sys_bus.service('org.freedesktop.systemd1')
  systemdRoot = service.object("/org/freedesktop/systemd1")
  manager = systemdRoot["org.freedesktop.systemd1.Manager"]

  servicesFileNames = manager.ListUnitFilesByPatterns(['enabled'], ['*in4__*'])[0]
  servicesFileNames.each do |service|
    services << service[0].split('/').last.split('.')[0][5..-1]
  end
  services
end
  

def engine(run, services)
  threads = []
  concurrent = {}  
  pool = Pool.new(3)
  idleSec = idleSec(run)
  
  services.each do |serviceS|
    case run[:method]
    when :threads
      threads << monitoringLoopThreaded(pool, serviceS.dup, {:view => 'metrics'}, {'Service' => getMetrics(:countable)}, idleSec[:metrics], run[:syslog])
      threads << monitoringLoopThreaded(pool, serviceS.dup, {:view => 'status'},  {'Service' => getMetrics(:serviceStatus), 'Unit' => getMetrics(:unitStatus)}, idleSec[:status], run[:syslog])
    when :promises
      concurrent[serviceS] = Concurrent::Promises.zip(
        Concurrent::Promises.future(executor: threadPool) {  monitoringLoop(serviceS, {:view => 'metrics'}, {'Service' => getMetrics(:countable)}, idleSec[:metrics], run[:syslog]) },  
        Concurrent::Promises.future(executor: threadPool) { monitoringLoop(serviceS, {:view => 'status'},  {'Service' => getMetrics(:serviceStatus), 'Unit' => getMetrics(:unitStatus)}, idleSec[:status], run[:syslog]) }
      )  
    end
  end

  case run[:method]
  when :threads
    threads.each(&:join) 
  when :promises
    concurrent.each do |service, prom|
      prom.then { |*values| puts values.inspect }.value!
    end
  end  
end

def dBusRunner(interface, interfacePostfix, metrics, lookup)
    run = interface.GetAll("org.freedesktop.systemd1.#{interfacePostfix}")[0]
    metrics.map{ |metric| run[metric]  == 18446744073709551615 ? value = 0 : value = run[metric]; lookup[metric.to_sym] = value }    
    lookup
end

def monitoringLoopThreaded(pool, serviceS, props, metricsHash, idleSec, syslog)
  Thread.new(size: 1) do
    Thread.current.abort_on_exception = true      
    monitoringLoop(pool, serviceS, props, metricsHash, idleSec, syslog)
  end
end

def monitoringLoop(pool, serviceS, props, metricsHash, idleSec, syslog)
  dotHex = '.'.unpack('H*')[0]
  undScHex = '_'.unpack('H*')[0]

  sys_bus = DBus::ASystemBus.new
  service = sys_bus.service('org.freedesktop.systemd1')
  unit = service.object("/org/freedesktop/systemd1/unit/in4_#{undScHex}_#{undScHex}#{serviceS}_#{dotHex}service")
  unit.default_iface = "org.freedesktop.DBus.Properties"

  head = {}
  head[:msg_class] = 'logitoring'
  head[:msg_view]  = props[:view]
  head[:type] = 'systemd'
  head[:service] = serviceS
  
  loop do
    sleep(idleSec)
    lookup = {}
    pool.schedule do    
      metricsHash.each do |interfacePostfix, metrics|
        run = dBusRunner(unit, interfacePostfix, metrics, lookup)
      end

      lookup.merge!(head)
      sendES(lookup.to_json, syslog)      
    end
  end
end

def sendES(lookup, syslog)
  syslog.info lookup
end


def idleSec(run)
  idleSec = {}
  if run[:debug] == false
    idleSec[:metrics] = 10
    idleSec[:status]  = 20
  else
    idleSec[:metrics] = 0.2
    idleSec[:status]  = 0.5
  end  
  idleSec
end

def  getMetrics(type) 
  case type
  when :countable
    m = ['MainPID', 'SyslogIdentifier', 'MemoryCurrent', 'CPUUsageNSec', 'TasksCurrent','IPIngressPackets', 'IPEgressPackets', 'IPIngressBytes', 'IPEgressBytes', 'IOEgressBytes', 'IOIngressBytes']
  when :serviceStatus
    m = ['MainPID', 'SyslogIdentifier', 'Result', 'NRestarts', 'ExecMainStatus', 'Nice']
  when :unitStatus
    m = ['ActiveState', 'SubState', 'UnitFileState']
  end
  m
end
#    GC.start
#    p GC.stat
