Server=core
ServerPort=10051
#Hostname=...
#HostnameItem=system.hostname

ListenPort=10053
# SourceIP=
LogType=system
DebugLevel=3


# EnableRemoteCommands=0
LogRemoteCommands=1
PidFile=/run/zabbixs/zabbix-proxy.pid
DBName=/tmp/zabbix_proxy.sqlite


######### PROXY SPECIFIC PARAMETERS #############


ProxyOfflineBuffer=4
# HeartbeatFrequency=60
# ConfigFrequency=600
# DataSenderFrequency=1

############ ADVANCED PARAMETERS ################
StartPollers=5

StartIPMIPollers=1

StartPollersUnreachable=2
StartTrappers=10
StartPingers=2
StartDiscoverers=2

StartHTTPPollers=2
# StartTimers=1


### Option: HousekeepingFrequency
HousekeepingFrequency=24

### Option: MaxHousekeeperDelete
# MaxHousekeeperDelete=5000


### Option: CacheSize
CacheSize=40M
Timeout=10


# TrapperTimeout=300
# UnreachablePeriod=45
# UnavailableDelay=60
# UnreachableDelay=15

# AlertScriptsPath=${datadir}/zabbix/alertscripts
# ExternalScripts=${datadir}/zabbix/externalscripts
# FpingLocation=/usr/sbin/fping


LogSlowQueries=5000


### Option: StartProxyPollers

# StartProxyPollers=1
# ProxyConfigFrequency=3600
# ProxyDataFrequency=1

AllowRoot=0
User=zabbix


# Include=/usr/local/etc/zabbix_server.general.conf
# Include=/usr/local/etc/zabbix-server.conf.d/
# Include=/usr/local/etc/zabbix-server.conf.d/*.conf



####### LOADABLE MODULES #######

### Option: LoadModulePath

# LoadModulePath=${libdir}/modules
# LoadModule=


####### TLS-RELATED PARAMETERS #######

### Option: TLSConnect
#       How the proxy should connect to Zabbix server. Used for an active proxy, ignored on a passive proxy.
#       Only one value can be specified:
#               unencrypted - connect without encryption
#               psk         - connect using TLS and a pre-shared key
#               cert        - connect using TLS and a certificate
#
# Mandatory: yes, if TLS certificate or PSK parameters are defined (even for 'unencrypted' connection)
# Default:
# TLSConnect=unencrypted

### Option: TLSAccept
#       What incoming connections to accept from Zabbix server. Used for a passive proxy, ignored on an active proxy.
#       Multiple values can be specified, separated by comma:
#               unencrypted - accept connections without encryption
#               psk         - accept connections secured with TLS and a pre-shared key
#               cert        - accept connections secured with TLS and a certificate
#
# Mandatory: yes, if TLS certificate or PSK parameters are defined (even for 'unencrypted' connection)
# Default:
# TLSAccept=unencrypted

### Option: TLSCAFile
#       Full pathname of a file containing the top-level CA(s) certificates for
#       peer certificate verification.
#
# Mandatory: no
# Default:
# TLSCAFile=

### Option: TLSCRLFile
#       Full pathname of a file containing revoked certificates.
#
# Mandatory: no
# Default:
# TLSCRLFile=

### Option: TLSServerCertIssuer
#      Allowed server certificate issuer.
#
# Mandatory: no
# Default:
# TLSServerCertIssuer=

### Option: TLSServerCertSubject
#      Allowed server certificate subject.
#
# Mandatory: no
# Default:
# TLSServerCertSubject=

### Option: TLSCertFile
#       Full pathname of a file containing the proxy certificate or certificate chain.
#
# Mandatory: no
# Default:
# TLSCertFile=

### Option: TLSKeyFile
#       Full pathname of a file containing the proxy private key.
#
# Mandatory: no
# Default:
# TLSKeyFile=

### Option: TLSPSKIdentity
#       Unique, case sensitive string used to identify the pre-shared key.
#
# Mandatory: no
# Default:
# TLSPSKIdentity=

### Option: TLSPSKFile
#       Full pathname of a file containing the pre-shared key.
#
# Mandatory: no
# Default:
# TLSPSKFile=
