#!/bin/bash 
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

in4func_run "logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s" "2_init/opensuse" "main--s.sh"

! systemctl disable syslog
! systemctl disable rsyslog
echo "disabled" > /usr/lib/systemd/system/rsyslog.service
chmod 000 /usr/lib/systemd/system/rsyslog.service
! rm -rf /etc/rsyslog.d/

in4func_systemd "logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s" "add" "service" "in4__rsyslog"
in4func_systemd "logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s" "enable" "service" "in4__rsyslog"

in4func_ln "dsl::logitoring--c--messagebus--o--syslog--f--rsyslog--g--main--s" "engine" "/etc/rsyslog.d"

