########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

%define library_name librelp0

BuildRoot:      %{_tmppath}/%{name}-%{version}-build
Url:            http://www.librelp.com/
Summary:        A reliable logging library
License:        GPL-3.0+
Group:          Development/Libraries/C and C++
BuildRequires:  pkgconfig
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  pkgconfig(gnutls) >= 2.0.0
BuildRequires:  pkgconfig(openssl)

%description
librelp is an easy to use library for the RELP protocol. RELP in turn provides
reliable event logging over the network (and consequently RELP stands for
Reliable Event Logging Protocol). RELP was initiated by Rainer Gerhards after
he was finally upset by the lossy nature of plain tcp syslog and wanted a cure
for all these dangling issues.

RELP (and hence) librelp assures that no message is lost, not even when
connections break and a peer becomes unavailable. The current version of RELP
has a minimal window of opportunity for message duplication after a session has
been broken due to network problems. In this case, a few messages may be
duplicated (a problem that also exists with plain tcp syslog). Future versions
of RELP will address this shortcoming. 

Please note that RELP is a general-purpose, extensible logging protocol. Even
though it was designed to solve the urgent need of rsyslog-to-rsyslog
communication, RELP supports many more applications. Extensible command verbs
provide ample opportunity to extend the protocol without affecting existing
applications.


Authors:
--------
    Rainer Gerhards <rgerhards@adiscon.com>


%package -n %library_name
Summary:        A reliable logging library
Group:          Development/Libraries/C and C++

%description -n %library_name
librelp is an easy to use library for the RELP protocol. RELP in turn provides
reliable event logging over the network (and consequently RELP stands for
Reliable Event Logging Protocol). RELP was initiated by Rainer Gerhards after
he was finally upset by the lossy nature of plain tcp syslog and wanted a cure
for all these dangling issues.

RELP (and hence) librelp assures that no message is lost, not even when
connections break and a peer becomes unavailable. The current version of RELP
has a minimal window of opportunity for message duplication after a session has
been broken due to network problems. In this case, a few messages may be
duplicated (a problem that also exists with plain tcp syslog). Future versions
of RELP will address this shortcoming. 

Please note that RELP is a general-purpose, extensible logging protocol. Even
though it was designed to solve the urgent need of rsyslog-to-rsyslog
communication, RELP supports many more applications. Extensible command verbs
provide ample opportunity to extend the protocol without affecting existing
applications.


Authors:
--------
    Rainer Gerhards <rgerhards@adiscon.com>

%package devel
Requires:       %{library_name} = %{version}
Summary:        A reliable logging library
Group:          Development/Libraries/C and C++
Requires:       libgnutls-devel >= 2.0.0
Requires:       pkgconfig(openssl)

%description devel
librelp is an easy to use library for the RELP protocol. RELP in turn provides
reliable event logging over the network (and consequently RELP stands for
Reliable Event Logging Protocol). RELP was initiated by Rainer Gerhards after
he was finally upset by the lossy nature of plain tcp syslog and wanted a cure
for all these dangling issues.

RELP (and hence) librelp assures that no message is lost, not even when
connections break and a peer becomes unavailable. The current version of RELP
has a minimal window of opportunity for message duplication after a session has
been broken due to network problems. In this case, a few messages may be
duplicated (a problem that also exists with plain tcp syslog). Future versions
of RELP will address this shortcoming. 

Please note that RELP is a general-purpose, extensible logging protocol. Even
though it was designed to solve the urgent need of rsyslog-to-rsyslog
communication, RELP supports many more applications. Extensible command verbs
provide ample opportunity to extend the protocol without affecting existing
applications.


Authors:
--------
    Rainer Gerhards <rgerhards@adiscon.com>

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

libtoolize
autoreconf --install
%configure   \
  --disable-static \
  --with-pic \
  --enable-tls=yes
make %{?_smp_mflags} V=1

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

%makeinstall
%{__rm} %{buildroot}%{_libdir}/librelp.la

%post   -n %{library_name} -p /sbin/ldconfig

%postun -n %{library_name} -p /sbin/ldconfig

%files -n librelp0
%defattr(-,root,root,-)
%{_libdir}/librelp.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/librelp.h
%{_libdir}/librelp.so
%{_libdir}/pkgconfig/relp.pc

%changelog
