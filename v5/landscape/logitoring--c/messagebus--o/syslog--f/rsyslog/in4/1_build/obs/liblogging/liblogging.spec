########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

%define with_systemd_journal 0%{?suse_version} >= 1310
%define with_rst2man 0%{?suse_version} >= 1230
%define sover 0
Summary:        An easy to use logging library
License:        BSD-2-Clause
Group:          Development/Libraries/C and C++
BuildRequires:  dos2unix
BuildRequires:  pkgconfig
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%if 0%{?suse_version} > 1110
BuildRequires:  pkgconfig >= 0.9.0
%else
BuildRequires:  pkgconfig >= 0.9.0
%endif
%if %{with_systemd_journal}
%if 0%{?suse_version} == 1310
BuildRequires:  pkgconfig(libsystemd-journal) >= 197
%else
BuildRequires:  pkgconfig(libsystemd) >= 209
%endif
%endif

%description
Liblogging is an easy to use logging library.

It contains the Libstdlog component is used for standard logging
(syslog replacement) purposes via multiple channels.

%package -n %{name}%{sover}
Summary:        An easy to use logging library
Group:          System/Libraries

%description -n %{name}%{sover}
Liblogging is an easy to use logging library.

It contains the Libstdlog component is used for standard logging
(syslog replacement) purposes via multiple channels.

%package devel
Summary:        Development files for LibLogging stdlog library
Group:          Development/Libraries/C and C++
Requires:       %{name}%{sover} = %{version}-%{release}

%description devel
The liblogging-devel package includes header files, libraries necessary for
developing programs which use liblogging library.

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

libtoolize
autoreconf --install
%configure \
	--disable-static \
	--enable-rfc3195 \
%if %{with_systemd_journal}
	--enable-journal \
%else
	--disable-journal \
%endif
	--disable-cached-man-pages \
  --disable-man-pages \

make %{?_smp_mflags:%{?_smp_mflags}} V=1
dos2unix AUTHORS ChangeLog COPYING README

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 /

%make_install
find %{buildroot} -type f -name "*.la" -delete -print
rm -v rfc3195/doc/html/Makefile*

%post -n %{name}%{sover} -p /sbin/ldconfig
%postun -n %{name}%{sover} -p /sbin/ldconfig

%files -n %{name}%{sover}
%defattr(-,root,root)
%{_libdir}/liblogging-rfc3195.so.*
%{_libdir}/liblogging-stdlog.so.*
%{_bindir}/stdlogctl
# %{_mandir}/man1/stdlogctl*

%files devel
%defattr(-,root,root)
%{_libdir}/liblogging-rfc3195.so
%{_libdir}/liblogging-stdlog.so
%dir %{_includedir}/%{name}
%{_includedir}/liblogging/*.h
%{_libdir}/pkgconfig/liblogging-rfc3195.pc
%{_libdir}/pkgconfig/liblogging-stdlog.pc
# %{_mandir}/man3/stdlog*

%changelog
