########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    %(echo %{_obs_helper}|sed -e "s/-__.*//")
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

Summary:        The enhanced syslogd for Linux and Unix
License:        (GPL-3.0+ and Apache-2.0)
Group:          System/Daemons
%bcond_without  systemd
%bcond_without  udpspoof
%bcond_without  dbi
%bcond_without  journal
%bcond_without  gnutls
%bcond_without  gcrypt
%bcond_without  guardtime
%bcond_without  mysql
%bcond_without  pgsql
%bcond_without  relp
%bcond_without  rfc3195
%bcond_without  snmp
%bcond_without  diagtools
%bcond_without  mmnormalize
%bcond_without  elasticsearch
%bcond_without  omhttpfs    
%bcond_without  omamqp1
%bcond_without  tcl
# TODO: ... doesnt have a proper configure check but wants hdfs.h
%bcond_with     hdfs
%bcond_with     mongodb
%bcond_with     hiredis
%bcond_with     zeromq
%bcond_with     maxminddb
%bcond_with     gssapi
%define         upstream_version            %{version}
%define         rsyslogdocdir               %{_docdir}/%{name}
%define         rsyslog_rundir              /run/rsyslog
%define         rsyslog_sockets_cfg         %{rsyslog_rundir}/additional-log-sockets.conf
%define         rsyslog_module_dir_nodeps   %{_libdir}/rsyslog/
%define         rsyslog_module_dir_withdeps %{_libdir}/rsyslog/
Url:            http://www.rsyslog.com/
%if %{with systemd}
Provides:       syslog
Provides:       sysvinit(syslog)
Conflicts:      otherproviders(syslog)
Requires(pre):  %fillup_prereq
%if %{with systemv}
Requires(pre):  %insserv_prereq
Requires(pre):  syslog-service < 2.0
Requires(pre):  /etc/init.d/syslog
%else
Requires(pre):  syslog-service >= 2.0
%endif
%{?systemd_requires}
BuildRequires:  pkgconfig(systemd) >= 209
%if %{with journal}
BuildRequires:  pkgconfig(libsystemd) >= 234
%endif
%else
Requires(pre):  %insserv_prereq %fillup_prereq /etc/init.d/syslog
BuildRequires:  klogd
%endif

BuildRequires:  autoconf >= 2.61
BuildRequires:  automake
BuildRequires:  libtool
#
BuildRequires:  bison
BuildRequires:  flex
BuildRequires:  openssl-devel >= 0.9.7
BuildRequires:  pcre-devel
BuildRequires:  pkgconfig
BuildRequires:  zlib-devel
%if %{with rfc3195}
BuildRequires:  pkgconfig(liblogging-rfc3195) >= 1.0.1
%endif
BuildRequires:  pkgconfig(liblogging-stdlog) >= 1.0.1

%if %{with elasticsearch}
BuildRequires:  curl-devel
%endif

%if %{with omhttpfs}
BuildRequires:  curl-devel >= 7.0.0
%endif
%if %{with omamqp1}
%if %{with pkgconfig}
BuildRequires:  pkgconfig(libqpid-proton) >= 0.9
%else
BuildRequires:  qpid-proton-devel >= 0.9
%endif
%endif
%if %{with hiredis}
BuildRequires:  hiredis-devel >= 0.10.1
%endif
%if %{with mongodb}
# TODO: PKG_CHECK_MODULES(LIBMONGO_CLIENT, libmongo-client >= 0.1.4)
%endif
%if %{with zeromq}
BuildRequires:  czmq-devel >= 1.1.0
%endif
%if %{with gssapi}
BuildRequires:  krb5-devel
%endif
%if %{with gnutls}
BuildRequires:  libgnutls-devel
%endif
%if %{with gcrypt}
BuildRequires:  libgcrypt-devel
%endif
%if %{with dbi}
BuildRequires:  libdbi-devel
%endif
%if %{with mysql}
BuildRequires:  mysql-devel
%endif
%if %{with snmp}
BuildRequires:  net-snmp-devel
%endif
%if %{with pgsql}
BuildRequires:  postgresql-devel
%endif
%if %{with relp}
# RELP support
BuildRequires:  pkgconfig(relp) >= 1.2.14
%endif
%if %{with udpspoof}
# UDP spoof support
BuildRequires:  libnet-devel
%endif
%if %{with mmnormalize}
# mmnormalize support
BuildRequires:  pkgconfig(libee) >= 0.4.0
BuildRequires:  pkgconfig(lognorm) >= 2.0.3
%endif
#
%if %{with maxminddb}
BuildRequires:  pkgconfig(libmaxminddb)
%endif
BuildRequires:  pkgconfig(libee) >= 0.4.0
BuildRequires:  pkgconfig(libestr) >= 0.1.9
BuildRequires:  pkgconfig(uuid) >= 2.21.0
BuildRequires:  pkgconfig(libfastjson) >= 0.99.3

%if 0%{?suse_version} >= 1200
BuildRequires:  python-docutils
%bcond_without  rst2man
%else
%bcond_with     rst2man
%endif
%if %{with tcl}
%if %{with pkgconfig}
BuildRequires:  pkgconfig(tcl)
%else
BuildRequires:  tcl-devel
%endif
%endif
%if %{with systemd}
%{?systemd_requires}
BuildRequires:  pkgconfig(systemd)
%endif


%description
Rsyslog is an enhanced multi-threaded syslogd supporting, among others,
MySQL, syslog/tcp, RFC 3195, permitted sender lists, filtering on any
message part, and fine grain output format control. It is quite
compatible to stock sysklogd and can be used as a drop-in replacement.
Its advanced features make it suitable for enterprise-class, encryption
protected syslog relay chains while at the same time being very easy to
setup for the novice user.

%if %{with diagtools}

%package diag-tools
Requires:       %{name} = %{version}
Summary:        Diagnostic tools
Group:          System/Daemons

%description diag-tools
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This package provides additional diagnostic tools (small helpers,
usually not needed).

%endif

%if %{with gssapi}

%package module-gssapi
Requires:       %{name} = %{version}
Summary:        GSS-API support module for rsyslog
Group:          System/Daemons

%description module-gssapi
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides the support to receive syslog messages from the
network protected via Kerberos 5 encryption and authentication.

%endif

%if %{with mysql}

%package module-mysql
Requires:       %{name} = %{version}
Summary:        MySQL support module for rsyslog
Group:          System/Daemons

%description module-mysql
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This package provides a module with the support for logging into MySQL
databases.

%endif

%if %{with pgsql}

%package module-pgsql
Requires:       %{name} = %{version}
Summary:        PostgreSQL support module for rsyslog
Group:          System/Daemons

%description module-pgsql
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides the support for logging into PostgreSQL databases.

%endif

%if %{with dbi}

%package module-dbi
Requires:       %{name} = %{version}
Summary:        Database support via DBI
Group:          System/Daemons

%description module-dbi
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This package provides a module with the support for logging into DBI
supported databases.

%endif

%if %{with snmp}

%package module-snmp
Requires:       %{name} = %{version}
Summary:        SNMP support module for rsyslog
Group:          System/Daemons

%description module-snmp
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides the ability to send syslog messages as an SNMPv1 &
v2c traps.

%endif

%if %{with gnutls}

%package module-gtls
Requires:       %{name} = %{version}
Summary:        TLS encryption support module for rsyslog
Group:          System/Daemons

%description module-gtls
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides the ability for TLS encrypted TCP logging (based
on current syslog-transport-tls internet drafts).
%endif

%if %{with gcrypt}

%package module-gcrypt
Requires:       %{name} = %{version}
Summary:        Libgcrypt log file encryption support module for rsyslog
Group:          System/Daemons

%description module-gcrypt
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides log file encryption support using libgcrypt and
a rsgtutil utility to manage the files.
%endif

%if %{with relp}

%package module-relp
Requires:       %{name} = %{version}
Summary:        RELP protocol support module for syslog
Group:          System/Daemons

%description module-relp
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides Reliable Event Logging Protocol support.

%endif

%if %{with mmnormalize}

%package module-mmnormalize
Requires:       %{name} = %{version}
Summary:        Contains the mmnormalize support module for syslog
Group:          System/Daemons

%description module-mmnormalize
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides log normalizing support.

%endif

%if %{with udpspoof}

%package module-udpspoof
Requires:       %{name} = %{version}
Summary:        UDP spoof support module for syslog
Group:          System/Daemons

%description module-udpspoof
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides a UDP forwarder that allows changing the sender address.

%endif

%if %{with elasticsearch}

%package module-elasticsearch
Requires:       %{name} = %{version}
Summary:        ElasticSearch output module for syslog
Group:          System/Daemons

%description module-elasticsearch
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides support to output to an ElasticSearch database.

%endif

%if %{with omhttpfs}

%package module-omhttpfs
Requires:       %{name} = %{version}
Summary:        HDFS via HTTP output module for syslog
Group:          System/Daemons

%description module-omhttpfs
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides support to output to HDFS via HTTP.

%endif

%if %{with hdfs}

%package module-hdfs
Requires:       %{name} = %{version}
Summary:        HDFS output module for syslog
Group:          System/Daemons

%description module-hdfs
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides support to output to an HDFS database.

%endif

%if %{with mongodb}

%package module-mongodb
Requires:       %{name} = %{version}
Summary:        MongoDB output module for syslog
Group:          System/Daemons

%description module-mongodb
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides support to output to a MongoDB database.

%endif

%if %{with hiredis}

%package module-hiredis
Requires:       %{name} = %{version}
Summary:        Redis output module for syslog
Group:          System/Daemons

%description module-hiredis
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides support to output to a Redis database.

%endif

%if %{with zeromq}
%package module-zeromq
Requires:       %{name} = %{version}
Summary:        ZeroMQ support module for syslog
Group:          System/Daemons

%description module-zeromq
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides support for ZeroMQ.
%endif

%if %{with omamqp1}
%package module-omamqp1
Requires:       %{name} = %{version}
Summary:        AMQP support module for syslog
Group:          System/Daemons

%description module-omamqp1
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides support for AMQP.
%endif

%if %{with tcl}
%package module-omtcl
Requires:       %{name} = %{version}
Summary:        TCL output module for rsyslog
Group:          System/Daemons

%description module-omtcl
Rsyslog is an enhanced multi-threaded syslog daemon. See rsyslog
package.

This module provides an output module for TCL.
%endif

%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

#%patch0 -p1
#
#dos2unix doc/*.html

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing -W -Wall -I../grammar -I../../grammar"
autoreconf -fiv

%configure			\
	--with-moddirs=%{rsyslog_module_dir_withdeps} \
%if ! %{with rst2man}
	--enable-cached-man-pages	\
%endif
	--enable-option-checking	\
	--enable-largefile	\
	--enable-regexp		\
	--enable-zlib		\
	--enable-klog		\
	--enable-kmsg		\
	--enable-inet		\
	--enable-unlimited-select	\
	--enable-rsyslogd	\
%if %{with elasticsearch}
	--enable-elasticsearch	\
%endif
%if %{with omhttpfs}
	--enable-omhttpfs	\
%endif
%if %{with gnutls}
	--enable-gnutls		\
%endif
%if %{with gssapi}
	--enable-gssapi-krb5	\
%endif
%if %{with dbi}
	--enable-libdbi		\
%endif
%if %{with mysql}
	--enable-mysql		\
%endif
%if %{with pgsql}
	--enable-pgsql		\
%endif
%if %{with relp}
	--enable-relp		\
%endif
%if %{with rfc3195}
        --enable-rfc3195        \
%endif
%if %{with snmp}
	--enable-snmp		\
	--enable-mmsnmptrapd	\
%endif
	--enable-mail		\
	--enable-imfile		\
	--enable-imptcp		\
	--enable-impstats	\
	--enable-omprog		\
	--enable-omuxsock	\
%if %{with udpspoof}
	--enable-omudpspoof	\
%endif
	--enable-omstdout	\
	--enable-pmlastmsg	\
	--enable-pmcisconames	\
	--enable-pmaixforwardedfrom	\
	--enable-pmsnare	\
	--enable-pmnull		\
	--enable-pmnormalize \
	--enable-omruleset	\
%if %{with mmnormalize}
	--enable-mmnormalize \
	--enable-mmjsonparse	\
	--enable-mmaudit	\
%endif
%if %{with hdfs}
	--enable-omhdfs		\
%endif
%if %{with mongodb}
	--enable-ommongodb	\
%endif
%if %{with omamqp1}
	--enable-omamqp1	\
%endif
%if %{with hiredis}
	--enable-omhiredis	\
%endif
%if %{with zeromq}
	--enable-imzmq3		\
	--enable-omzmq3		\
%endif
%if %{with diagtools}
	--enable-imdiag		\
	--enable-diagtools	\
%endif
%if %{with systemd} && %{with journal}
	--enable-imjournal	\
	--enable-omjournal	\
%endif
	--enable-mmanon		\
	--enable-mmaudit	\
	--enable-mmjsonparse	\
	--enable-mmutf8fix	\
	--enable-mmcount	\
	--enable-mmsequence	\
	--enable-mmfields	\
	--enable-mmpstrucdata	\
	--enable-mmrfc5424addhmac \
	--enable-mmrm1stspace	\
	--enable-pmciscoios \
	--enable-pmpanngfw \
%if %{with gcrypt}
	--enable-libgcrypt \
%else
	--disable-libgcrypt \
%endif
%if %{with tcl}
    --enable-omtcl \
%endif
%if %{with maxminddb}
    --enable-mmdblookup \
%endif
	--enable-usertools \
	--disable-static

make %{?_smp_mflags:%{_smp_mflags}} V=1

%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

make install DESTDIR="%{buildroot}"  V=1
#
rm -f %{buildroot}%{rsyslog_module_dir_nodeps}/*.la
#
# move all modules linking libraries in /usr to /usr/lib[64]
# the user has to specify them with full path then...
install -d -m0755 %{buildroot}%{rsyslog_module_dir_withdeps}
if test "%{rsyslog_module_dir_nodeps}" != "%{rsyslog_module_dir_withdeps}" ; then
	for mod in  \
%if %{with gnutls}
		lmnsd_gtls.so \
%endif
%if %{with gcrypt}
		lmcry_gcry.so \
%endif
%if %{with gssapi}
		omgssapi.so imgssapi.so lmgssutil.so \
%endif
%if %{with dbi}
		omlibdbi.so \
%endif
%if %{with mysql}
		ommysql.so \
%endif
%if %{with pgsql}
		ompgsql.so \
%endif
%if %{with relp}
		imrelp.so omrelp.so \
%endif
%if %{with snmp}
		omsnmp.so \
%endif
%if %{with mmnormalize}
		mmnormalize.so  \
		mmjsonparse.so \
		mmaudit.so \
%endif
%if %{with elasticsearch}
		omelasticsearch.so \
%endif
%if %{with omhttpfs}
		omhttpfs.so \
%endif
	; do
		mv -f %{buildroot}%{rsyslog_module_dir_nodeps}/$mod \
		      %{buildroot}%{rsyslog_module_dir_withdeps}
	done
fi
if test "%{_sbindir}" != "/sbin" ; then
	install -d -m0755 %{buildroot}/sbin
	ln -sf %{_sbindir}/rsyslogd $RPM_BUILD_ROOT/sbin/rsyslogd
fi
#

install -d -m0755 %{buildroot}%{_sysconfdir}/rsyslog.d
install -d -m0755 %{buildroot}/run/rsyslog
install -d -m0755 %{buildroot}%{_localstatedir}/spool/rsyslog
install -d -m0755 %{buildroot}/var/adm/fillup-templates

#
rm -f doc/Makefile*
install -d -m0755 %{buildroot}%{rsyslogdocdir}/
find ChangeLog README AUTHORS COPYING COPYING.LESSER \
	\( -type d -exec install -m755 -d   %{buildroot}%{rsyslogdocdir}/\{\} \; \) \
     -o \( -type f -exec install -m644 \{\} %{buildroot}%{rsyslogdocdir}/\{\} \; \)
#
%if %{with mysql}
install -m644 plugins/ommysql/createDB.sql \
	%{buildroot}%{rsyslogdocdir}/mysql-createDB.sql
%endif
%if %{with pgsql}
install -m644 plugins/ompgsql/createDB.sql \
	%{buildroot}%{rsyslogdocdir}/pgsql-createDB.sql
%endif
# create ghosts
install -d -m0755 %{buildroot}%{rsyslog_rundir}
touch %{buildroot}%{rsyslog_sockets_cfg}
chmod 644 %{buildroot}%{rsyslog_sockets_cfg}

%clean
if [ -n "%{buildroot}" ] && [ "%{buildroot}" != "/" ] ; then
	rm -rf "%{buildroot}"
fi

%if %{with systemd} && ! %{with systemv}

%pre
#%{service_add_pre rsyslog.service}

%endif

%post
#
# update linker caches
#
/sbin/ldconfig
#
# remove obsolete variables
#
%{remove_and_set -n syslog SYSLOG_DAEMON SYSLOG_REQUIRES_NETWORK}
%{remove_and_set -n syslog RSYSLOGD_COMPAT_VERSION RSYSLOGD_NATIVE_VERSION}
%if %{with systemv}
%{fillup_and_insserv -ny syslog syslog}
%endif
#
# add RSYSLOGD_* variables
#
%{fillup_only -ns syslog rsyslog}
%if %{with systemv}
#
# switch SYSLOG_DAEMON to outself
#
if test -f etc/sysconfig/syslog ; then
	sed -i \
		-e 's/^SYSLOG_DAEMON=.*/SYSLOG_DAEMON="rsyslogd"/g' \
		etc/sysconfig/syslog
fi
%endif
#
# Do not use multiple facilities with the same priority pattern.
# It causes start failure since rsyslog-6.4.x (bnc#780607).
#
# FIXME: it seems to be a valid syntax -> rsyslog bug?
#
if grep -qs '^local[0246],' etc/rsyslog.conf ; then
   sed -i -e 's/^local\([0246]\),/local\1.*;/g' etc/rsyslog.conf
fi
#
# create dirs, touch log default files
#
mkdir -p var/log
touch var/log/messages;  chmod 640 var/log/messages
touch var/log/mail;      chmod 640 var/log/mail
touch var/log/mail.info; chmod 640 var/log/mail.info
touch var/log/mail.warn; chmod 640 var/log/mail.warn
touch var/log/mail.err;  chmod 640 var/log/mail.err
test -f var/log/news && mv -f var/log/news var/log/news.bak
mkdir -p -m 0750 var/log/news
chown news:news  var/log/news
touch var/log/news/news.crit;   chmod 640 var/log/news/news.crit
chown news:news var/log/news/news.crit
touch var/log/news/news.err;    chmod 640 var/log/news/news.err
chown news:news var/log/news/news.err
touch var/log/news/news.notice; chmod 640 var/log/news/news.notice
chown news:news var/log/news/news.notice
#
# touch the additional log files we are using
#
touch var/log/acpid;            chmod 640 var/log/acpid
touch var/log/firewall;         chmod 640 var/log/firewall
touch var/log/NetworkManager;   chmod 640 var/log/NetworkManager
#
# touch the additional log sockets config file
#
mkdir -p -m750 ".%{rsyslog_rundir}"
touch ".%{rsyslog_sockets_cfg}"
chmod 640 ".%{rsyslog_sockets_cfg}"
#


%preun
#
# stop the rsyslogd daemon when it is running
#
%if %{with systemd} && ! %{with systemv}
%{service_del_preun syslog.socket}
%{service_del_preun rsyslog.service}
%else
if test -x /etc/init.d/syslog ; then
	%{stop_on_removal syslog}
fi
#
# reset SYSLOG_DAEMON variable on removal
#
if test "$1" = "0" -a -f etc/sysconfig/syslog ; then
	sed -i \
		-e 's/^SYSLOG_DAEMON=.*/SYSLOG_DAEMON=""/g' \
		etc/sysconfig/syslog
fi
%endif

%postun
#
# update linker caches
#
/sbin/ldconfig
%if %{with systemd} && ! %{with systemv}
#
# cleanup init scripts
#
%{service_del_postun rsyslog.service}
%else
#
# stop the rsyslogd daemon when it is running
#
if test -x /etc/init.d/syslog ; then
	%{restart_on_update syslog}
fi
#
# cleanup init scripts
#
%{insserv_cleanup}
%endif

%files
%defattr(-,root,root)
%dir %{_sysconfdir}/rsyslog.d
%{_sbindir}/rsyslogd
%if "%{_sbindir}" != "/sbin"
/sbin/rsyslogd
%endif
%dir %{rsyslog_module_dir_nodeps}
%{rsyslog_module_dir_nodeps}/fmhash.so
%{rsyslog_module_dir_nodeps}/fmhttp.so
%{rsyslog_module_dir_nodeps}/imfile.so
%{rsyslog_module_dir_nodeps}/imklog.so
%{rsyslog_module_dir_nodeps}/imkmsg.so
%{rsyslog_module_dir_nodeps}/immark.so
%{rsyslog_module_dir_nodeps}/impstats.so
%{rsyslog_module_dir_nodeps}/imtcp.so
%{rsyslog_module_dir_nodeps}/imudp.so
%{rsyslog_module_dir_nodeps}/imuxsock.so
%{rsyslog_module_dir_nodeps}/lmnet.so
%{rsyslog_module_dir_nodeps}/lmnetstrms.so
%{rsyslog_module_dir_nodeps}/lmnsd_ptcp.so
%{rsyslog_module_dir_nodeps}/imptcp.so
%{rsyslog_module_dir_nodeps}/lmregexp.so
#%{rsyslog_module_dir_nodeps}/lmstrmsrv.so
%{rsyslog_module_dir_nodeps}/lmtcpclt.so
%{rsyslog_module_dir_nodeps}/lmtcpsrv.so
%{rsyslog_module_dir_nodeps}/lmzlibw.so
%{rsyslog_module_dir_nodeps}/mmanon.so
%{rsyslog_module_dir_nodeps}/mmcount.so
%{rsyslog_module_dir_nodeps}/mmexternal.so
%{rsyslog_module_dir_nodeps}/mmfields.so
%{rsyslog_module_dir_nodeps}/mmpstrucdata.so
%{rsyslog_module_dir_nodeps}/mmrfc5424addhmac.so
%{rsyslog_module_dir_nodeps}/mmsequence.so
%{rsyslog_module_dir_nodeps}/mmutf8fix.so
%{rsyslog_module_dir_nodeps}/mmrm1stspace.so
%{rsyslog_module_dir_nodeps}/ommail.so
%{rsyslog_module_dir_nodeps}/omprog.so
%{rsyslog_module_dir_nodeps}/omruleset.so
%{rsyslog_module_dir_nodeps}/omstdout.so
%{rsyslog_module_dir_nodeps}/omtesting.so
%{rsyslog_module_dir_nodeps}/omuxsock.so
%{rsyslog_module_dir_nodeps}/pmlastmsg.so
%{rsyslog_module_dir_nodeps}/pmaixforwardedfrom.so
%{rsyslog_module_dir_nodeps}/pmcisconames.so
%{rsyslog_module_dir_nodeps}/pmciscoios.so
%{rsyslog_module_dir_nodeps}/pmsnare.so
%{rsyslog_module_dir_nodeps}/pmnull.so
%{rsyslog_module_dir_nodeps}/pmnormalize.so
%{rsyslog_module_dir_nodeps}/pmpanngfw.so
%if %{with rfc3195}
%{rsyslog_module_dir_nodeps}/im3195.so
%endif
%if %{with systemd} && %{with journal}
%{rsyslog_module_dir_nodeps}/imjournal.so
%{rsyslog_module_dir_nodeps}/omjournal.so
%endif
%dir %{rsyslog_module_dir_withdeps}
%{_mandir}/man5/rsyslog.conf.5*
%{_mandir}/man8/rsyslogd.8*
%dir %{rsyslogdocdir}
%doc %{rsyslogdocdir}/ChangeLog
%doc %{rsyslogdocdir}/README
%doc %{rsyslogdocdir}/AUTHORS
%doc %{rsyslogdocdir}/COPYING
%doc %{rsyslogdocdir}/COPYING.LESSER
%dir %{_localstatedir}/spool/rsyslog
%attr(0755,root,root) %dir %ghost %{rsyslog_rundir}
%attr(0644,root,root) %ghost %{rsyslog_sockets_cfg}

%if %{with diagtools}

%files diag-tools
%defattr(-,root,root)
%{_sbindir}/msggen
%{_sbindir}/rsyslog_diag_hostname
%{rsyslog_module_dir_nodeps}/imdiag.so
%endif

%if %{with gssapi}

%files module-gssapi
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omgssapi.so
%{rsyslog_module_dir_withdeps}/imgssapi.so
%{rsyslog_module_dir_withdeps}/lmgssutil.so
%endif

%if %{with mysql}

%files module-mysql
%defattr(-,root,root)
%doc %{rsyslogdocdir}/mysql-createDB.sql
%{rsyslog_module_dir_withdeps}/ommysql.so
%endif

%if %{with pgsql}

%files module-pgsql
%defattr(-,root,root)
%doc %{rsyslogdocdir}/pgsql-createDB.sql
%{rsyslog_module_dir_withdeps}/ompgsql.so
%endif

%if %{with dbi}

%files module-dbi
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omlibdbi.so
%endif

%if %{with snmp}

%files module-snmp
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omsnmp.so
%{rsyslog_module_dir_nodeps}/mmsnmptrapd.so
%endif

%if %{with gnutls}

%files module-gtls
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/lmnsd_gtls.so
%endif

%if %{with relp}

%files module-relp
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/imrelp.so
%{rsyslog_module_dir_withdeps}/omrelp.so
%endif

%if %{with mmnormalize}

%files module-mmnormalize
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/mmnormalize.so
%{rsyslog_module_dir_withdeps}/mmjsonparse.so
%{rsyslog_module_dir_withdeps}/mmaudit.so
%endif

%if %{with udpspoof}

%files module-udpspoof
%defattr(-,root,root)
%{rsyslog_module_dir_nodeps}/omudpspoof.so
%endif

%if %{with elasticsearch}

%files module-elasticsearch
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omelasticsearch.so
%endif

%if %{with omhttpfs}

%files module-omhttpfs
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omhttpfs.so
%endif

%if %{with hdfs}

%files module-hdfs
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omhdfs.so
%endif

%if %{with mongodb}

%files module-mongodb
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/ommongodb.so
%endif

%if %{with hiredis}

%files module-hiredis
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omhiredis.so
%endif

%if %{with zeromq}

%files module-zeromq
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/imzmq3.so
%{rsyslog_module_dir_withdeps}/omzmq3.so
%endif

%if %{with omamqp1}
%files module-omamqp1
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omamqp1.so
%endif

%if %{with gcrypt}

%files module-gcrypt
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/lmcry_gcry.so
%{_bindir}/rscryutil
#%{_mandir}/man1/rscryutil.1*
%endif

%if %{with tcl}
%files module-omtcl
%defattr(-,root,root)
%{rsyslog_module_dir_withdeps}/omtcl.so*
%endif

%changelog
