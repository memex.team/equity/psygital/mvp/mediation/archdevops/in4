VERSION=$1
PORT=$2

VerMaj=${VERSION:0:1}

sh ./plugins/search-guard-$VerMaj/tools/sgadmin.sh -f /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgconfig_${VERSION}/sg_tenants.yml -t tenants -key /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.key -cert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.pem -cacert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/ca_infra.pem  -nhnv -icl -p 93${PORT}

sh ./plugins/search-guard-$VerMaj/tools/sgadmin.sh -f /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgconfig_${VERSION}/sg_action_groups.yml -t actiongroups -key /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.key -cert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.pem -cacert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/ca_infra.pem  -nhnv -icl -p 93${PORT}

sh ./plugins/search-guard-$VerMaj/tools/sgadmin.sh -f /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgconfig_${VERSION}/sg_config.yml -t config -key /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.key -cert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.pem -cacert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/ca_infra.pem  -nhnv -icl -p 93${PORT}

sh ./plugins/search-guard-$VerMaj/tools/sgadmin.sh -f /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgconfig_${VERSION}/sg_roles.yml -t roles -key /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.key -cert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.pem -cacert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/ca_infra.pem  -nhnv -icl -p 93${PORT}

sh ./plugins/search-guard-$VerMaj/tools/sgadmin.sh -f /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgconfig_${VERSION}/sg_roles_mapping.yml -t rolesmapping -key /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.key -cert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.pem -cacert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/ca_infra.pem  -nhnv -icl -p 93${PORT}

sh ./plugins/search-guard-$VerMaj/tools/sgadmin.sh -f /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgconfig_${VERSION}/sg_internal_users.yml -t internalusers -key /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.key -cert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/sgadmin.pem -cacert /media/sysdata/in4/context/_me/landscape/data--c/database--o/json--f/elasticsearch/ca_infra.pem  -nhnv -icl -p 93${PORT}

exit 0
