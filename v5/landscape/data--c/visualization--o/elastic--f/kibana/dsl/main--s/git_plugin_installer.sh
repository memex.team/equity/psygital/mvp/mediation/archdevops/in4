#!/bin/bash
########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = bash
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########
set -e

##


##

KibanaVersions=$1
export INSTANCE=$2
GitHubRepo=$3
Package=`echo $GitHubRepo| cut -d'/' -f 5`

TempPath='/tmp/kibanaPlugins'
rm -rf $TempPath

mkdir -p $TempPath/kibana && cd $TempPath/kibana
git clone ${GitHubRepo}.git $TempPath/kibana/$Package
rm -rf ${Package}/.git

for ver in "${KibanaVersions[@]}"; do
  export VERSION=`echo $ver|cut -d '.' -f 1``echo $ver|cut -d '.' -f 2`
  . /media/sysdata/in4/context/_me/landscape/data--c/visualization--o/elastic--f/kibana/env_${VERSION}_${INSTANCE}
  export ES_LOGIN
  export ES_PASS
  export KIBANA_COOKIE_PASS
  export DATAPATH="/media/storage/projects/$INSTANCE/elastic/kibana/$VERSION/data"
  
  echo "Version: ${ver}, VERSION: $VERSION"
  sed -i "s|\"version\".*:.*.\".*.\"|\"version\":\ \"${ver}\"|g" ./${Package}/package.json 
  
  cd ../
  zip -r ${Package}-${ver}.zip kibana
  ! NODE_OPTIONS="--max-old-space-size=3072" timeout 20 /media/storage/app/elastic/kibana/${VERSION}/bin/kibana-plugin install --no-optimize -c /media/sysdata/in4/platform/v5/landscape/data--c/visualization--o/elastic--f/kibana/dsl/main--s/kibana.yaml -c /media/sysdata/in4/platform/v5/landscape/data--c/visualization--o/elastic--f/kibana/dsl/main--s/${VERSION}.yaml file://$TempPath/${Package}-${ver}.zip

  cd /media/storage/app/elastic/kibana/${VERSION}/plugins/$Package
  npm install --production
  
  /media/storage/app/elastic/kibana/${VERSION}/bin/kibana -c /media/sysdata/in4/platform/v5/landscape/data--c/visualization--o/elastic--f/kibana/dsl/main--s/kibana.yaml -c /media/sysdata/in4/platform/v5/landscape/data--c/visualization--o/elastic--f/kibana/dsl/main--s/${VERSION}.yaml --optimize
  
  rm -f $TempPath/kibana/*.zip
  cd $TempPath/kibana
done

